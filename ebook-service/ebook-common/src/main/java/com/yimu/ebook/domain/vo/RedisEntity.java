package com.yimu.ebook.domain.vo;

public interface RedisEntity {
    String generateRedisKey();
    String generateRedisKey(Object key);
}