package com.yimu.ebook.util;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;


@Slf4j
public class TxSmsUtils {

    private static final int APPID = 1400222698;
    private static final String APPKEY = "497e5c4168d64ca2966240d1c01819ce";

    private static final int LOGIN_TMP_ID = 359262;

    private TxSmsUtils() {
    }

    public static class SingleHolder {
        private static TxSmsUtils txSmsUtils;
        private SingleHolder() {}
        public static TxSmsUtils getInstance() {
            if (txSmsUtils == null) {
                synchronized (SingleHolder.class) {
                    if (txSmsUtils == null) {
                        txSmsUtils = new TxSmsUtils();
                    }
                }
            }
            return txSmsUtils;
        }
    }

    public static SmsSingleSenderResult seedMessage(String telphone, String msgCode) {
        SmsSingleSender smsSingleSender = new SmsSingleSender(APPID, APPKEY);
        try {
            String[] arr = {msgCode};
            SmsSingleSenderResult result = smsSingleSender.sendWithParam("86", telphone, LOGIN_TMP_ID, arr, "", "", "");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        return null;
    }

    /*public static void main(String[] args) {
        SmsSingleSender smsSingleSender = new SmsSingleSender(APPID, APPKEY);
        try {
            String[] arr = {"3311"};
            SmsSingleSenderResult result = smsSingleSender.sendWithParam("86", "17521226604", LOGIN_TMP_ID, arr, "", "", "");

            System.out.println(result);
            return;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
    }*/

    /**
     * 普通单发
     *
     * 普通单发短信接口，明确指定内容，如果有多个签名，请在内容中以【】的方式添加到信息内容中，否则系统将使用默认签名
     *
     * @param type 短信类型，0 为普通短信，1 营销短信
     * @param nationCode 国家码，如 86 为中国
     * @param phoneNumber 不带国家码的手机号
     * @param msg 信息内容，必须与申请的模板格式一致，否则将返回错误
     * @param extend 扩展码，可填空
     * @param ext 服务端原样返回的参数，可填空
     * @return {@link}SmsSingleSenderResult
     * @throws HTTPException  http status exception
     * @throws JSONException  json parse exception
     * @throws IOException    network problem
     */
    /*public SmsSingleSenderResult send(int type, String nationCode, String phoneNumber,
        String msg, String extend, String ext)
            throws HTTPException, JSONException, IOException {
        SmsSingleSender smsSingleSender = new SmsSingleSender(APPID, APPKEY);
        SmsSingleSenderResult result = smsSingleSender.send(type, nationCode, phoneNumber, msg, extend, ext);
        return result;
    }*/

    /**
     * 指定模板单发
     *
     * @param nationCode 国家码，如 86 为中国
     * @param phoneNumber 不带国家码的手机号
     * @param templateId 信息内容
     * @param params 模板参数列表，如模板 {1}...{2}...{3}，那么需要带三个参数
     * @param sign 签名，如果填空，系统会使用默认签名
     * @param extend 扩展码，可填空
     * @param ext 服务端原样返回的参数，可填空
     * @return {@link}SmsSingleSenderResult
     * @throws HTTPException  http status exception
     * @throws JSONException  json parse exception
     * @throws IOException    network problem
     */
    /*public SmsSingleSenderResult sendWithParam(String nationCode, String phoneNumber, int templateId,
        ArrayList<String> params, String sign, String extend, String ext)
            throws HTTPException, JSONException, IOException {
        SmsSingleSender smsSingleSender = new SmsSingleSender(APPID, APPKEY);
        SmsSingleSenderResult result = smsSingleSender.sendWithParam(nationCode, phoneNumber, templateId, params, sign, extend, ext);
        return result;
    }

    public SmsSingleSenderResult sendWithParam(String nationCode, String phoneNumber, int templateId,
        String[] params, String sign, String extend, String ext)
            throws HTTPException, JSONException, IOException {
        SmsSingleSender smsSingleSender = new SmsSingleSender(APPID, APPKEY);
        SmsSingleSenderResult result = smsSingleSender.sendWithParam(nationCode, phoneNumber, templateId, params, sign, extend, ext);
        return result;
    }*/

}
