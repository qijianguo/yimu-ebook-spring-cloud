package com.yimu.ebook.constant;

import java.util.Arrays;
import java.util.List;

/**
 * @author Angus
 * @version 1.0
 * @Title: Constants
 * @Description: TODO
 * @date 2019/2/26 15:25
 */
public class Constants {

    public static final String AUTHENTICATION = "authentication";

    public static final String VERSION = "version";
    public static final String VERSION_CODE = "versionCode";
    public static final String VERSION_NAME = "versionName";

    public static final String PLATFORM = "platform";

    public static final String HANNELS = "hannels";

    /**
     * 存储当前登录用户id的字段名
     */
    public static final String CURRENT_USER_ID = "currentUserId";

    public static final String LOGIN_USER = "login_user";

    public static final String IS_LOGIN = "is_login";

    public static final String TOKEN = "token";

    public static String TELPHONE = "telphone";

    public static int TOKEN_EXPIRES_HOUR = 60 * 60 * 24 * 7;

    public static int TELPHONE_CODE_HOUR = 60 * 60 * 24;

    /**
     * 验证码5分钟超时
     */
    public static long TELPHONE_CODE_TIMEOUT_5_MINUTE = 1000 * 60 * 5;

    public static long TELPHONE_CODE_DAY_MAX_SIZE = 10;

    public static long TELPHONE_CODE_MINUTE_MAX_SIZE = 1;

    public static long HOUR = 1000 * 60 * 60;

    public static long MINUTE = 1000 * 60;

    public static long DAY = 1000 * 60 * 60 * 24;


    public static final List<String> APP_PLATFORMS = Arrays
        .asList("android", "ios", "h5", "flutter", "yzCloud");

    public static final int SUMMARY_MAX_SIZE = 300;
}
