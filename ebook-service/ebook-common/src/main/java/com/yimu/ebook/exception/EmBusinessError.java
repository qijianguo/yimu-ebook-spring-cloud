package com.yimu.ebook.exception;

/**
 * @author Angus
 */
public enum  EmBusinessError implements CommonError {

    // 10000开头是通用错误类型
    PARAMETER_VALIDATION_ERROR(10001, "参数不合法"),
    UNKNOW_ERROR(10002, "未知错误"),
    NOT_FOUND_RESOURCE(10003, "无对应数据"),
    DATA_FORMAT_ERROR(10004, "格式转换错误"),

    /* 20000开头是用户模块错误码 */
    USER_NOT_EXIST(20001, "用户不存在"),
    USER_ALREADY_EXIST(20002, "手机号已注册，请登录"),
    USER_LOGIN_FAIL(20003, "用户名或密码错误"),
    USER_NOT_LOGIN(20004, "用户未登陆"),
    USER_NOT_AUTHORIZATION(20005, "用户无权限"),
    USER_NOT_TELPHONE(20006, "请绑定手机号"),
    WINXIN_AUTHORIZATION_FAIL(20007, "微信授权失败"),
    USER_TOKEN_EXPIRE(20008, "用户账号异常，请重新登陆"),
    QUEST_CODE_FREQUENCY(20009, "验证码获取太频繁"),
    QUERY_CODE_TIMES_MAXED(10010, "请求次数已超过限制"),
    QUERY_CODE_FILED(10011, "短信验证码发送失败，请重试"),

    /* 30000 开头App版本信息错误码*/
    VERSION_EXPIRED(30001, "请更新至最新版本"),

    ROLE_ALREADY_EXISTS(60000, "角色已存在"),
    ROLE_NOT_EXISTS(60000, "角色不存在"),

    ;


    private int errorCode;

    private String errorMsg;

    EmBusinessError(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public int getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String getErrorMsg() {
        return this.errorMsg;
    }

    @Override
    public CommonError setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
        return this;
    }
}
