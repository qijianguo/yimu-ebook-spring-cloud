package com.yimu.ebook.exception;

/**
 * @author Angus
 * @version 1.0
 * @Title: CommonError
 * @Description: TODO
 * @date 2019/2/16 12:40
 */
public interface CommonError {

    int getErrorCode();

    String getErrorMsg();

    CommonError setErrorMsg(String errorMsg);
}
