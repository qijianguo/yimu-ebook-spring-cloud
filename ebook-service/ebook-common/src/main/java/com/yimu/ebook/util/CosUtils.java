package com.yimu.ebook.util;

import com.google.common.base.Charsets;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.exception.CosServiceException;
import com.qcloud.cos.model.GetObjectRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.model.StorageClass;
import com.qcloud.cos.region.Region;
import com.qcloud.cos.transfer.Download;
import com.qcloud.cos.transfer.Transfer.TransferState;
import com.qcloud.cos.transfer.TransferManager;
import com.qcloud.cos.transfer.Upload;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

/**
 * 高级sdk操作
 * @author Angus
 */
@Slf4j
public class CosUtils {

  /**
   * SECRET_ID 是用于标识 API 调用者的身份
   */
  public static final String SECRET_ID = "AKIDKPFp8MFKyLTM0OkquiFbhX9bt2cwZ3iP";
  /**
   * SecretKey是用于加密签名字符串和服务器端验证签名字符串的密钥
   */
  public static final String SECRET_KEY = "lGUR7YibItEk13pKZcS2TfeOvENG3jAK";
  /**
   * 上海 （中国大陆）（ap-shanghai）
   */
  public static final String REGION = "ap-shanghai";

  /**
   * 书籍已不再更新，拟废弃（请使用ebook2的内容）
   */
  @Deprecated
  public static final String BUCKET_EBOOK = "ebook";
  public static final String BUCKET_EBOOK2 = "ebook2";
  public static final String BUCKET_EBOOK_IMAGE = "ebook-image";
  /**
   * APP_ID
   */
  public static final String APP_ID = "1259465809";
  public static COSCredentials cred = null;
  public static TransferManager transferManager = null;
  public static COSClient cosClient = null;
  public static ThreadPoolExecutor threadPoolExecutor;

  static {
    // 1 初始化用户身份信息(secretId, secretKey)
    cred = new BasicCOSCredentials(SECRET_ID, SECRET_KEY);
    // 2 设置bucket的区域,
    ClientConfig clientConfig = new ClientConfig(new Region(REGION));
    // 3 生成cos客户端
    cosClient = new COSClient(cred, clientConfig);
    // 指定要上传到 COS 上的路径
    ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("cos-pool-%d").build();
    threadPoolExecutor = new ThreadPoolExecutor(5, 10, 0L,
        TimeUnit.MILLISECONDS,
        new LinkedBlockingDeque<>(128), threadFactory);
    // 传入一个 threadpool, 若不传入线程池, 默认 TransferManager 中会生成一个单线程的线程池。
    transferManager = new TransferManager(cosClient, threadPoolExecutor);
  }

  private static void init() {

  }

  public static void destroyTransferManager() {
    if (cosClient != null) {
      cosClient.shutdown();
    }
    if (transferManager != null) {
      transferManager.shutdownNow();
    }
  }

    /**
     * 上传
     */
    public static String upload(String bucketName, String fullpath, String content) {
        try {
          init();
          // .....(提交上传下载请求, 如下文所属)
          // bucket 的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
          String bucket = bucketName + "-" + APP_ID;

          // 先删掉原来存在的
          delete(bucketName, fullpath);

          byte[] bytes = content.getBytes(Charsets.UTF_8);
          ObjectMetadata objectMetadata = new ObjectMetadata();
          PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fullpath, new ByteArrayInputStream(bytes), objectMetadata);

          // File localFile = new File(fullname);
          // PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, fullpath, localFile);
          // putObjectRequest.setInputStream(new FileInputStream(content));
          // 本地文件上传
          Upload upload = transferManager.upload(putObjectRequest);
          // 异步（如果想等待传输结束，则调用 waitForUploadResult）
          //UploadResult uploadResult = upload.waitForUploadResult();
          //同步的等待上传结束waitForCompletion
          upload.waitForCompletion();
          //获取上传成功之后文件的下载地址
          URL url = cosClient.generatePresignedUrl(bucketName + "-" + APP_ID, fullpath,
              new Date(System.currentTimeMillis() + 5 * 60 * 10000));
          // 走CDN
          return "http://ebook2-1259465809.file.myqcloud.com" + url.getPath();
        } catch (Throwable tb) {
            tb.printStackTrace();
            log.error("上传失败:" + tb.getMessage(), tb);
        } finally {
            // 关闭 TransferManger
//            transferManager.shutdownNow();
//            cosClient.shutdown();
        }
        return null;
    }

    /**
     * 下载
     */
    public static String downloadContent(String bucketName, String fullpath) {
      try {
        init();
        //下载到本地指定路径
        File localDownFile = File.createTempFile(fullpath, null);
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName + "-" + APP_ID,
            fullpath);
        // 下载文件
          Download download = transferManager.download(getObjectRequest, localDownFile);
          // 等待传输结束（如果想同步的等待上传结束，则调用 waitForCompletion）
          download.waitForCompletion();
          if (download.getState() == TransferState.Completed) {
            String content = FileUtils.readFileToString(localDownFile, Charsets.UTF_8);
            return content;
          }
      } catch (Throwable tb) {
          tb.printStackTrace();
          log.error("下载失败:" + tb.getMessage(), tb);
      } finally {
          // 关闭 TransferManger
//          transferManager.shutdownNow();
//          cosClient.shutdown();
      }
      return null;
    }

    /**
     * 删除
     */
    public static void delete(String bucketName, String fullpath) {
      // 指定要删除的 bucket 和路径
      try {
        init();
        cosClient.deleteObject(bucketName + "-" + APP_ID, fullpath);
      } catch (Throwable tb) {
          tb.printStackTrace();
          log.error("删除文件失败:" + tb.getMessage(), tb);
      }
    }

    private static String findFilePath(String fullpath, String content) {
      boolean success = FileUtil.writeContent(fullpath, content);
      if (success) {
          return fullpath;
      }
      return null;
    }

  /**
   * 从输入流进行读取并上传到COS
   */
  public static void SimpleUploadFileFromStream(String bucketName, String key, String fullpath) {
    init();
    File localFile = new File(fullpath);
    InputStream input = new ByteArrayInputStream(new byte[10]);
    ObjectMetadata objectMetadata = new ObjectMetadata();
    // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
    objectMetadata.setContentLength(10);
    // 默认下载时根据cos路径key的后缀返回响应的contenttype, 上传时设置contenttype会覆盖默认值
    objectMetadata.setContentType("image/jpeg");

    PutObjectRequest putObjectRequest =
        new PutObjectRequest(bucketName + "-" + APP_ID, key, input, objectMetadata);
    // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
    putObjectRequest.setStorageClass(StorageClass.Standard_IA);
    try {
      PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
      // putobjectResult会返回文件的etag
      String etag = putObjectResult.getETag();
    } catch (CosServiceException e) {
      e.printStackTrace();
    } catch (CosClientException e) {
      e.printStackTrace();
    }

    // 关闭客户端
    cosClient.shutdown();
  }

  /**
   * 将本地文件上传到COS
   * @param bucketName
   * @param key
   */
  public static void simpleUploadFile(String bucketName, String key, File file) {
    init();
    ObjectMetadata objectMetadata = new ObjectMetadata();
    // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
    objectMetadata.setContentLength(10);
    // 默认下载时根据cos路径key的后缀返回响应的contenttype, 上传时设置contenttype会覆盖默认值
    objectMetadata.setContentType("image/jpeg");
    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName + "-" + APP_ID, key,
        file);
    // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
    putObjectRequest.setStorageClass(StorageClass.Standard_IA);
    try {
      PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
      // putobjectResult会返回文件的etag
      String etag = putObjectResult.getETag();
    } catch (CosServiceException e) {
      e.printStackTrace();
    } catch (CosClientException e) {
      e.printStackTrace();
    }
    // 关闭客户端
    cosClient.shutdown();
  }
}