package com.yimu.ebook.util;

public class RedisKeyUtils {

    public static String userLoginCode(String telphone) {
        return String.format("user_login_code:%s:telphone", telphone);
    }

    public static String monthHotRecommend(Integer bookId) {
        return String.format("hot_rank_month:%d:book_id", bookId);
    }

    public static String weekHotRecommend(Integer bookId) {
        return String.format("hot_rank_week:%d:book_id", bookId);
    }

    public static String newBookHotRecommend(Integer bookId) {
        return String.format("hot_rank_newbook:%d:book_id", bookId);
    }

    public static String bookFollower(long bookId) {
        return String.format("book_follower:%d:book", bookId);
    }

    public static String platformVersion(String platform) {
        return String.format("version:%s:platform", platform);
    }

    public static String third17QreadMaxChapter(String bookUuid) {
        return String.format("third_17qread_lastest_chapter:%s:bookUuid", bookUuid);
    }

    public static final String RANK_TOTAL = "rank:total";
    public static final String RANK_MONTH = "rank:month_hot";
    public static final String RANK_WEEK = "rank:week";
    public static final String RANK_SEARCH = "rank:search";
    public static final String RANK_NEWBOOK = "rank:newbook";
    public static final String RANK_EDIT_RECOMMEND = "rank:editor_recommend";

    public static String checkBookUpdate(Long bookId) {
        return String.format("check_book_update:%d", bookId);
    }

    public static String bookHotRank(Long bookId) {
        return String.format("book_hot_rank:%d", bookId);
    }
}
