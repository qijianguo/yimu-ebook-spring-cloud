package com.yimu.ebook.util;

import org.apache.tomcat.util.security.ConcurrentMessageDigest;
import org.apache.tomcat.util.security.MD5Encoder;

public class MD5Utils {

    public static String MD5(String key) {
        return MD5Encoder.encode(ConcurrentMessageDigest.digestMD5(
                key.getBytes()));
    }
}