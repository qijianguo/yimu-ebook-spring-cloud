package com.yimu.ebook.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class QcloudSmsUtil {

    private static int sdkappid = 1400023360;
    private static String appkey = "5d143f854a2c6b30ee2e4959ff7cccd3";
    private static String sign = "3686";
    private static String tplId = "9107";

    /*private static int sdkappid = 1400222698;
    private static String appkey = "497e5c4168d64ca2966240d1c01819ce";
    private static String sign = "218136";
    private static String tplId = "359262";
    */

    private static boolean sendMsg(String mobile, String content) {
        String url = "https://yun.tim.qq.com/v5/tlssmssvr/sendsms";
        Random random = new Random();
        long rnd = random.nextInt(999999) % (999999 - 100000 + 1) + 100000;
        long time = System.currentTimeMillis()/1000;
        String wholeUrl = String.format("%s?sdkappid=%d&random=%d", url, sdkappid, rnd);
        String pnum = null;
        try {
            Map data = new HashMap();
            JSONObject tel = new JSONObject();
            tel.put("nationcode", "86");
            tel.put("mobile", mobile);

            List<String> params = Arrays.asList(content);
            String sig = getSign(appkey,rnd,time,mobile);

            data.put("tpl_id", tplId);
            data.put("sign", sign);
            data.put("sig", sig);
            data.put("params", params);
            data.put("tel", tel);
            data.put("time",time);
            data.put("extend", "");
            data.put("ext", "");

            String response = HttpUtils.getInstance().executePost(wholeUrl, data);
            JSONObject rsp = JSON.parseObject(response);
            if(rsp.getInteger("result") == 0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * sha256(appkey=5f03a35d00ee52a21327ab048186a2c4&random=7226249334&time=1457336869&mobile=13788888888)
     * = ecab4881ee80ad3d76bb1da68387428ca752eb885e52621a3129dcf4d9bc4fd4
     * @param appkey
     * @param rnd
     * @param time
     * @param mobile
     * @return
     */
    private static String getSign(String appkey,long rnd,long time,String mobile){
        return getSHA256Str(String.format("appkey=%s&random=%d&time=%d&mobile=%s",appkey,rnd,time,mobile));
    }

    /***
     * 利用Apache的工具类实现SHA-256加密
     * @param str 加密后的报文
     * @return
     */
    public static String getSHA256Str(String str){
        MessageDigest messageDigest;
        String encdeStr = "";
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] hash = messageDigest.digest(str.getBytes("UTF-8"));
            encdeStr = Hex.encodeHexString(hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encdeStr;
    }

    /*public static void main(String[] args) {
        QcloudSmsUtil.sendMsg("17521226604","1234");
    }*/
}