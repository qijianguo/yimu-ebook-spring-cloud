package com.yimu.ebook.service;

import com.yimu.ebook.domain.vo.BookChapterInfoResponse;
import com.yimu.ebook.domain.vo.BookChaptersResponse;
import com.yimu.ebook.domain.vo.BookInfoResponse;
import com.yimu.ebook.domain.vo.BookListResponse;
import com.yimu.ebook.exception.BusinessException;

public interface Third17qreadService {

    /**
     * 获取MD5后的sign
     * @param key
     * @param sid 数据ID
     * @return
     */
    String getSign(String key, String sid);

    /**
     * 获取所有授权作品ID
     * @param updateTime 最后更新时间(不是必须)
     * @param sid 数据ID
     * @param sign MD5加密密钥
     * @return
     * @throws BusinessException
     */
    BookListResponse bookList(String updateTime, String sid, String sign) throws BusinessException;


    /**
     * 获取作品信息
     * @param bookId 小说ID
     * @param sid 数据ID
     * @param sign MD5加密密钥
     * @return
     * @throws BusinessException
     */
    BookInfoResponse bookInfo(String bookId, String sid, String sign) throws BusinessException;

    /**
     * 获取作品信息
     * @param bookId 小说ID
     * @param sid 数据ID
     * @param sign MD5加密密钥
     * @return
     * @throws BusinessException
     */
    BookChaptersResponse bookChapters(String bookId, String sid, String sign)
        throws BusinessException;

    /**
     * 获取作品信息
     * @param bookId 小说ID
     * @param chapterId 章节ID
     * @param sid 数据ID
     * @param sign MD5加密密钥
     * @return
     * @throws BusinessException
     */
    BookChapterInfoResponse bookChapterInfo(String bookId, String chapterId, String sid,
        String sign) throws BusinessException;

}
