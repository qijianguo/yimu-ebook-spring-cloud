package com.yimu.ebook.service.impl;


import com.yimu.ebook.dao.crawler.CrawlerBookDOMapper;
import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDO;
import com.yimu.ebook.service.CrawlerBookService;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
public class CrawlerBookServiceImpl implements CrawlerBookService {

    private final CrawlerBookDOMapper crawlerBookDOMapper;

    public CrawlerBookServiceImpl(
        CrawlerBookDOMapper crawlerBookDOMapper) {
        this.crawlerBookDOMapper = crawlerBookDOMapper;
    }

    @Override
    public List<CrawlerBookDO> getBySync(Integer sync) {
        return Optional.ofNullable(crawlerBookDOMapper.selectBySync(sync)).orElse(Collections.EMPTY_LIST);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateSyncByBatch(List<String> bookIds) {
        if (CollectionUtils.isEmpty(bookIds)) {
            return;
        }
        crawlerBookDOMapper.updateSyncByBatch(bookIds);
    }

    @Override
    public void updateSync(String bookId) {
        crawlerBookDOMapper.updateSync(bookId);
    }
}
