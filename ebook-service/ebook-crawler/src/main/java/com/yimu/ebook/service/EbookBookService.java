package com.yimu.ebook.service;

import com.yimu.ebook.domain.dataobject.ebook.BookChapterDO;
import com.yimu.ebook.domain.dataobject.ebook.BookDO;
import com.yimu.ebook.domain.vo.CreateBookChapterRequest;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.exception.BusinessException;
import java.util.List;

/**
 * @author Angus
 */
public interface EbookBookService {

    /**
     * 查询所有章节
     * @return
     */
    List<BookChapterDO> findAll();

    /**
     * 保存书籍
     * @param request
     * @throws BusinessException
     */
    void saveOrUpdate(CreateBookRequest request) throws BusinessException;

    /**
     * 保存章节内容
     * @param request
     * @throws BusinessException
     */
    void saveOrUpdate(CreateBookChapterRequest request) throws BusinessException;

    /**
     * 批量保存章节内容
     * @param requests
     * @throws BusinessException
     * @return 
     */
    int saveOrUpdateBatch(List<CreateBookChapterRequest> requests) throws BusinessException;

    /**
     * 批量保存章节内容
     * @param requests
     * @throws BusinessException
     * @return
     */
    int saveOrUpdateBatchNotUpdateSync(List<CreateBookChapterRequest> requests) throws BusinessException;

    int updateBookAndChapter(List<CreateBookChapterRequest> unSyncChapterRequestList, CreateBookRequest unSyncBookRequest);

}
