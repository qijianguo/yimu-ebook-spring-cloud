package com.yimu.ebook.service;


import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookChapterDO;
import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDO;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface CrawlerBookChapterService {

    /**
     * 分页获取同步/非同步的章节
     * @param sync 0未同步 1已同步
     * @param pageNum 当前页，从1开始
     * @param size 每页个数
     * @return 章节列表
     */
    List<CrawlerBookChapterDO> getBySync(Integer sync, Integer pageNum, Integer size) throws BusinessException;

    /**
     * 批量更新章节的同步状态
     * @param ids
     * @throws BusinessException
     */
    void updateSyncByBatch(List<Long> ids) throws BusinessException;

    /**
     * 获取没有同步的章节个数
     * @return
     */
    int getUnSyncCount();

    List<CrawlerBookChapterDO> getByBookIdAndSync(String bookUuid, Integer sync) throws BusinessException;

}
