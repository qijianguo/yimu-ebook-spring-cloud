package com.yimu.ebook.service;


import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDO;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface CrawlerBookService {

    /**
     * 根据同步状态查询
     * @param sync 0未同步 1同步
     * @return 书籍列表
     * @throws BusinessException
     */
    List<CrawlerBookDO> getBySync(Integer sync) throws BusinessException;

    /**
     * 批量更新sync状态
     * @param bookIds
     * @throws BusinessException
     */
    void updateSyncByBatch(List<String> bookIds) throws BusinessException;

    void updateSync(String bookId);

}
