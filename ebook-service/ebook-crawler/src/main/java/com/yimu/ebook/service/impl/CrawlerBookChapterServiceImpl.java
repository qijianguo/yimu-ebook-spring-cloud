package com.yimu.ebook.service.impl;


import com.yimu.ebook.dao.crawler.CrawlerBookChapterDOMapper;
import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookChapterDO;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CrawlerBookChapterService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
public class CrawlerBookChapterServiceImpl implements CrawlerBookChapterService {

    private final CrawlerBookChapterDOMapper crawlerBookChapterDOMapper;

    public CrawlerBookChapterServiceImpl(
        CrawlerBookChapterDOMapper crawlerBookChapterDOMapper) {
        this.crawlerBookChapterDOMapper = crawlerBookChapterDOMapper;
    }

    @Override
    public List<CrawlerBookChapterDO> getBySync(Integer sync, Integer pageNum, Integer size) {
        return Optional.ofNullable(crawlerBookChapterDOMapper.selectBySync(sync, pageNum, size)).orElse(Collections.EMPTY_LIST);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateSyncByBatch(List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        crawlerBookChapterDOMapper.updateSyncByBatch(ids);
    }

    @Override
    public int getUnSyncCount() {
        return crawlerBookChapterDOMapper.getUnSyncCount();
    }

    @Override
    public List<CrawlerBookChapterDO> getByBookIdAndSync(String bookUuid, Integer sync)
        throws BusinessException {
        return crawlerBookChapterDOMapper.selectByBookIdAndSync(bookUuid, sync);
    }
}
