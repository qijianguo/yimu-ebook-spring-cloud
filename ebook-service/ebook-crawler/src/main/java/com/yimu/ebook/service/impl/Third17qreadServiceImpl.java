package com.yimu.ebook.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.yimu.ebook.domain.vo.BookChapterInfoResponse;
import com.yimu.ebook.domain.vo.BookChaptersResponse;
import com.yimu.ebook.domain.vo.BookInfoResponse;
import com.yimu.ebook.domain.vo.BookListResponse;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.Third17qreadService;
import com.yimu.ebook.util.HttpUtils;
import com.yimu.ebook.util.MD5Utils;
import com.yimu.ebook.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Angus
 */
@Slf4j
@Service
public class Third17qreadServiceImpl implements Third17qreadService {

    @Override
    public String getSign(String key, String sid) {
        String date = TimeUtil.formatDate(new Date(), TimeUtil.YYYYHHMM);
        return MD5Utils.MD5(date + sid + key);
    }

    @Override
    public BookListResponse bookList(String updateTime, String sid, String sign)
        throws BusinessException {
        StringBuilder stringBuffer = new StringBuilder("http://api.17qread.com/apis/api/BookList.php");
        stringBuffer.append("?sid=").append(sid)
                .append("&sign=").append(sign)
                .append("&create_time=").append(updateTime);

        try {
            String result = HttpUtils.getInstance().executeGet(stringBuffer.toString());
            return JSONObject.parseObject(result, BookListResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, String.format("获取书籍列表失败, 原因是:【%s】", e));
        }
    }

    @Override
    public BookInfoResponse bookInfo(String bookId, String sid, String sign)
        throws BusinessException {
        StringBuilder stringBuffer = new StringBuilder("http://api.17qread.com/apis/api/BookInfo.php");
        stringBuffer.append("?sid=").append(sid)
                .append("&sign=").append(sign)
                .append("&bookid=").append(bookId);

        try {
            String result = HttpUtils.getInstance().executeGet(stringBuffer.toString());
            return JSONObject.parseObject(result, BookInfoResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, String.format("获取bookId=【%s】失败, 原因是:【%s】", bookId, e));
        }
    }

    @Override
    public BookChaptersResponse bookChapters(String bookId, String sid, String sign)
        throws BusinessException {
        StringBuilder stringBuffer = new StringBuilder("http://api.17qread.com/apis/api/BookChapters.php");
        stringBuffer.append("?sid=").append(sid)
                .append("&sign=").append(sign)
                .append("&bookid=").append(bookId);

        try {
            String result = HttpUtils.getInstance().executeGet(stringBuffer.toString());
            return JSONObject.parseObject(result, BookChaptersResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, String.format("获取bookId=【%s】章节列表失败, 原因是:【%s】", bookId, e));
        }
    }

    @Override
    public BookChapterInfoResponse bookChapterInfo(String bookId, String chapterId, String sid, String sign)
        throws BusinessException {
        StringBuilder stringBuffer = new StringBuilder("http://api.17qread.com/apis/api/BookChapterInfo.php");
        stringBuffer.append("?sid=").append(sid)
                .append("&sign=").append(sign)
                .append("&bookid=").append(bookId)
                .append("&chapterid=").append(chapterId);

        try {
            String result = HttpUtils.getInstance().executeGet(stringBuffer.toString());
            return JSONObject.parseObject(result, BookChapterInfoResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, String.format("获取bookId=【%s】章节chapterId=【%s】详情失败, 原因是: 【%s】", bookId, chapterId, e));
        }
    }


}
