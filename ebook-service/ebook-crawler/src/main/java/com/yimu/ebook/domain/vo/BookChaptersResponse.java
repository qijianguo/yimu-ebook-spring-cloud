package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookChaptersResponse {


    /**
     * code : 200
     * data : [{"volumeName":"正文","chapterlist":[{"chapter_id":"4012","chapter_name":"第一章 故人1","update_time":"1542770186","is_vip":0},{"chapter_id":"4013","chapter_name":"第二章 故人2","update_time":"1542770186","is_vip":0},{"chapter_id":"4014","chapter_name":"第三章 故人3","update_time":"1542770186","is_vip":0},{"chapter_id":"4015","chapter_name":"第四章 故人4","update_time":"1542770186","is_vip":0},{"chapter_id":"4016","chapter_name":"第五章 三人1","update_time":"1542770186","is_vip":0},{"chapter_id":"4017","chapter_name":"第六章 三人2","update_time":"1542770186","is_vip":0},{"chapter_id":"4018","chapter_name":"第七章 三人2","update_time":"1542770186","is_vip":0}]}]
     * message : Success
     */

    private int code;
    private String message;
    private List<DataBean> data;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DataBean {
        /**
         * volumeName : 正文
         * chapterlist : [{"chapter_id":"4012","chapter_name":"第一章 故人1","update_time":"1542770186","is_vip":0},{"chapter_id":"4013","chapter_name":"第二章 故人2","update_time":"1542770186","is_vip":0},{"chapter_id":"4014","chapter_name":"第三章 故人3","update_time":"1542770186","is_vip":0},{"chapter_id":"4015","chapter_name":"第四章 故人4","update_time":"1542770186","is_vip":0},{"chapter_id":"4016","chapter_name":"第五章 三人1","update_time":"1542770186","is_vip":0},{"chapter_id":"4017","chapter_name":"第六章 三人2","update_time":"1542770186","is_vip":0},{"chapter_id":"4018","chapter_name":"第七章 三人2","update_time":"1542770186","is_vip":0}]
         */

        private String volumeName;
        private List<ChapterlistBean> chapterlist;


        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public static class ChapterlistBean {
            /**
             * chapter_id : 4012
             * chapter_name : 第一章 故人1
             * update_time : 1542770186
             * is_vip : 0
             */

            private String chapter_id;
            private String chapter_name;
            private long update_time;
            private int is_vip;

            // TODO：非接口返回字段， 为了方便存储自行添加的
            private String contentUrl;

        }
    }
}
