package com.yimu.ebook.domain.dataobject.ebook;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BookDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.id
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.book_uuid
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private String bookUuid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.name
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private String name;

    private String author;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.cover
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private String cover;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.word_size
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private BigDecimal wordSize;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.hot
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Long hot;

    private Long favorites;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.score
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private BigDecimal score;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.summary
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private String summary;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.create_time
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.update_time
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Date updateTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.category_id
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Integer categoryId;

    private Integer subCategoryId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.channel_id
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Integer channelId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_book.book_status_id
     *
     * @mbg.generated Thu May 23 17:10:05 CST 2019
     */
    private Integer bookStatusId;

    /**
     * 是否控制展示：0不控制/1控制
     */
    private Integer active;

    private Integer ipControl;


}