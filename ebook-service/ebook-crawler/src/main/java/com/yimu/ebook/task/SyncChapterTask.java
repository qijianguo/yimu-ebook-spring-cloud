package com.yimu.ebook.task;

import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookChapterDO;
import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDO;
import com.yimu.ebook.domain.vo.CreateBookChapterRequest;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CrawlerBookChapterService;
import com.yimu.ebook.service.CrawlerBookService;
import com.yimu.ebook.service.EbookBookService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Component
@Slf4j
@Service
public class SyncChapterTask {

    private final CrawlerBookChapterService crawlerBookChapterService;

    private final EbookBookService ebookBookService;

    private final CrawlerBookService crawlerBookService;

    public SyncChapterTask(CrawlerBookChapterService crawlerBookChapterService,
        EbookBookService ebookBookService, CrawlerBookService crawlerBookService) {
        this.crawlerBookChapterService = crawlerBookChapterService;
        this.ebookBookService = ebookBookService;
        this.crawlerBookService = crawlerBookService;
    }

    /**
     * 拟废弃
     */
    @Deprecated
    public void syncBookShalf() {
//        log.info("-----开始同步章节任务-----");
//        try {
//            List<CrawlerBookChapterDO> bySync = crawlerBookChapterService.getBySync(0);
//            List<CreateBookChapterRequest> requests = convertFromDataObjectList(bySync);
//            List<Integer> successIds = new ArrayList<>();
//            requests.forEach(request -> {
//                try {
//                    ebookBookService.saveOrUpdate(request);
//                    successIds.add(request.getChapterId());
//                } catch (BusinessException e) {
//                    e.printStackTrace();
//                }
//            });
//            // 更新数据库同步状态
//            crawlerBookChapterService.updateSyncByBatch(successIds);
//        } catch (BusinessException e) {
//            e.printStackTrace();
//            log.info(e.getMessage(), e);
//        }
//        log.info("-----同步章节任务完成-----");
    }
    // @Scheduled(cron = "0 0 3 * * ?")
    public void sync() {
        syncBook();
        syncBookChapterBatch();
    }

    /**
     * 批量同步
     */
    @Deprecated
    private void syncBookChapterBatch() {
        log.info("-----开始同步章节任务-----");
        try {
            int count = crawlerBookChapterService.getUnSyncCount();
            int size = 1000;
            while (count > 1) {
                List<CrawlerBookChapterDO> bySync = crawlerBookChapterService.getBySync(0, 1, size);
                List<CreateBookChapterRequest> requests = convertFromChapterDataObjectList(bySync);
                ebookBookService.saveOrUpdateBatch(requests);
                count = crawlerBookChapterService.getUnSyncCount();
            }
            // 章节内容同步完成后再同步，防止章节内容同步落后于书籍内容同步
            syncBook();
        } catch (BusinessException e) {
            e.printStackTrace();
            log.info(e.getMessage(), e);
        }
        log.info("-----同步章节任务完成-----");
    }

    @Deprecated
    private void syncBook() {
        log.info("-----开始同步书籍更新信息-----");
        try {
            List<CrawlerBookDO> bySync = crawlerBookService.getBySync(0);
            List<CreateBookRequest> requests = convertFromBookDataObjectList(bySync);
            List<String> successIds = new ArrayList<>();
            requests.forEach(request -> {
                try {
                    ebookBookService.saveOrUpdate(request);
                    successIds.add(request.getBookId());
                } catch (BusinessException e) {
                    log.error(e.getMessage(), e);
                }
            });
            // 更新数据库同步状态
            crawlerBookService.updateSyncByBatch(successIds);
        } catch (BusinessException e) {
            e.printStackTrace();
            log.info(e.getMessage(), e);
        }
        log.info("-----同步书籍更新信息完成-----");
    }

    /**
     * 1. 查询所有未同步的书籍
     * 2. 遍历书籍，查询此书未同步的章节
     * 3. 同步章节，同步完后更新crawler中chapter的sync=1
     * 3. 同步书籍，同步完后更新crawler中book的sync=1
     */
    @Scheduled(cron = "0 1 * * * ?")
    public void syncNew() {
        List<CrawlerBookDO> allBook = crawlerBookService.getBySync(null);
        List<CreateBookRequest> allBookRequest = convertFromBookDataObjectList(allBook);
        allBookRequest.forEach(bookRequest -> {
            log.info("开始更新bookUuid=【{}】的信息", bookRequest.getBookId());
            List<CrawlerBookChapterDO> unSyncChapterList = crawlerBookChapterService
                .getByBookIdAndSync(bookRequest.getBookId(), 0);
            if (CollectionUtils.isEmpty(unSyncChapterList))  {
                log.info("bookUuid=【{}】无需要更新的章节", bookRequest.getBookId());
                return;
            }
            List<CreateBookChapterRequest> unSyncChapterRequestList = convertFromChapterDataObjectList(unSyncChapterList);
            // 保存修改 书籍和章节内容
            ebookBookService.updateBookAndChapter(unSyncChapterRequestList, bookRequest);
            log.info("更新bookUuid=【{}】完成", bookRequest.getBookId());
        });
    }


    private List<CreateBookRequest> convertFromBookDataObjectList(List<CrawlerBookDO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.EMPTY_LIST;
        }
        List<CreateBookRequest> requests = new ArrayList<>();
        list.forEach(book -> {
            CreateBookRequest request = new CreateBookRequest();
            BeanUtils.copyProperties(book, request);
            request.setBookId(book.getBookId());
            String wordSize = book.getWordSize();
            request.setWordSize(getGenerateNum(wordSize));

            String hot = book.getHot();
            request.setHot((long) getGenerateNum(hot));

            String favorites = book.getFavorites();
            request.setFavorites((long) getGenerateNum(favorites));

            request.setSummary(book.getSummary());
            requests.add(request);
        });
        return requests;
    }

    private double getGenerateNum(String value) {
        if (value != null && (value.contains("万字") || value.contains("万"))) {
            value = value.replace("万字", "").replace("万", "");
            if (!Objects.equals("", value)) {
                return Double.parseDouble(value) * 10000;
            }
        } else {
            try {
                return Double.parseDouble(value);
            } catch (Exception e) {
                log.error("数字转换异常，" + e.getMessage(), e);
            }
        }
        return 0;
    }

    private List<CreateBookChapterRequest> convertFromChapterDataObjectList(
        List<CrawlerBookChapterDO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.EMPTY_LIST;
        }
        List<CreateBookChapterRequest> requests = new ArrayList<>();
        list.forEach(chapterDO -> {
            CreateBookChapterRequest request = new CreateBookChapterRequest();
            request.setId(chapterDO.getId());
            request.setChapterId(chapterDO.getChapterId());
            request.setContentUrl(chapterDO.getContent());
            request.setTitle(chapterDO.getTitle());
            request.setBookId(chapterDO.getBookUuid());
            requests.add(request);
        });
        return requests;
    }
}
