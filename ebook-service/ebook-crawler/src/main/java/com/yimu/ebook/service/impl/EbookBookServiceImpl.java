package com.yimu.ebook.service.impl;

import static com.yimu.ebook.constant.Constants.SUMMARY_MAX_SIZE;

import com.yimu.ebook.dao.ebook.BookChapterDOMapper;
import com.yimu.ebook.dao.ebook.BookDOMapper;
import com.yimu.ebook.domain.dataobject.ebook.BookChapterDO;
import com.yimu.ebook.domain.dataobject.ebook.BookDO;
import com.yimu.ebook.domain.model.BookAudit;
import com.yimu.ebook.domain.vo.CreateBookChapterRequest;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.CrawlerBookChapterService;
import com.yimu.ebook.service.CrawlerBookService;
import com.yimu.ebook.service.EbookBookService;
import com.yimu.ebook.util.AesUtils;
import com.yimu.ebook.util.CosUtils;
import com.yimu.ebook.util.RedisKeyUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Service
@Slf4j
public class EbookBookServiceImpl implements EbookBookService {

    private final BookDOMapper bookDOMapper;

    private final BookChapterDOMapper bookChapterDOMapper;

    private final CrawlerBookChapterService crawlerBookChapterService;

    @Autowired
    private CrawlerBookService crawlerBookService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    public EbookBookServiceImpl(BookDOMapper bookDOMapper, BookChapterDOMapper bookChapterDOMapper, CrawlerBookChapterService crawlerBookChapterService) {
        this.bookDOMapper = bookDOMapper;
        this.bookChapterDOMapper = bookChapterDOMapper;
        this.crawlerBookChapterService = crawlerBookChapterService;
    }


    @Override
    public List<BookChapterDO> findAll() {
        List<BookChapterDO> bookChapterDOS = bookChapterDOMapper.selectAll();
        return bookChapterDOS;
    }

    @Override
    @Transactional(rollbackOn = RuntimeException.class)
    public void saveOrUpdate(CreateBookRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        BookDO bookDO = convertFromCreateBookRequest(request);
        List<BookDO> list = bookDOMapper.selectByUuid(bookDO.getBookUuid());
        int state;
        if (!CollectionUtils.isEmpty(list)) {
            BookDO old = list.get(0);
            bookDO.setId(old.getId());
            state = bookDOMapper.updateByPrimaryKeySelective(bookDO);
        } else {
            bookDO.setActive(BookAudit.NO_AUDIT.getId());
            bookDO.setCreateTime(new Date());
            bookDO.setUpdateTime(bookDO.getCreateTime());
            state = bookDOMapper.insertSelective(bookDO);
        }

        if(state <= 0) {
            throw new RuntimeException("保存书籍失败");
        }
        redisTemplate.opsForValue().set(RedisKeyUtils.checkBookUpdate(bookDO.getId()), bookDO.getUpdateTime());
    }

    @Override
    @Transactional(rollbackOn = RuntimeException.class)
    public void saveOrUpdate(CreateBookChapterRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }

        List<BookDO> list = bookDOMapper.selectByUuid(request.getBookId());
        if (CollectionUtils.isEmpty(list)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "书籍不存在");
        }
        BookDO bookDO = list.get(0);
        List<BookChapterDO> chapterDOList = bookChapterDOMapper.selectByBookIdAndChapterId(bookDO.getId(), request.getChapterId());
        if (!CollectionUtils.isEmpty(chapterDOList)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "章节已存在");
        }
        BookChapterDO chapterDO;
        try {
            chapterDO = convertFromCreateChapterRequest(request);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "保存章节内容失败");
        }
        chapterDO.setBookId(bookDO.getId());
        int state = bookChapterDOMapper.insertSelective(chapterDO);
        if (state <= 0) {
            throw new RuntimeException("保存章节失败");
        }

    }

    @Override
    @Transactional(rollbackOn = BusinessException.class)
    public int saveOrUpdateBatch(List<CreateBookChapterRequest> requests) throws BusinessException {
        if (CollectionUtils.isEmpty(requests)) {
            log.info("无更新的章节");
            return 0;
        }
        List<Long> needUpdateSyncToCrawler = saveChapters(requests);
        if (!CollectionUtils.isEmpty(needUpdateSyncToCrawler)) {
            crawlerBookChapterService.updateSyncByBatch(needUpdateSyncToCrawler);
        } else {
            return 0;
        }
        return 1;
    }

    @Override
    public int saveOrUpdateBatchNotUpdateSync(List<CreateBookChapterRequest> requests)
        throws BusinessException {
        if (CollectionUtils.isEmpty(requests)) {
            log.info("无更新的章节");
            return 0;
        }
        saveChapters(requests);
        return 1;
    }

    @Override
    @Transactional(rollbackOn = BusinessException.class)
    public int updateBookAndChapter(List<CreateBookChapterRequest> unSyncChapterRequestList,
        CreateBookRequest unSyncBookRequest) {
        // 保存章节内容
        saveOrUpdateBatch(unSyncChapterRequestList);
        // 保存书籍内容
        saveOrUpdate(unSyncBookRequest);
        // 修改crawler中sync=1
        crawlerBookService.updateSync(unSyncBookRequest.getBookId());
        return 1;
    }

    private List<Long> saveChapters(List<CreateBookChapterRequest> requests) {
        // 保存要入库的章节内容
        List<BookChapterDO> needStoreToEbook = new ArrayList<>();
        // 对已经入库但是未将数据的同步状态置为1的记录
        List<Long> needUpdateSyncToCrawler = new ArrayList<>();
        // 查询所有的书籍内容
        Set<String> bookIds = requests.stream().map(CreateBookChapterRequest::getBookId)
            .collect(Collectors.toSet());
        List<BookDO> existsBooks = Optional.ofNullable(bookDOMapper.selectByBookUuids(bookIds))
            .orElse(Collections.EMPTY_LIST);
        Set<String> bookUuidSet = existsBooks.stream().map(BookDO::getBookUuid)
            .collect(Collectors.toSet());
        log.info("查询到所有的book_uuid: {}", bookUuidSet);
        requests.forEach(request -> {
            if (!request.validate()) {
                log.error("更新失败！章节内容参数有问题：{}", request);
                return;
            }
            if (!bookUuidSet.contains(request.getBookId())) {
                log.error("更新失败！未查询到章节对应的书籍：bookUuid = {}", request.getBookId());
                return;
            }
            List<BookDO> bookDOSet = existsBooks.stream().filter(bookDO1 -> Objects
                .equals(bookDO1.getBookUuid(), request.getBookId())).collect(Collectors.toList());
            if (CollectionUtils.isEmpty(bookDOSet)) {
                log.error("更新失败！未查询到章节对应的书籍：[bookId/chapterId]={}/{}", request.getBookId(), request.getChapterId());
                return;
            }
            BookDO bookDO = bookDOSet.get(0);

            BookChapterDO chapterDO;
            try {
                System.out.println(" " +request.getChapterId());
                chapterDO = convertFromCreateChapterRequest(request);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return;
            }
            // TODO 已存在的章节重新覆盖
            List<BookChapterDO> chapterDOList = bookChapterDOMapper.selectByBookIdAndChapterId(bookDO.getId(), request.getChapterId());
            if (!CollectionUtils.isEmpty(chapterDOList)) {
                log.error("更新失败！章节已存在：[bookId/chapterId]={}/{}", request.getBookId(), request.getChapterId());
                // TODO 更改同步状态
                needUpdateSyncToCrawler.add(request.getId());
                return;
            }
            chapterDO.setBookId(bookDO.getId());
            needStoreToEbook.add(chapterDO);
        });
        if (!CollectionUtils.isEmpty(needStoreToEbook)) {
            // 批量插入章节内容
            int state = bookChapterDOMapper.insertBatch(needStoreToEbook);
            if (state <= 0) {
                log.error("章节内容入库失败：{}", needStoreToEbook);
                throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "章节保存失败");
            }
            // 修改书籍内容
            // 将crawler中的章节sync修改为1
            if (state == needStoreToEbook.size()) {
                Set<Long> saveChapters = needStoreToEbook.stream().map(BookChapterDO::getId)
                    .collect(Collectors.toSet());
                saveChapters.addAll(needUpdateSyncToCrawler);
                needUpdateSyncToCrawler.clear();
                needUpdateSyncToCrawler.addAll(saveChapters);
                // TODO 可以在这更新书籍的信息
            }
        }
        return needUpdateSyncToCrawler;
    }

    private BookChapterDO convertFromCreateChapterRequest(CreateBookChapterRequest request) throws Exception {

        BookChapterDO chapterDO = new BookChapterDO();
        BeanUtils.copyProperties(request, chapterDO);
        chapterDO.setCreateTime(new Date());
        String content = AesUtils.encrypt(request.getContentUrl(), AesUtils.AES_KEY);
        String paths = request.getBookId() + "/" + request.getChapterId() + "/" +
                request.getBookId() + "-" + request.getChapterId();
        String url = CosUtils.upload(CosUtils.BUCKET_EBOOK2, paths, content);
        if (null == url) {
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "存储COS失败");
        }
        chapterDO.setContentUrl(url);
        return chapterDO;
    }

    private BookDO convertFromCreateBookRequest(CreateBookRequest request) {
        BookDO bookDO = new BookDO();
        BeanUtils.copyProperties(request, bookDO);
        bookDO.setBookUuid(request.getBookId());
        if (request.getScore() != null) {
            bookDO.setScore(new BigDecimal(request.getScore()));
        }
        if (request.getWordSize() != null) {
            bookDO.setWordSize(new BigDecimal(request.getWordSize()));
        }
        String summary = request.getSummary();
        if (summary.length() > SUMMARY_MAX_SIZE) {
            summary = summary.substring(0, SUMMARY_MAX_SIZE);
            bookDO.setSummary(summary);
        }
        bookDO.setUpdateTime(new Date());
        return bookDO;
    }
}
