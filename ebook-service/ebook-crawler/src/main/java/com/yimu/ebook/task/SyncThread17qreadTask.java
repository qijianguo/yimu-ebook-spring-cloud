package com.yimu.ebook.task;

import com.yimu.ebook.domain.vo.BookChapterInfoResponse;
import com.yimu.ebook.domain.vo.BookChaptersResponse;
import com.yimu.ebook.domain.vo.BookInfoResponse;
import com.yimu.ebook.domain.vo.BookListResponse;
import com.yimu.ebook.domain.vo.CreateBookChapterRequest;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.EbookBookService;
import com.yimu.ebook.service.Third17qreadService;
import com.yimu.ebook.util.CosUtils;
import com.yimu.ebook.util.FileUtil;
import com.yimu.ebook.util.RedisKeyUtils;
import com.yimu.ebook.util.SortUtils;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Slf4j
@Component
public class SyncThread17qreadTask {


    private static final String SID = "100000014";
    private static final String KEY = "yghjhgjhg";
    private static final String PREFIX_BOOK_ID = "17qread";
    private final Third17qreadService third17qreadService;
    private final RedisTemplate redisTemplate;
    private final EbookBookService ebookBookService;

    private static final Integer SUCCESS = 200;

    public SyncThread17qreadTask(Third17qreadService third17qreadService, RedisTemplate redisTemplate, EbookBookService ebookBookService) {
        this.third17qreadService = third17qreadService;
        this.redisTemplate = redisTemplate;
        this.ebookBookService = ebookBookService;
    }

    /**
     *
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void sync() {
        String sign = third17qreadService.getSign(KEY, SID);

        // 3:获取章节
        Optional.ofNullable(getBookInfoList(sign)).orElse(Collections.emptyList()).forEach(bookInfo -> {
            log.info("处理书籍信息：bookId={}, bookName={}", bookInfo.getBookId(), bookInfo.getBook_name());
            BookChaptersResponse bookChaptersResponse;
            try {
                bookChaptersResponse = third17qreadService.bookChapters(bookInfo.getBookId(), SID, sign);
            } catch (BusinessException e) {
                return;
            }
            if (bookChaptersResponse == null || bookChaptersResponse.getCode() != SUCCESS || CollectionUtils.isEmpty(bookChaptersResponse.getData())) {
                log.info("未查询到书籍：bookId【{}】对应的章节", bookInfo.getBookId());
                return;
            }
            // 筛选掉已经存储过的章节
            List<BookChaptersResponse.DataBean.ChapterlistBean> chapterlist = filterBookChapter(bookInfo, bookChaptersResponse.getData());
            if (CollectionUtils.isEmpty(chapterlist)) {
                return;
            }

            // 4:获取章节内容
            AtomicBoolean success = new AtomicBoolean(false);
            List<BookChaptersResponse.DataBean.ChapterlistBean> chapterlistBeans = new ArrayList<>();
            chapterlist.forEach(chapter -> {
                if (success.get()) {
                    return;
                }
                BookChapterInfoResponse bookChapterInfoResponse;
                try {
                    bookChapterInfoResponse = third17qreadService.bookChapterInfo(bookInfo.getBookId(), chapter.getChapter_id(), SID, sign);
                } catch (BusinessException e) {
                    return;
                }
                if (bookChapterInfoResponse == null || bookChapterInfoResponse.getCode() != SUCCESS || bookChapterInfoResponse.getData() == null || bookChapterInfoResponse.getData().getContent() == null) {
                    log.info("未查询到书籍 bookId={}, chapterId={}对应的章节内容", bookInfo.getBookId(), chapter.getChapter_id());
                    return;
                }
                String content = bookChapterInfoResponse.getData().getContent();
                content = content
                    .replace("\r\n\r\n    ", "<p/>&nbsp;&nbsp;&nbsp;&nbsp;")
                    .replace("\n\n    ", "<p/>&nbsp;&nbsp;&nbsp;&nbsp;")
                    .replace("\n    ", "<p/>&nbsp;&nbsp;&nbsp;&nbsp;")
                    .replace("\n", "<p/>&nbsp;&nbsp;&nbsp;&nbsp;");
                chapter.setContentUrl(content);
                chapterlistBeans.add(chapter);
                System.out.print("  " + chapterlistBeans.size());
            });
            // 5:保存章节
            try {
                int state = ebookBookService.saveOrUpdateBatchNotUpdateSync(convertFromChapterlistBeanList(bookInfo, chapterlistBeans));
                if (state != 0) {
                    // 将书籍的最近一次最大章节存入redis中，方便下一次筛选
                    int max = getMaxChapterId(chapterlistBeans);
                    String redisKey = RedisKeyUtils.third17QreadMaxChapter(PREFIX_BOOK_ID + bookInfo.getBookId());
                    redisTemplate.opsForValue().set(redisKey, max);
                }
            } catch (BusinessException e) {
                e.printStackTrace();
                log.error("保存bookId=【{}】章节内容失败", bookInfo.getBookId());
                return;
            }
            // 6:保存书籍
            try {
                // TODO 去掉封面下载暂时
                /*String url = downloadPicture(bookInfo.getCover());
                if (url != null) {
                    url = url.replace("https://www.17qread.com/imagedata/",
                        "http://ebook-image-1259465809.file.myqcloud.com/cover/");
                    bookInfo.setCover(url);
                }*/
                ebookBookService.saveOrUpdate(convertFromDataObjectList(bookInfo));
            } catch (BusinessException e) {
                e.printStackTrace();
                log.error(e.getMessage(), e);
            }
        });
    }

    /**
     * 获取书籍详情列表
     */
    private List<BookInfoResponse.DataBean> getBookInfoList(String sign) {
        // 1:获取到所有的授权书籍
        BookListResponse bookListResponse;
        try {
            bookListResponse = third17qreadService.bookList(null, SID, sign);
        } catch (BusinessException e) {
            return null;
        }
        if (bookListResponse == null || bookListResponse.getCode() != SUCCESS || CollectionUtils.isEmpty(bookListResponse.getData())) {
            log.info("未查询到书籍，直接返回");
            return null;
        }

        // 2:获取书籍的详细内容
        List<BookInfoResponse.DataBean> bookInfoList = new ArrayList<>();

        List<BookListResponse.DataBean> data = bookListResponse.getData();
        data.forEach(book -> {
            BookInfoResponse bookInfoResponse;
            try {
                bookInfoResponse = third17qreadService.bookInfo(book.getBookId(), SID, sign);
            } catch (BusinessException e) {
                return;
            }
            if (bookInfoResponse == null || bookInfoResponse.getCode() != SUCCESS || bookInfoResponse.getData() == null) {
                log.info("未查询到书籍：【{}】的详细内容", book);
                return;
            }
            bookInfoList.add(bookInfoResponse.getData());
        });
        return bookInfoList;
    }

    private int getMaxChapterId( List<BookChaptersResponse.DataBean.ChapterlistBean> chapterlistBeans) {
        if (CollectionUtils.isEmpty(chapterlistBeans)) {
            return 0;
        }
        List<String> collect = chapterlistBeans.stream().map(BookChaptersResponse.DataBean.ChapterlistBean::getChapter_id).collect(Collectors.toList());
        int[] arr = new int[collect.size()];
        for (int i = 0; i < collect.size(); i++) {
            arr[i] = Integer.parseInt(collect.get(i));
        }
        int[] ints = SortUtils.shellSort(arr);
        return ints[ints.length -1];
    }

    private List<CreateBookChapterRequest> convertFromChapterlistBeanList(BookInfoResponse.DataBean bookInfo, List<BookChaptersResponse.DataBean.ChapterlistBean> chapterlistBeans) {
        List<CreateBookChapterRequest> requests = new ArrayList<>();
        chapterlistBeans.forEach(chapter -> {
            CreateBookChapterRequest request = new CreateBookChapterRequest();
            request.setBookId(PREFIX_BOOK_ID + bookInfo.getBookId());
            request.setTitle(chapter.getChapter_name());
            request.setContentUrl(chapter.getContentUrl());
            request.setCreateTime(new Date(chapter.getUpdate_time()));
            request.setChapterId(Integer.parseInt(chapter.getChapter_id()));
            requests.add(request);
        });
        return requests;
    }

    private CreateBookRequest convertFromDataObjectList(BookInfoResponse.DataBean book) {
        CreateBookRequest request = new CreateBookRequest();
        request.setBookId(PREFIX_BOOK_ID + book.getBookId());
        request.setName(book.getBook_name());
        request.setAuthor(book.getAuthor());
        request.setCover(book.getCover());
        request.setWordSize(Double.parseDouble(book.getWords()));
        request.setHot(0L);
        request.setFavorites(0L);
        request.setScore(5.5);
        request.setSummary(book.getBrief());
        request.setCreateTime(new Date());
        request.setUpdateTime(new Date(book.getUpdate_time()));
        request.setCategoryId(getCategoryId(book.getGroup_id()));
        request.setSubCategoryId(0);
        request.setChannelId(getChannelId(book.getGroup_id()));
        request.setBookStatusId(book.getStatus());
        request.setIpControl(2);
        return request;
    }

    private List<BookChaptersResponse.DataBean.ChapterlistBean> filterBookChapter(BookInfoResponse.DataBean bookInfo, List<BookChaptersResponse.DataBean> data) {
        // 在redis总未查询到书籍章节相关信息， 说明未记录过，则直接返回
        int latestChapterId = getThread17QreadBookIdLatestUpdateChapterId(bookInfo.getBookId());
        List<BookChaptersResponse.DataBean.ChapterlistBean> convert = convert(data);
        if (latestChapterId == 0) {
            return convert;
        }
        // 过滤
        convert = convert.stream().filter(chapter -> {
            if (Integer.parseInt(chapter.getChapter_id()) <= latestChapterId) {
                // FIXME 建议将书籍的id和章节ID都存入到redis中，如果存在再进行过滤，不然可能会有一些问题
                log.info("书籍：{} -- 章节：{}已经存在, 过滤", bookInfo, chapter);
                return false;
            }
            return true;

        }).collect(Collectors.toList());
        return convert;
    }

    private List<BookChaptersResponse.DataBean.ChapterlistBean> convert(List<BookChaptersResponse.DataBean> data) {

        List<List<BookChaptersResponse.DataBean.ChapterlistBean>> list = new ArrayList<>();
        data.forEach(dataBean -> {
            if (CollectionUtils.isEmpty(dataBean.getChapterlist())) {
                log.info("无章节内容【{}】", dataBean);
            }
            list.add(dataBean.getChapterlist());
        });
        return CollectionUtils.isEmpty(list) ? Collections.emptyList() : list.get(0);
    }

    private int getThread17QreadBookIdLatestUpdateChapterId(String bookId) {
        String redisKey = RedisKeyUtils.third17QreadMaxChapter(PREFIX_BOOK_ID + bookId);
        Object obj = redisTemplate.opsForValue().get(redisKey);
        if (obj instanceof Integer) {
            return (int) obj;
        }
        return 0;
    }

    /**
     * 链接imgUrl下载图片
     */
    private String downloadPicture(String imgUrl) {
        String path = imgUrl.replace("https://www.17qread.com/imagedata", "cover")
            .replace("http://www.dongyuedu.com/imagedata", "cover")
            .replace("https://www.17qread.com/images", "cover");
        URL url;
        try {
            FileUtil.createFile(path);
            url = new URL(imgUrl);
            DataInputStream dataInputStream = new DataInputStream(url.openStream());
            File file = new File(path);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            fileOutputStream.write(output.toByteArray());
            fileOutputStream.close();
            CosUtils.simpleUploadFile("ebook-image", path, file);
            dataInputStream.close();
            return imgUrl;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        return "";
    }

    private int getCategoryId(String groupId) {
        switch (groupId) {
            case "1001":
                return 24;
            case "1002":
                return 3;
            case "1003":
                return 23;
            case "1004":
                return 17;
            case "1005":
                return 6;
            case "1006":
                return 9;
            case "1007":
                return 12;
            case "1008":
                return 18;
            case "1009":
                return 4;
            case "1010":
                return 1;
            case "1011":
                return 11;
            default:
                return 0;
        }
    }

    private int getChannelId(String groupId) {
        switch (groupId) {
            // 男频
            case "1001":
            case "1002":
            case "1003":
            case "1004":
            case "1005":
            case "1006":
            case "1007":
                return 1;
            // 女频
            case "1008":
            case "1009":
            case "1010":
            case "1011":
                return 2;
            default:
                return 0;
        }
    }

}
