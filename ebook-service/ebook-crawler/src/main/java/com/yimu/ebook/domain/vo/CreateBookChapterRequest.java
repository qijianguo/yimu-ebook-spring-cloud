package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateBookChapterRequest {

    private Long id;

    private Integer chapterId;

    private String title;

    private String contentUrl;

    private String bookId;

    private Date createTime;

    public boolean validate() {
        return chapterId != null && bookId != null && title != null && contentUrl != null;
    }
}
