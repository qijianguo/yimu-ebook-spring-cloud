package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookListResponse {


    /**
     * code : 200
     * data : [{"bookId":"281","book_name":"试传文件2","update_time":"1510991069"},{"bookId":"282","book_name":"试传文件22","update_time":"1510992075"}]
     * message : Success
     */

    private int code;
    private String message;
    private List<DataBean> data;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DataBean {
        /**
         * bookId : 281
         * book_name : 试传文件2
         * update_time : 1510991069
         */

        private String bookId;
        private String book_name;
        private long update_time;

    }
}
