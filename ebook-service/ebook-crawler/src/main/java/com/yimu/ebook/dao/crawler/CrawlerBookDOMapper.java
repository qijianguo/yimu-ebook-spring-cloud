package com.yimu.ebook.dao.crawler;

import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDO;
import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDOExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CrawlerBookDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    int deleteByPrimaryKey(String bookId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    int insert(CrawlerBookDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    int insertSelective(CrawlerBookDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    List<CrawlerBookDO> selectByExample(CrawlerBookDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    CrawlerBookDO selectByPrimaryKey(String bookId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    int updateByPrimaryKeySelective(CrawlerBookDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_book
     *
     * @mbg.generated Tue Jun 11 13:48:25 CST 2019
     */
    int updateByPrimaryKey(CrawlerBookDO record);

    List<CrawlerBookDO> selectBySync(Integer sync);


    int updateSyncByBatch(@Param("bookIds") List<String> bookIds);


    int updateSync(String bookId);
}