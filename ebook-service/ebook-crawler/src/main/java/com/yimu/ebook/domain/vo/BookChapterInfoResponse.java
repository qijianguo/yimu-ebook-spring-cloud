package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookChapterInfoResponse {


    /**
     * code : 200
     * data : {"content":"　　\u201c以前他还会脸红呢。\u201d罗娜感叹道，\u201c青春一去不复返啊。\u201d\n　　吴泽冷笑了一声。\n　　两周后，段宇成出发参加田径大奖赛第一站。"}
     * message : Success
     */

    private int code;
    private DataBean data;
    private String message;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DataBean {
        /**
         * content : 　　“以前他还会脸红呢。”罗娜感叹道，“青春一去不复返啊。”
         　　吴泽冷笑了一声。
         　　两周后，段宇成出发参加田径大奖赛第一站。
         */

        private String content;
    }
}
