package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BookInfoResponse {


    /**
     * code : 200
     * data : {"bookId":"61","book_name":"这是一本测试小说","author":"测试作者","brief":"测试简介\u201d","words":"710092","keywords":"","cover":"http://api.17qread.com/images/nocover.jpg","group_id":"1008","cate_id":"1008002","is_vip":1,"update_time":"1542770256","status":1}
     * message : Success
     */

    private int code;
    private DataBean data;
    private String message;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    public static class DataBean {
        /**
         * bookId : 61
         * book_name : 这是一本测试小说
         * author : 测试作者
         * brief : 测试简介”
         * words : 710092
         * keywords :
         * cover : http://api.17qread.com/images/nocover.jpg
         * group_id : 1008
         * cate_id : 1008002
         * is_vip : 1
         * update_time : 1542770256
         * status : 1
         */

        private String bookId;
        private String book_name;
        private String author;
        private String brief;
        private String words;
        private String keywords;
        private String cover;
        private String group_id;
        private String cate_id;
        private int is_vip;
        private long update_time;
        private int status;

    }

}
