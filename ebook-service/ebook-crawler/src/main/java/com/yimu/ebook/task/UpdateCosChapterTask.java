package com.yimu.ebook.task;

import com.google.common.base.Charsets;
import com.yimu.ebook.domain.dataobject.ebook.BookChapterDO;
import com.yimu.ebook.service.EbookBookService;
import com.yimu.ebook.util.AesUtils;
import com.yimu.ebook.util.CosUtils;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 修改Cos中的章节内容
 * @author Angus
 */
@Slf4j
@Component
public class UpdateCosChapterTask {

  @Autowired
  private EbookBookService ebookBookService;

  @Scheduled(cron = "0/10 * * * * ?")
  public void sync() {
    List<BookChapterDO> all = Optional.ofNullable(ebookBookService.findAll()).orElse(
        Collections.emptyList());
    all.forEach(bookChapterDO -> {
      // FIXME 应该是book_uuid
//      String paths = bookChapterDO.getBookId() + "/" + bookChapterDO.getChapterId() + "/" +
//          bookChapterDO.getBookId() + "-" + bookChapterDO.getChapterId();
      String paths = bookChapterDO.getContentUrl();
      if (paths != null) {
        if (paths.contains("http://ebook-1259465809.file.myqcloud.com/")) {
          paths = paths.replace("http://ebook-1259465809.file.myqcloud.com/", "");
        }
      }
      String downloadContent = CosUtils.downloadContent(CosUtils.BUCKET_EBOOK2, paths);
      if (downloadContent != null) {
        String dec = AesUtils.decrypt(downloadContent.replace("\r\n", "").replace("\n", ""), AesUtils.AES_KEY);
        System.out.println(dec);
      }
    });


  }


}
