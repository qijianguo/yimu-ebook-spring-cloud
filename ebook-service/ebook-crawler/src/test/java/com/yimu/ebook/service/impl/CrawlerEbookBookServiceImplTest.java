package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookDO;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CrawlerBookService;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CrawlerEbookBookServiceImplTest {

    @Autowired
    private CrawlerBookService crawlerBookService;

    @Test
    public void getBySync() throws BusinessException {
        List<CrawlerBookDO> bySync = crawlerBookService.getBySync(0);
        Assert.assertNotEquals(0, bySync.size());
    }

    @Test
    public void updateSyncByBatch() throws BusinessException {
        List<String> bookIds = Collections.singletonList("10025");
        crawlerBookService.updateSyncByBatch(bookIds);
    }
}