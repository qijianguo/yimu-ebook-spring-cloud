package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.vo.CreateBookChapterRequest;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.EbookBookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EbookBookServiceImplTest {

    @Autowired
    private EbookBookService ebookBookService;

    @Test
    public void saveOrUpdate() throws BusinessException {
        CreateBookRequest request = new CreateBookRequest();
        request.setBookId("111111");
        request.setName("第一行代码");
        request.setAuthor("Angus");
        request.setCover("http://img.redocn.com/sheji/20161122/xiandainongyenongchanpindianzishuhuacefengmian_7489517_small.jpg");
        request.setWordSize(333.12);
        request.setHot(11000000L);
        request.setFavorites(200000L);
        request.setScore(1.0);
        request.setScore(8.3);
        request.setChannelId(1);
        request.setCategoryId(1);
        request.setSubCategoryId(2);
        ebookBookService.saveOrUpdate(request);

    }

    @Test
    public void saveOrUpdate2() throws BusinessException {
        CreateBookChapterRequest request = new CreateBookChapterRequest();
        request.setBookId("10025");
        request.setChapterId(8);
        request.setTitle("第八章");
        request.setContentUrl("http://yapi.demo.qunar.com/mock/69536/chapter/content");
        ebookBookService.saveOrUpdate(request);
    }
}