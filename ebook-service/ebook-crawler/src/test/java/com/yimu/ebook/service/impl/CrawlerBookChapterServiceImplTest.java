package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.dataobject.crawler.CrawlerBookChapterDO;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CrawlerBookChapterService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CrawlerBookChapterServiceImplTest {

    @Autowired
    private CrawlerBookChapterService crawlerBookChapterService;

    @Test
    public void getBySync() throws BusinessException {
        List<CrawlerBookChapterDO> bySync = crawlerBookChapterService.getBySync(0, 1, 10);
        Assert.assertNotEquals(null, bySync);
    }

    @Test
    public void updateSyncByBatch() throws BusinessException {
        List<Long> list = Arrays.asList(0L, 1L, 2L);
        crawlerBookChapterService.updateSyncByBatch(list);
    }
}