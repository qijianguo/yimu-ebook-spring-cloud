package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.SubCategoryModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CategorySubService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategorySubServiceImplTest {

    @Autowired
    private CategorySubService categorySubService;

    @Test
    public void getByCategoryId() {
        List<SubCategoryModel> list = categorySubService.getByCategoryId(1);
        Assert.assertNotEquals(0, list.size());

    }

    @Test
    public void getByCategoryAndSub() throws BusinessException {
        SubCategoryModel model = categorySubService.getSubCategory( 1);
        Assert.assertNotEquals(null, model);

    }
}