package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.HotRankModel;
import com.yimu.ebook.domain.vo.HotRankRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BookRankService;
import com.yimu.ebook.util.TimeUtil;
import java.time.LocalDate;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BookRankServiceImplTest {

    @Autowired
    private BookRankService bookRankService;

    @Test
    public void putHotRecommend() throws BusinessException {

        bookRankService.followBook(1);
    }

    @Test
    public void getMonthHotRank() throws BusinessException {
        HotRankRequest request = new HotRankRequest();
        request.setChannelId(1);
        List<HotRankModel> models =  bookRankService.getMonthHotRank(request);
        Assert.assertNotEquals(0, models);
    }

    @Test
    public void  getWeekHotRank() throws BusinessException {
        HotRankRequest request = new HotRankRequest();
        request.setPageNum(1);
        request.setSize(10);
        request.setChannelId(1);
        List<HotRankModel> models =  bookRankService.getWeekHotRank(request);
        Assert.assertEquals(1, models.size());
    }

    @Test
    public void getNewbookHotRank() throws BusinessException {
        HotRankRequest request = new HotRankRequest();
        request.setPageNum(1);
        request.setSize(10);
        request.setChannelId(1);
        List<HotRankModel> models =  bookRankService.getNewbookHotRank(request);
        Assert.assertEquals(0, models.size());
    }

    @Test
    public void getEditorRecommend() throws BusinessException {

    }

    @Test
    public void getHotSearch() throws BusinessException {
        HotRankRequest request = new HotRankRequest();
        request.setPageNum(1);
        request.setSize(10);
        request.setChannelId(1);
        List<HotRankModel> models =  bookRankService.getHotSearch(request);
        Assert.assertEquals(0, models.size());
    }

    @Test
    public void getHotRank() throws BusinessException {

    }

    @Test
    public void deleteWeekRankBeforeTime() throws BusinessException {
        int i = bookRankService
            .deleteWeekRankBeforeTime(TimeUtil.convertLocalDate2Date(LocalDate.now()));
        System.out.println(i);
    }
}