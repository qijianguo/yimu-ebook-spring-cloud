package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AdminLoginRequest;
import com.yimu.ebook.domain.vo.AdminRequest;
import com.yimu.ebook.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AdminServiceImplTest {

  @Autowired
  private AdminService adminService;

  @Test
  public void saveOrUpdate() {
    AdminRequest request = new AdminRequest();
    request.setUsername("admin");
    request.setPassword("12345678");
    request.setRoleId(2);
    adminService.saveOrUpdate(request);
  }

  @Test
  public void login() {
    AdminLoginRequest request = new AdminLoginRequest();
    request.setUsername("admin");
    request.setPassword("12345678");
    UserModel login = adminService.login(request);
    Assert.assertNotEquals(null, login);
  }

}