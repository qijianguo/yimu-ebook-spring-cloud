package com.yimu.ebook.service.impl;

import static org.junit.Assert.*;

import com.yimu.ebook.domain.model.AboutUsModel;
import com.yimu.ebook.service.AboutUsService;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AboutUsServiceImplTest {

  @Autowired
  private AboutUsService aboutUsService;

  @Test
  public void getAll() {
    List<AboutUsModel> all = aboutUsService.getAll();
    Assert.assertNotEquals(0, all.size());
  }
}