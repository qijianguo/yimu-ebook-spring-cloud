package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateReadHistoryRequest;
import com.yimu.ebook.domain.vo.ReadHistoryRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.ReadHistoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReadHistoryServiceImplTest {

    @Autowired
    private ReadHistoryService readHistoryService;

    @Test
    public void add() throws BusinessException {

        CreateReadHistoryRequest request = new CreateReadHistoryRequest();
        List<ReadHistoryRequest> list = new ArrayList<>();
        list.add(new ReadHistoryRequest(1l, 1, 1l));
        list.add(new ReadHistoryRequest(3l,1, 1l));
        list.add(new ReadHistoryRequest(2l,2, 1l));
        request.setReadHistoryList(list);
        UserModel userModel = new UserModel();
        userModel.setId(1L);
        readHistoryService.saveOrUpdate(userModel, request);
    }
}