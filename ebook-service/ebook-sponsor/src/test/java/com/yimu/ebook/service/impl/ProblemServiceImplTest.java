package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.ProblemModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.ProblemService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProblemServiceImplTest {

    @Autowired
    private ProblemService problemService;

    @Test
    public void getAll() throws BusinessException {
        List<ProblemModel> all = problemService.getAll();
        Assert.assertNotEquals(0, all.size());
    }
}