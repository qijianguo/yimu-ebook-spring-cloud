package com.yimu.ebook.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RedisTest {

  @Autowired
  private RedisTemplate redisTemplate;

  @Test
  public void zset() {
    Long score1 = 1L;
    Long score2 = 2L;
    Long score3 = 3L;
    Long score4 = 4L;
    Long score5 = 5L;
    redisTemplate.opsForZSet().add("read:read_times", 1, score1);
    redisTemplate.opsForZSet().add("read:read_times", 3, score2);
    redisTemplate.opsForZSet().add("read:read_times", 5, score3);
    redisTemplate.opsForZSet().add("read:read_times", 2, score4);
    redisTemplate.opsForZSet().add("read:read_times", 7, score5);
    Object obj = redisTemplate.opsForZSet().reverseRange("read:read_times", 0, -1);
    System.out.println("顺序存取：" + obj);
    Object obj2 = redisTemplate.opsForZSet().reverseRangeByScore("read:read_times", 0, 5, 0, -1);
    System.out.println("从0-10之间倒叙打印：" + obj2);

  }
}