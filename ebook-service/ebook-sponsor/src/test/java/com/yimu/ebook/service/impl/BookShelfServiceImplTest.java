package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.BookShelfModel;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AddBookShelfRequest;
import com.yimu.ebook.domain.vo.BookShelfRequest;
import com.yimu.ebook.domain.vo.DeleteBookShelfRequest;
import com.yimu.ebook.domain.vo.GetBookShelfRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BookShelfService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookShelfServiceImplTest {

    @Autowired
    private BookShelfService bookShelfService;

    @Test
    public void addBookShelf() throws BusinessException {
        AddBookShelfRequest request = new AddBookShelfRequest();

        List<BookShelfRequest> shelves = new ArrayList<>();
        shelves.add(new BookShelfRequest(1l, 1L));
        shelves.add(new BookShelfRequest(2l, 100000000L));
        shelves.add(new BookShelfRequest(3l, 1000000L));

        request.setShelves(shelves);
        UserModel userModel = new UserModel();
        userModel.setId(1L);
        bookShelfService.addBookShelf(userModel, request);
    }

    @Test
    public void getByUserId() throws BusinessException {

        GetBookShelfRequest request = new GetBookShelfRequest();
        UserModel userModel = new UserModel();
        userModel.setId(1L);
        List<BookShelfModel> list = bookShelfService.getByUserId(userModel, request);
        // Assert.assertNotEquals(0, list.size());

    }

    @Test
    public void deleteBookShelf() throws BusinessException {
        DeleteBookShelfRequest request =
                new DeleteBookShelfRequest(
                        Arrays.asList(1l, 2l, 3l)
                );
        UserModel userModel = new UserModel();
        userModel.setId(1L);
        bookShelfService.deleteBookFromShelf(userModel, request);
    }
}