package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.dataobject.BookChapterDO;
import com.yimu.ebook.domain.dataobject.BookSizeByCategoryDo;
import com.yimu.ebook.domain.model.BookModel;
import com.yimu.ebook.domain.vo.BookRequest;
import com.yimu.ebook.domain.vo.CreateBookChapterRequest;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.domain.vo.SearchRequest;
import com.yimu.ebook.domain.vo.SearchResponse;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BookService;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class BookServiceImplTest {

    @Autowired
    private BookService bookService;


    @Test
    public void getBookListByCategoryAndChannelId() throws BusinessException {
        BookRequest request = new BookRequest();
        request.setCategoryId(1);
        request.setChannelId(1);
        request.setChannelId(1);
        request.setIpControl(1);
        List<BookModel> bookModels = bookService.getBookListByCondition(request);
        // Assert.assertNotEquals(0, bookModels.size());
    }

    @Test
    public void getBookDetailByUuid() throws BusinessException {
        BookModel bookModel = bookService.getBookDetailByUuid("111111");
        Assert.assertNotEquals(null, bookModel);
    }

    @Test
    public void getBookDetailById() throws BusinessException {
        BookModel bookModel = bookService.getBookDetailById((long) 1);
        Assert.assertNotEquals(null, bookModel);
    }

    @Test
    public void setSolr() throws BusinessException {
        bookService.setSolr();
    }

    @Test
    public void getByRegexp() throws BusinessException {
        String regexp = "^《";
        List<BookChapterDO> byRegexp = bookService.getByRegexp(regexp);
    }
    @Test
    public void updateChapterByRegexp() throws BusinessException {
        String regexp = "^《";
        bookService.updateChapterByRegexp(regexp);
    }

    @Test
    public void searchFromSolr() throws BusinessException, IOException, SolrServerException {

        for (int i = 0; i < 10000; i++) {
            SearchRequest request = new SearchRequest();
            request.setKeyword("网恋");
            List<SearchResponse> searchResponses = bookService.searchFromSolr(request);
            System.out.println(searchResponses.size());
        }
    }

    @Test
    public void saveOrUpdate() throws BusinessException {
        CreateBookRequest request = new CreateBookRequest();
        request.setBookId(0L);
        request.setName("书籍fff");
        request.setAuthor("作者fff");
        request.setCover("http://img.redocn.com/sheji/20161122/xiandainongyenongchanpindianzishuhuacefengmian_7489517_small.jpg");
        request.setWordSize(16D);
        request.setHot(100L);
        request.setFavorites(1000L);
        request.setScore(9.0D);
        request.setSummary("简介");
        request.setUpdateTime(new Date());
        request.setCategoryId(1);
        request.setSubCategoryId(1);
        request.setChannelId(1);
        request.setBookStatusId(1);
        bookService.update(request);
    }

    @Test
    public void saveOrUpdate2() throws BusinessException {
        CreateBookChapterRequest request = new CreateBookChapterRequest();
        request.setBookId("10025");
        request.setChapterId(1);
        request.setTitle("第八章");
        request.setContentUrl("http://yapi.demo.qunar.com/mock/69536/chapter/content");
        bookService.update(request);
    }

    @Test
    public void getBookSizeByCategory(){
        List<BookSizeByCategoryDo> bookSizeByCategory = bookService.getBookSizeByCategory();
        Assert.assertNotEquals(0f, bookSizeByCategory.size());
    }
}