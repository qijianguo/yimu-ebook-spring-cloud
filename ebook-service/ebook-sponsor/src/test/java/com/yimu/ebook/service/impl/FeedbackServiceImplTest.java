package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateFeedbackRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.FeedbackService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeedbackServiceImplTest {

    @Autowired
    private FeedbackService feedbackService;

    @Test
    public void save() throws BusinessException {
        CreateFeedbackRequest request = new CreateFeedbackRequest();
        request.setContact("17521226604");
        request.setContent("广告太多，能不能少点啊");
        UserModel userModel = new UserModel();
        userModel.setId(1L);
        feedbackService.save(userModel, request);
    }
}