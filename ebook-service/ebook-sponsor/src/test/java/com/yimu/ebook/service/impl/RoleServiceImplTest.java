package com.yimu.ebook.service.impl;

import static org.junit.Assert.*;

import com.yimu.ebook.domain.vo.RoleRequest;
import com.yimu.ebook.domain.vo.UpdateRoleRequest;
import com.yimu.ebook.service.RoleService;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoleServiceImplTest {
  @Autowired
  private RoleService roleService;

  @Test
  public void saveRole() {
    RoleRequest request = new RoleRequest();
    request.setName("超级管理员");
    request.setDescription("查看所有书籍，修改/删除等操作");
    request.setPermissionIds(Arrays.asList(100, 101, 200, 201, 300, 301, 302, 400, 401));
    roleService.saveRole(request);
  }

  @Test
  public void updateRole() {
    UpdateRoleRequest request = new UpdateRoleRequest();
    request.setId(1);
    request.setName("超级管理员");
    request.setDescription("所有权限");
    request.setPermissionIds(Arrays.asList(100, 101, 200, 201, 300, 301, 302, 400, 401));
    roleService.updateRole(request);
  }

  @Test
  public void deleteRole() {
    roleService.deleteRole(1);
  }
}