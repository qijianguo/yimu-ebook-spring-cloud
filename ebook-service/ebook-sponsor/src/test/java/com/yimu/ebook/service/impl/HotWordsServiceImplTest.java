package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.HotWordsModel;
import com.yimu.ebook.service.HotWordsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HotWordsServiceImplTest {

    @Autowired
    private HotWordsService hotWordsService;

    @Test
    public void getAll() {
        List<HotWordsModel> all = hotWordsService.getAll();
        Assert.assertNotEquals(0, all.size());
    }
}