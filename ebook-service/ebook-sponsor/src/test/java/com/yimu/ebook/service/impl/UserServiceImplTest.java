package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.LoginRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UserServiceImplTest {

    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void getTelphoneCode() throws BusinessException {
        String telphone = "17521226604";
        int code = userService.getTelphoneCode(telphone);
        Assert.assertNotEquals(0, code);
    }

    @Test
    public void login() throws BusinessException {
        LoginRequest request = new LoginRequest();
        request.setTelphone("17521226604");
        request.setCode(2859);
        // request.setPassword();
        UserModel userModel = userService.loginByTelphone(request);
        Assert.assertEquals(userModel.getName(), request.getTelphone());
    }
}