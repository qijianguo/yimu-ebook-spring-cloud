package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.AppVersionModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.AppVersionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppVersionServiceImplTest {

    @Autowired
    private AppVersionService appVersionService;

    @Test
    public void getByPlatform() throws BusinessException {
        AppVersionModel ios = appVersionService.getByPlatform("ios");
        Assert.assertNotEquals(null, ios);
    }
}