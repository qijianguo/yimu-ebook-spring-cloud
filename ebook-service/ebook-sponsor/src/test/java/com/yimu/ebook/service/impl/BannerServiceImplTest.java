package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.BannerModel;
import com.yimu.ebook.domain.model.BannerType;
import com.yimu.ebook.domain.vo.BannerRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BannerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BannerServiceImplTest {

    @Autowired
    private BannerService bannerService;

    @Test
    public void getAllBannerTypes() {
        List<BannerType> list = bannerService.getAllBannerTypes();
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void getBookCityBanner() throws BusinessException {
        BannerRequest request = new BannerRequest(1, 1);
        List<BannerModel> list = bannerService.getBannerByCondition(request);
        Assert.assertNotEquals(0, list.size());
    }
}