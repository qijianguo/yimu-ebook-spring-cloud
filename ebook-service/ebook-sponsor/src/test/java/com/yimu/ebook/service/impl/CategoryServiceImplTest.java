package com.yimu.ebook.service.impl;

import com.yimu.ebook.domain.model.CategoryModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CategoryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceImplTest {

    @Autowired
    private CategoryService categoryService;

    @Test
    public void getAll() {
        List<CategoryModel> list = categoryService.selectAllActive();
        Assert.assertNotEquals(0, list.size());
    }

    @Test
    public void getAllAndSub() {
        List<CategoryModel> list = categoryService.getAllAndSub();
        Assert.assertNotEquals(0, list.size());
        Assert.assertNotEquals(0, list.get(0).getSubCategoryModelList().size());
    }

    @Test
    public void getByCurrentUser() throws BusinessException {
        List<CategoryModel> list = categoryService.getByChannel(1);
        Assert.assertNotEquals(0, list.size());
    }

}