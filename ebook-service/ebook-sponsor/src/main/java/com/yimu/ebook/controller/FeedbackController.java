package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.annotation.CurrentUser;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateFeedbackRequest;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.FeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "feedback", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "意见反馈")
public class FeedbackController {

    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @ApiOperation(value = "查询版本")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "content", value = "反馈内容，字数限制在300字内", required = true, dataType = "varchar", paramType = "query"),
            @ApiImplicitParam(name = "contact", value = "联系方式，限制在15字内", required = true, dataType = "varchar", paramType = "query"),
    })
    @PostMapping
    @Authentication(roleIds = {Role.ALL})
    public Result get(@ApiIgnore @CurrentUser UserModel userModel, CreateFeedbackRequest request) throws BusinessException {
        feedbackService.save(userModel, request);
        return Result.success("反馈成功");
    }
}
