package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.domain.vo.RoleRequest;
import com.yimu.ebook.domain.vo.UpdateRoleRequest;
import com.yimu.ebook.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "role", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description= "角色管理")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "添加角色：如需添加角色请联系开发人员")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "name", value = "角色名称", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "description", value = "角色描述", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "permissionIds", value = "权限ID集合，id之间用英文逗号隔开", dataType = "string", required = true, paramType = "query"),
    })
    @PostMapping
    @Authentication(roleIds = Role.ALL)
    public Result save(RoleRequest request) {
        roleService.saveRole(request);
        return Result.success("添加成功");
    }

    @ApiOperation(value = "修改角色")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "name", value = "角色名称", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "description", value = "角色描述", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "permissionIds", value = "权限ID集合，id之间用英文逗号隔开", dataType = "string", required = true, paramType = "query"),
    })
    @PutMapping
    @Authentication(roleIds = Role.ADMIN)
    public Result update(UpdateRoleRequest request) {
        roleService.updateRole(request);
        return Result.success("修改成功");
    }

    @ApiOperation(value = "删除角色：如需修改请联系开发人员")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "name", value = "角色名称", required = true, dataType = "string", paramType = "query"),
    })
    @DeleteMapping
    @Authentication(roleIds = Role.ALL)
    public Result delete(Integer id) {
        roleService.deleteRole(id);
        return Result.success("修改成功");
    }

}
