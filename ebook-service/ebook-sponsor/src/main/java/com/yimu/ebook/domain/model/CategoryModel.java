package com.yimu.ebook.domain.model;

import lombok.Data;
import java.util.List;

@Data
public class CategoryModel {

    /**
     * 分类ID
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类默认图片
     */
    private String image;

    /**
     * 频道ID
     */
    private Integer channelId;

    private Integer active;


    private List<SubCategoryModel> subCategoryModelList;

}
