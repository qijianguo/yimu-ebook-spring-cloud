package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.annotation.CurrentUser;
import com.yimu.ebook.domain.model.ReadHistoryModel;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateReadHistoryRequest;
import com.yimu.ebook.domain.vo.GetHistoryRequest;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.ReadHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "/readHistory", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "阅读历史")
public class ReadHistoryController {

    private final ReadHistoryService readHistoryService;

    @Autowired
    public ReadHistoryController(ReadHistoryService readHistoryService) {
        this.readHistoryService = readHistoryService;
    }

    @ApiOperation(value = "批量添加阅读历史")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "bookId", value = "书籍ID", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "chapterId", value = "章节ID", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "time", value = "时间，时间戳", dataType = "long", paramType = "query"),
    })
    @PostMapping
    @Authentication(roleIds = {Role.ALL})
    public Result save(@CurrentUser UserModel userModel, @RequestBody CreateReadHistoryRequest request) throws BusinessException {
        readHistoryService.saveOrUpdate(userModel, request);
        return Result.success("添加成功");
    }

    @ApiOperation(value = "查询阅读历史：根据用户ID")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10或12", dataType = "string", paramType = "query"),
    })
    @GetMapping
    @Authentication(roleIds = {Role.ALL})
    public Result find(@ApiIgnore @CurrentUser UserModel userModel, GetHistoryRequest request) throws BusinessException {
        List<ReadHistoryModel> data = readHistoryService.getByPage(userModel, request);
        return Result.success(data);
    }

}
