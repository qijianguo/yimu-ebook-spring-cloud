package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateFeedbackRequest {

    private String content;

    private String contact;

    public boolean validate() {
        if (content != null && content.length() < 300 && contact != null && contact.length() <= 15) {
            return true;
        }
        return false;
    }

}
