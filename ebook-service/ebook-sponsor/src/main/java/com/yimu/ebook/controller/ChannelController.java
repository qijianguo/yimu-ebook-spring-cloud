package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.ChannelModel;
import com.yimu.ebook.domain.vo.ChannelResponse;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.ChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "/channel", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "频道管理")
public class ChannelController {

    private final ChannelService channelService;

    @Autowired
    public ChannelController(ChannelService channelService) {
        this.channelService = channelService;
    }

    @ApiOperation(value = "查询所有的频道")
    @GetMapping
    public Result getChannel() throws BusinessException {
        List<ChannelModel> models = Optional.ofNullable(channelService.getAll()).orElse(Collections.emptyList());
        return Result.success(convertFromModelList(models));
    }

    private List<ChannelResponse> convertFromModelList(List<ChannelModel> models) {
        List<ChannelResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            ChannelResponse response = new ChannelResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }
}
