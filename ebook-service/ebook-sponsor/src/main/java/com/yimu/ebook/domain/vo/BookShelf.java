package com.yimu.ebook.domain.vo;

import lombok.Data;

import java.util.Date;

@Data
public class BookShelf {

    private Long bookId;

    private Date createTime;

}
