package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.AdminDOMapper;
import com.yimu.ebook.dao.RoleDOMapper;
import com.yimu.ebook.domain.dataobject.AdminDO;
import com.yimu.ebook.domain.dataobject.RoleDO;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AdminLoginRequest;
import com.yimu.ebook.domain.vo.AdminRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.AdminService;
import com.yimu.ebook.service.TokenService;
import com.yimu.ebook.util.MD5Utils;
import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * @author Angus
 */
@Service
public class AdminServiceImpl implements AdminService {

  private final AdminDOMapper adminDOMapper;
  private final TokenService tokenService;
  private final RoleDOMapper roleDOMapper;

  private static final int PASSWORD_MIN_LENGTH = 8;

  public AdminServiceImpl(
      AdminDOMapper adminDOMapper, TokenService tokenService, RoleDOMapper roleDOMapper) {
    this.adminDOMapper = adminDOMapper;
    this.tokenService = tokenService;
    this.roleDOMapper = roleDOMapper;
  }

  @Override
  @Transactional(rollbackOn = BusinessException.class)
  public void saveOrUpdate(AdminRequest request) {
    if (!request.validate()) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
    }
    if (request.getPassword().length() < PASSWORD_MIN_LENGTH) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, String.format("密码长度不足%d位", PASSWORD_MIN_LENGTH));
    }
    // 检查权限是否存在
    RoleDO roleDO = roleDOMapper.selectByPrimaryKey(request.getRoleId());
    if(roleDO == null) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "权限不存在");
    }
    AdminDO adminDO = convertFromRequest(request);
    if (request.getId() != null) {
      adminDOMapper.updateByPrimaryKeySelective(adminDO);
    } else {
      adminDOMapper.insertSelective(adminDO);
    }
  }

  @Override
  public UserModel login(AdminLoginRequest request) {
    if (!request.validate()) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
    }
    AdminDO adminDO = adminDOMapper.selectByUsernameAndPassword(request.getUsername(), MD5Utils.MD5(request.getPassword()));
    if (adminDO == null) {
      throw new BusinessException(EmBusinessError.USER_LOGIN_FAIL);
    }
    // 放入到redis中
    UserModel userModel = convertFromAdminDo(adminDO);
    tokenService.createToken(userModel);
    return userModel;
  }

  private UserModel convertFromAdminDo(AdminDO adminDO) {
    UserModel userModel = new UserModel();
    userModel.setRoleId(adminDO.getRoleId());
    userModel.setName(adminDO.getUsername());
    userModel.setPassword(adminDO.getPassword());
    userModel.setCreateDate(adminDO.getCreateDate());
    userModel.setTelphone(adminDO.getTelphone());
    return userModel;
  }

  private AdminDO convertFromRequest(AdminRequest request) {
    AdminDO adminDO = new AdminDO();
    BeanUtils.copyProperties(request, adminDO);
    if (request.getActive() != null) {
      adminDO.setActive(request.getActive());
    }
    adminDO.setCreateDate(new Date());
    adminDO.setPassword(MD5Utils.MD5(request.getPassword()));
    return adminDO;
  }
}
