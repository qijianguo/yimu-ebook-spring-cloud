package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.AdDOMapper;
import com.yimu.ebook.dao.AdHannelsDOMapper;
import com.yimu.ebook.domain.dataobject.AdDO;
import com.yimu.ebook.domain.dataobject.AdHannelsDO;
import com.yimu.ebook.domain.model.AdHannelsModel;
import com.yimu.ebook.domain.model.AdModel;
import com.yimu.ebook.service.AdService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 * @author Angus
 */
@Service
public class AdServiceImpl implements AdService {

  private final AdDOMapper adDOMapper;
  private final AdHannelsDOMapper adHannelsDOMapper;

  public AdServiceImpl(AdDOMapper adDOMapper,
      AdHannelsDOMapper adHannelsDOMapper) {
    this.adDOMapper = adDOMapper;
    this.adHannelsDOMapper = adHannelsDOMapper;
  }

  @Override
  public List<AdModel> getRandomAdGroupByType() {
    List<AdDO> adDO = adDOMapper.selectRandomGroupByType();
    if (adDO == null) {
      // throw new BusinessException(EmBusinessError.NOT_FOUND_RESOURCE, "未查询到数据");
      return null;
    }
    return covertFromAdDataObject(adDO);
  }

  @Override
  public AdHannelsModel getAdHannels(String sign) {
    if (StringUtils.isEmpty(sign)) {
      return null;
    }
    AdHannelsDO adHannelsDO = adHannelsDOMapper.selectBySign(sign);
    if (adHannelsDO == null) {
      return null;
    }
    return covertFromAdHannelsDataObject(adHannelsDO);
  }

  @Override
  public List<AdModel> selectByTfidIn(List<String> tfids) {
    if (CollectionUtils.isEmpty(tfids)) {
      return null;
    }
    tfids = tfids.stream().filter(tfid -> !StringUtils.isEmpty(tfid)).collect(Collectors.toList());
    return covertFromAdDataObject(adDOMapper.selectByTfidIn(tfids));
  }

  private List<AdModel> covertFromAdDataObject(List<AdDO> adDOList) {
    if (CollectionUtils.isEmpty(adDOList)) {
      return Collections.emptyList();
    }
    List<AdModel> models = new ArrayList<>();
    adDOList.forEach(dataobject -> {
      AdModel model = new AdModel();
      BeanUtils.copyProperties(dataobject, model);
      models.add(model);
    });
    return models;
  }

  private List<AdHannelsModel> convertFromAdHannelsDataObjectList(List<AdHannelsDO> adHannelsDOS) {
    if (CollectionUtils.isEmpty(adHannelsDOS)) {
      return Collections.emptyList();
    }
    List<AdHannelsModel> models = new ArrayList<>();
    adHannelsDOS.forEach(adHannelsDO -> {
      AdHannelsModel model = covertFromAdHannelsDataObject(adHannelsDO);
      models.add(model);
    });
    return models;
  }

  private AdHannelsModel covertFromAdHannelsDataObject(AdHannelsDO adHannelsDO) {
    AdHannelsModel adHannelsModel = new AdHannelsModel();
    BeanUtils.copyProperties(adHannelsDO, adHannelsModel);
    return adHannelsModel;
  }
}
