package com.yimu.ebook;

import cn.licoy.encryptbody.annotation.EnableEncryptBody;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Angus
 */
@EnableFeignClients
@EnableCircuitBreaker
@EnableSwagger2
@SpringBootApplication(scanBasePackages = {"com.yimu.ebook"})
@MapperScan("com.yimu.ebook.dao")
@EnableEncryptBody
@EnableScheduling
@EnableTransactionManagement
public class SponsorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SponsorApplication.class, args);
    }

}
