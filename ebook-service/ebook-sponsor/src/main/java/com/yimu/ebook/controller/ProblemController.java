package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.ProblemModel;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.ProblemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "problem")
@CrossOrigin
@Api(description = "常见问题")
public class ProblemController {

    private final ProblemService problemService;

    @Autowired
    public ProblemController(ProblemService problemService) {
        this.problemService = problemService;
    }

    @ApiOperation(value = "查询所有")
    @GetMapping
    public Result getAll() throws BusinessException {
        // List<ProblemModel> models = Optional.ofNullable(problemService.getAll()).orElse(Collections.emptyList());
        // 只返回URL
        return Result.success("http://111.231.55.209/#/commonProblem");
    }

}
