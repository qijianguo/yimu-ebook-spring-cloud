package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.UserAgreementDOMapper;
import com.yimu.ebook.domain.dataobject.UserAgreementDO;
import com.yimu.ebook.domain.model.UserAgreementModel;
import com.yimu.ebook.service.UserAgreementService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Angus
 */
@Service
public class UserAgreementServiceImpl implements UserAgreementService {

  @Autowired
  private UserAgreementDOMapper userAgreementDOMapper;

  @Override
  public UserAgreementModel get(String platform) {
    UserAgreementDO userAgreementDO = userAgreementDOMapper.selectByPlatform(platform);
    return convertFromDataObject(userAgreementDO);
  }

  private UserAgreementModel convertFromDataObject(UserAgreementDO dataobject) {
    if (dataobject == null) {
      return null;
    }
    UserAgreementModel model = new UserAgreementModel();
    BeanUtils.copyProperties(dataobject, model);
    return model;
  }
}
