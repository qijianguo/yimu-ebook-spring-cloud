package com.yimu.ebook.domain.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class BookResponse {

    /**
     * 对应书的uuid
     */
    private Long id;

    /**
     * 书名
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    /**
     * 封面图片
     */
    private String cover;

    /**
     * 字数：单位（万字）
     */
    private Double wordSize;

    /**
     * 阅读人数
     */
    private Long hot;

    /**
     * 评分
     */
    private BigDecimal score;

    /**
     * 简介
     */
    private String summary;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 分类
     */
    private String categoryName;

    /**
     * 分类Id
     */
    private Integer categoryId;

    /**
     * 章节总数
     */
    private int chaptersCount;

    /**
     * 是否有版权（有1，无0）
     */
    private int hasCp;

    private Date updateTime;

    /**
     * 是否完结
     */
    private String status;

    private Integer channelId;

    private Integer latestReadChapterId;

    private Integer inBookShelf = 0;
}
