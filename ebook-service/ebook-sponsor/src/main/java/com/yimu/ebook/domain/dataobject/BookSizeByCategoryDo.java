package com.yimu.ebook.domain.dataobject;

import lombok.Data;

@Data
public class BookSizeByCategoryDo {

  private int categoryId;

  private Long count;


}
