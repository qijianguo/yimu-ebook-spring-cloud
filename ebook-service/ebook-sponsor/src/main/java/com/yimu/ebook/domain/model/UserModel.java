package com.yimu.ebook.domain.model;

import com.yimu.ebook.domain.vo.RedisEntity;
import com.yimu.ebook.util.MD5Utils;
import lombok.Data;

import java.util.Date;

/**
 * @author Angus
 */
@Data
public class UserModel implements RedisEntity {

    private Long id;

    private String name;

    private Integer gender;

    private Integer age;

    private String telphone;

    private String headImgUrl;

    private String registerMode;

    private String thirdPartyId;

    private String password;

    private String token;

    private Date createDate;

    private Integer channelId;

    private Integer roleId = 0;

//    private String accessToken;
//
//    private String refreshAccessToken;


    @Override
    public String generateRedisKey() {
        return String.format("user_info:%s:token", token);
    }

    @Override
    public String generateRedisKey(Object token) {
        return String.format("user_info:%s:token", token);
    }

}