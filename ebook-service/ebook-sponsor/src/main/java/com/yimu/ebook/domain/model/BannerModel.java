package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BannerModel {

    private String image;

    private String link;

    private Date createTime;

    private String name;

    private String description;

    private Integer channelId;

    private String startTime;

    private String endTime;

    private Integer priority;

    private Integer active;

}
