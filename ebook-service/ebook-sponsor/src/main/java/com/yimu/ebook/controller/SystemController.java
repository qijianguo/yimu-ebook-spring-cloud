package com.yimu.ebook.controller;


import com.yimu.ebook.domain.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "system", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "时间同步")
public class SystemController {

    @ApiOperation(value = "查询版本")
    @GetMapping("/time")
    public Result getSystemTime() {
        return Result.success(new Date());
    }
}
