package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.UserAgreementModel;

/**
 * @author Angus
 */
public interface UserAgreementService {

  /**
   * 获取用户协议
   * @param platform
   * @return
   */
  UserAgreementModel get(String platform);

}
