package com.yimu.ebook.domain.model;

import lombok.Data;

/**
 * @author Angus
 */
@Data
public class Role {

    /*ADMIN(1, "高级管理员"),
    OPERATIONS(2, "运营"),
    CALL_CENTER(3, "客服"),
    TESTER(4, "测试"),
    MARKETING(5, "市场"),
    ;
    private Integer id;
    private String name;
    */

    private Integer id;
    private String name;

    public static final int ADMIN = 1;
    public static final int OPERATIONS = 2;
    public static final int CALL_CENTER = 3;
    public static final int TESTER = 4;
    public static final int MARKETING = 5;

    public static final int ALL = 0;

}

