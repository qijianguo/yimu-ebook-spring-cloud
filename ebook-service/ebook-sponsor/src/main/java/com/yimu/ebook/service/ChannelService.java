package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.ChannelModel;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface ChannelService {

    /**
     * 返回所有频道
     */
    List<ChannelModel> getAll() throws BusinessException;


}
