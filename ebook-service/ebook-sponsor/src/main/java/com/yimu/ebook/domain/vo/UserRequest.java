package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {

    private String name;

    private Integer gender;

    private Integer age;

    private Integer channelId;

    public boolean validate() {
        return (channelId == null || channelId == 1 || channelId == 2)
                && (age == null || (age >= 0 && age < 120))
                && (gender == null || (gender == 1 || gender == 2));
    }


}
