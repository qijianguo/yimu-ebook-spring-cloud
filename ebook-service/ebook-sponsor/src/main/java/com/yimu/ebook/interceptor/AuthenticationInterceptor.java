package com.yimu.ebook.interceptor;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.constant.Constants;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.TokenService;
import java.util.Arrays;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * 自定义拦截器，判断此次请求是否有权限
 * @author Angus
 */
@Component
@Slf4j
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private TokenService tokenService;

    @Override
    public boolean preHandle(HttpServletRequest request,
        HttpServletResponse response, Object handler) throws BusinessException {
        // 如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();

        if (method.getAnnotation(Authentication.class) == null) {
            return true;
        }

        // 从header中得到token
        String token = request.getHeader(Constants.AUTHENTICATION);

        // 如果未注明Authenticatoin
        /*if (method.getAnnotation(Authentication.class) == null
            && method.getAnnotation(Authorization.class) == null) {
            return true;
        }*/
        // 给getBookDetail放行：如果不为null，则需要给前端添加书架/历史记录等信息
        UserModel authenticatedUser = null;
        Authentication authentication = method.getAnnotation(Authentication.class);

        if (authentication != null) {
            if (method.getName().contains("getBookDetail")) {
                try {
                    authenticatedUser = tokenService.checkToken(token);
                } catch (Exception e) {
                    return true;
                }
            } else {
                authenticatedUser = tokenService.checkToken(token);
            }
        }
        if (authenticatedUser == null) {
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        if (authenticatedUser.getRoleId() == null) {
            authenticatedUser.setRoleId(Role.ALL);
        }

        // 如果未注明Authorization
        if (!Arrays.stream(authentication.roleIds()).anyMatch(authenticatedUser.getRoleId()::equals)) {
            throw new BusinessException(EmBusinessError.USER_NOT_AUTHORIZATION);
        }

        //如果token验证成功，将token对应的用户存在request中，便于之后注入
        request.setAttribute(Constants.CURRENT_USER_ID, authenticatedUser);
        return true;
    }
}
