package com.yimu.ebook.domain.dataobject;

public class ChannelDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_channel.id
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_channel.name
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    private String name;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_channel.image
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    private String image;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_channel.id
     *
     * @return the value of t_channel.id
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_channel.id
     *
     * @param id the value for t_channel.id
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_channel.name
     *
     * @return the value of t_channel.name
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_channel.name
     *
     * @param name the value for t_channel.name
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_channel.image
     *
     * @return the value of t_channel.image
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    public String getImage() {
        return image;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_channel.image
     *
     * @param image the value for t_channel.image
     *
     * @mbg.generated Fri May 31 15:03:30 CST 2019
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }
}