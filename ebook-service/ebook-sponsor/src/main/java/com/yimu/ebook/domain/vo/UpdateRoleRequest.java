package com.yimu.ebook.domain.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateRoleRequest {

  private Integer id;

  private String name;

  private String description;

  private List<Integer> permissionIds;

  public boolean validateUpdate() {
    return id != null && (name != null || permissionIds != null || !CollectionUtils.isEmpty(permissionIds));
  }

}
