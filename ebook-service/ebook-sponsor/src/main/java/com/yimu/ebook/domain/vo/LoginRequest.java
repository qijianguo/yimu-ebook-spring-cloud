package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginRequest {

    private String telphone;

    private int code;

    // private String password;

    private Integer channelId;

    public boolean validate() {
        return null != telphone && telphone.length() == 11 && code > 0 && channelId != null;
    }

}
