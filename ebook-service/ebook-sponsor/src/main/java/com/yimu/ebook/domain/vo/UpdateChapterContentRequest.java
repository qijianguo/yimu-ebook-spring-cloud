package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateChapterContentRequest {

  /**
   * 章节内容URL
   */
  private String contentUrl;

  /**
   * 章节内容
   */
  private String content;

  public boolean validate() {
    return content != null && contentUrl != null;
  }

}
