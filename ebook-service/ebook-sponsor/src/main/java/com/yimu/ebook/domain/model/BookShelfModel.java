package com.yimu.ebook.domain.model;

import lombok.Data;

import java.util.Date;

/**
 * 书架模型类
 * @author Angus
 */
@Data
public class BookShelfModel {

    private Integer id;

    private Long bookId;

    private Long userId;

    private String bookName;

    private String bookCover;

    private Date createTime;
}
