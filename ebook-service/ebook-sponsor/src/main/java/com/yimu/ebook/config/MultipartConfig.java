package com.yimu.ebook.config;

import javax.servlet.MultipartConfigElement;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Angus
 */
@Configuration
public class MultipartConfig {

  /**
   * 文件上传配置
   * @return
   */
  @Bean
  public MultipartConfigElement multipartConfigElement() {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    //文件最大: KB,MB
    factory.setMaxFileSize("102400KB");
    /// 设置总上传数据总大小
    factory.setMaxRequestSize("1024000KB");
    return factory.createMultipartConfig();
  }
}
