package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.LoginRequest;
import com.yimu.ebook.domain.vo.UserRequest;
import com.yimu.ebook.exception.BusinessException;

/**
 * 用户业务接口
 * @author Angus
 */
public interface UserService {

    /**
     * 获取手机验证码
     * @param telphone 手机号
     * @return code
     * @throws BusinessException
     */
    int getTelphoneCode(String telphone) throws BusinessException;

    /**
     * 根据手机登录
     * @param request 登陆请求参数
     * @return UserModel
     * @throws BusinessException
     */
    UserModel loginByTelphone(LoginRequest request) throws BusinessException;

    /**
     * 根据微信登录
     * @param code 微信code
     * @return UserModel
     * @throws BusinessException
     */
    UserModel loginByWeiXin(String code) throws BusinessException;

    /**
     * 修改用户信息
     * @param userModel 用户信息
     * @param request 请求参数
     */
    UserModel updateUserInfo(UserModel userModel, UserRequest request) throws BusinessException;

    /**
     * 退出登陆
     * @param userModel
     */
    void logout(UserModel userModel);
}
