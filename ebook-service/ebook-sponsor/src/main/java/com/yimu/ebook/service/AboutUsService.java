package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.AboutUsModel;
import java.util.List;

/**
 * @author Angus
 */
public interface AboutUsService {

  List<AboutUsModel> getAll();
}
