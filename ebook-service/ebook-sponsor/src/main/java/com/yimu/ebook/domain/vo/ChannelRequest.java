package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelRequest {

    private Long userId;

    private Integer channelId;

    public boolean validate() {
        return userId != null && channelId != null && channelId != 0;
    }

}
