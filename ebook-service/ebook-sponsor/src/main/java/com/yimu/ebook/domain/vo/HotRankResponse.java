package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HotRankResponse {

    /**
     * 热度
     */
    private Long hot;

    /**
     * 书名
     */
    private String bookName;

    /**
     * 图书ID
     */
    private Long bookId;

    private String cover;

    private String summary;

    private String author;

    private String categoryName;

    private String status;

}
