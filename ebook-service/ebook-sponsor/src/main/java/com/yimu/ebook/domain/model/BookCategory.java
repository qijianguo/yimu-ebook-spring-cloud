package com.yimu.ebook.domain.model;

public enum  BookCategory {

    FISHY_STORIES(1, "玄幻小说", Channel.MALE.getId()),;

    private int id;

    private String name;

    private Integer channelId;

    BookCategory(int id, String name, Integer channelId) {
        this.id = id;
        this.name = name;
        this.channelId = channelId;
    }
}
