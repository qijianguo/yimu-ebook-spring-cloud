package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.*;
import com.yimu.ebook.domain.dataobject.*;
import com.yimu.ebook.domain.model.BookModel;
import com.yimu.ebook.domain.model.BookRank;
import com.yimu.ebook.domain.model.HotRankModel;
import com.yimu.ebook.domain.vo.CreateHotRankRequest;
import com.yimu.ebook.domain.vo.HotRankRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.interceptor.AppVersionCheckInterceptor;
import com.yimu.ebook.service.BookRankService;
import com.yimu.ebook.service.BookService;
import com.yimu.ebook.util.RedisKeyUtils;
import java.util.Date;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
@SuppressWarnings("all")
public class BookRankServiceImpl implements BookRankService {

    private static final int DEFAULT_MONTH_SIZE = 10;
    private static final int DEFAULT_WEEK_SIZE = 12;
    private static final int DEFAULT_NEWBOOK_SIZE = 12;
    private static final int DEFAULT_EDITOR_RECOMMEND = 12;
    private static final int DEFAULT_HOT_SEARCH_SIZE = 10;
    private static final int DEFAULT_HOT_SIZE = 10;
    private final RedisTemplate redisTemplate;
    private final MonthHotRankDOMapper monthHotRankDOMapper;
    private final WeekHotRankDOMapper weekHotRankDOMapper;
    private final NewbookHotRankDOMapper newbookHotRankDOMapper;
    private final EditorRecommendDOMapper editorRecommendDOMapper;
    private final HotSearchDOMapper hotSearchDOMapper;
    private final TotalRankDOMapper totalRankDOMapper;
    private final BookService bookService;

    @Autowired
    public BookRankServiceImpl(RedisTemplate redisTemplate,
        MonthHotRankDOMapper monthHotRankDOMapper, WeekHotRankDOMapper weekHotRankDOMapper,
        NewbookHotRankDOMapper newbookHotRankDOMapper,
        EditorRecommendDOMapper editorRecommendDOMapper, HotSearchDOMapper hotSearchDOMapper,
        TotalRankDOMapper totalRankDOMapper, BookService bookService) {
        this.redisTemplate = redisTemplate;
        this.monthHotRankDOMapper = monthHotRankDOMapper;
        this.weekHotRankDOMapper = weekHotRankDOMapper;
        this.newbookHotRankDOMapper = newbookHotRankDOMapper;
        this.editorRecommendDOMapper = editorRecommendDOMapper;
        this.hotSearchDOMapper = hotSearchDOMapper;
        this.totalRankDOMapper = totalRankDOMapper;
        this.bookService = bookService;
    }

    @Override
    public List<BookRank> getAllRankType() {
        List<BookRank> list = new ArrayList<>();
        list.add(new BookRank(1, "monthRank", "人气推荐"));
        list.add(new BookRank(2, "weekRank", "本周最热"));
        list.add(new BookRank(3, "newbookRank", "新书推荐"));
        list.add(new BookRank(4, "editorRecommend", "小编推荐"));
        list.add(new BookRank(5, "hotSearch", "搜索热门"));
        list.add(new BookRank(6, "totalRank", "大家都在看"));
        return list;
    }

    @Override
    public void followBook(Integer bookId) throws BusinessException {
        // 判断是否已经点过赞
        long userId = 0L;
        if (follow(bookId, userId)) {
            throw new BusinessException(EmBusinessError.NOT_FOUND_RESOURCE);
        }
        // 人气推荐
        redisTemplate.opsForZSet().incrementScore(RedisKeyUtils.monthHotRecommend(bookId), bookId, 1);
        // 本周最热
        redisTemplate.opsForZSet().incrementScore(RedisKeyUtils.weekHotRecommend(bookId), bookId, 1);
        // 新书推荐：
        redisTemplate.opsForZSet().incrementScore(RedisKeyUtils.newBookHotRecommend(bookId), bookId, 1);

    }

    @Override
    public List<HotRankModel> getMonthHotRank(HotRankRequest request) {
        request.defPage(DEFAULT_MONTH_SIZE);
        List list = monthHotRankDOMapper.selectByChannelOrderByHotLimit(getIpControl(), request.getChannelId(), request.getPageNum(), request.getSize());
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.emptyList());
    }

    @Override
    public List<HotRankModel> getWeekHotRank(HotRankRequest request) {
        request.defPage(DEFAULT_WEEK_SIZE);
        List list = weekHotRankDOMapper.selectByChannelOrderByHotLimit(getIpControl(), request.getChannelId(), request.getPageNum(), request.getSize());
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.emptyList());
    }

    @Override
    public List<HotRankModel>  getNewbookHotRank(HotRankRequest request) {
        request.defPage(DEFAULT_NEWBOOK_SIZE);
        List list = newbookHotRankDOMapper.selectByChannelOrderByHotLimit(getIpControl(), request.getChannelId(), request.getPageNum(), request.getSize());
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.emptyList());
    }

    @Override
    public List<HotRankModel> getEditorRecommend(HotRankRequest request) {
        request.defPage(DEFAULT_EDITOR_RECOMMEND);
        List list = editorRecommendDOMapper.selectByChannelOrderByHotLimit(getIpControl(), request.getChannelId(), request.getPageNum(), request.getSize());
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.emptyList());
    }

    @Override
    public List<HotRankModel> getHotSearch(HotRankRequest request) {
        request.defPage(DEFAULT_HOT_SEARCH_SIZE);
        List list = hotSearchDOMapper.selectByChannelOrderByHotLimit(getIpControl(), request.getChannelId(), request.getPageNum(), request.getSize());
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.emptyList());
    }

    @Override
    public List<HotRankModel> getTotalRank(HotRankRequest request) {
        request.defPage(DEFAULT_HOT_SIZE);
        List list = totalRankDOMapper.selectByChannelOrderByHotLimit(getIpControl(), request.getChannelId(), request.getPageNum(), request.getSize());
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.emptyList());
    }

    private Integer getIpControl() {
        return AppVersionCheckInterceptor.ipControl.get();
    }

    @Override
    public void saveMonthHotRank(List<HotRankModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return;
        }
        monthHotRankDOMapper.insertBatch(models);
    }

    @Override
    public void saveWeekHotRank(List<HotRankModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return;
        }
        weekHotRankDOMapper.insertBatch(models);
    }

    @Override
    public void saveNewBookHotRank(List<HotRankModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return;
        }
        newbookHotRankDOMapper.insertBatch(models);
    }

    @Override
    public void saveHotSearchRank(List<HotRankModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return;
        }
        hotSearchDOMapper.insertBatch(models);
    }

    @Override
    public void saveTotalRank(List<HotRankModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return;
        }
        totalRankDOMapper.insertBatch(models);
    }

    @Override
    public void saveEditorRecommend(List<HotRankModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return;
        }
        editorRecommendDOMapper.insertBatch(models);
    }

    @Override
    public MonthHotRankDO getLatestOne() {
        return monthHotRankDOMapper.selectOneByDateDesc();
    }

    @Override
    public Integer checkBookFrom(Long bookId) {


        return null;
    }

    @Override
    public void saveHotRank(CreateHotRankRequest request) {
        BookModel bookModel = bookService.getBookDetailById(request.getBookId());
        redisTemplate.opsForValue().set(RedisKeyUtils.bookHotRank(request.getBookId()), bookModel);
    }

    @Override
    public int deleteWeekRankBeforeTime(Date date) {
        return weekHotRankDOMapper.deleteBefore(date);
    }

    @Override
    public int deleteMonthRankBeforeTime(Date date) {
        return monthHotRankDOMapper.deleteBefore(date);
    }

    @Override
    public int deleteEditorRecommendBeforeTime(Date date) {
        return editorRecommendDOMapper.deleteBefore(date);
    }

    @Override
    public int deleteHotSearchBeforeTime(Date date) {
        return hotSearchDOMapper.deleteBefore(date);
    }

    @Override
    public int deleteNewbookBeforeTime(Date date) {
        return newbookHotRankDOMapper.deleteBefore(date);
    }

    @Override
    public int deleteTotalBeforeTime(Date date) {
        return totalRankDOMapper.deleteBefore(date);
    }

    private List<HotRankModel> convertFromDataObjectList(List<Object> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        List<HotRankModel> result = new ArrayList<>();
        list.forEach(dataObject -> {
            HotRankModel model = new HotRankModel();
            BeanUtils.copyProperties(dataObject, model);
            BookDO bookDO = null;
            if (dataObject instanceof MonthHotRankDO) {
                bookDO = ((MonthHotRankDO) dataObject).getBookDO();
            } else if (dataObject instanceof WeekHotRankDO) {
                bookDO = ((WeekHotRankDO) dataObject).getBookDO();
            } else if (dataObject instanceof NewbookHotRankDO) {
                bookDO = ((NewbookHotRankDO) dataObject).getBookDO();
            } else if (dataObject instanceof EditorRecommendDO) {
                bookDO = ((EditorRecommendDO) dataObject).getBookDO();
            } else if (dataObject instanceof HotSearchDO) {
                bookDO = ((HotSearchDO) dataObject).getBookDO();
            } else if (dataObject instanceof TotalRankDO) {
                bookDO = ((TotalRankDO) dataObject).getBookDO();
            }
            setBookData(model, bookDO);
            result.add(model);
        });
        return result;
    }

    private void setBookData(HotRankModel model, BookDO bookDO) {
        if (bookDO != null) {
            model.setBookName(bookDO.getName());
            model.setCover(bookDO.getCover());
            model.setSummary(bookDO.getSummary());
            model.setAuthor(bookDO.getAuthor());
            int status = Optional.ofNullable(bookDO.getBookStatusId()).orElse(2);
            model.setStatus(status == 1 ? "连载" : "完结");
            if (bookDO.getCategoryDO() != null) {
                model.setCategoryName(bookDO.getCategoryDO().getName());
            }
        }
    }

    private boolean follow(long bookId, long userId) {
        return redisTemplate.opsForSet().isMember(RedisKeyUtils.bookFollower(bookId), userId);
    }

}
