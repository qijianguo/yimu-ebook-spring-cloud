package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeleteBookShelfRequest {

    private List<Long> bookIds;

    public boolean validate() {
        return !CollectionUtils.isEmpty(bookIds);
    }
}
