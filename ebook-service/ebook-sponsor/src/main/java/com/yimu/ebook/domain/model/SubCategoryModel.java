package com.yimu.ebook.domain.model;

import lombok.Data;

@Data
public class SubCategoryModel {

    /**
     * 分类ID
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类默认图片
     */
    private String image;

    /**
     * 频道父ID
     */
    private Integer parentId;

    private CategoryModel categoryModel;

}
