package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.AppVersionModel;
import com.yimu.ebook.domain.model.UserAgreementModel;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.domain.vo.UpdateVersionRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.AppVersionService;
import com.yimu.ebook.service.UserAgreementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "version", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "版本管理")
public class AppVersionController {

    private final AppVersionService appVersionService;

    private final UserAgreementService userAgreementService;

    public AppVersionController(AppVersionService appVersionService,
        UserAgreementService userAgreementService) {
        this.appVersionService = appVersionService;
        this.userAgreementService = userAgreementService;
    }

    @ApiOperation(value = "查询版本")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "platform", value = "platform:android/ios/h5/flutter", required = true, dataType = "varchar", paramType = "query")
    })
    @GetMapping
    public Result getVersion(String platform) throws BusinessException {
        AppVersionModel byPlatform = appVersionService.getByPlatform(platform);
        UserAgreementModel agreementModel = userAgreementService.get(platform);
        if (agreementModel != null) {
            byPlatform.setAgreement(agreementModel.getUrl());
        }
        return Result.success(byPlatform);
    }

    @ApiOperation(value = "修改版本")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "platform", value = "platform:android/ios/h5/flutter", required = true, dataType = "varchar", paramType = "query"),
        @ApiImplicitParam(name = "versionCode", value = "版本号：例如：1", dataType = "varchar", paramType = "query"),
        @ApiImplicitParam(name = "versionName", value = "版本名称：v1.0.1", dataType = "varchar", paramType = "query"),
        @ApiImplicitParam(name = "title", value = "显示标题：例如：是否更新到v1.0.1", dataType = "varchar", paramType = "query"),
        @ApiImplicitParam(name = "description", value = "描述", paramType = "query"),
        @ApiImplicitParam(name = "link", value = "下载地址", paramType = "query"),
        @ApiImplicitParam(name = "forceUpdate", value = "是否强制更新：0不强制/1强制", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "inReview", value = "是否在审核中：0正常/1审核中", dataType = "int", paramType = "query"),
    })
    @PutMapping
    public Result updateVersion(UpdateVersionRequest request) throws BusinessException {
        appVersionService.updateVersion(request);
        return Result.success("更新版本成功");
    }

}
