package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AdminLoginRequest;
import com.yimu.ebook.domain.vo.AdminRequest;

/**
 * @author Angus
 */
public interface AdminService {

  void saveOrUpdate(AdminRequest request);

  UserModel login(AdminLoginRequest request);

}
