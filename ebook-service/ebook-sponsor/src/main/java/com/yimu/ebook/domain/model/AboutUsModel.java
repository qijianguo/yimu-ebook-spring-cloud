package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AboutUsModel {

    private Integer id;

    private String contactType;

    private String contact;

    private String description;

}