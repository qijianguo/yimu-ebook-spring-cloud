package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.AdHannelsModel;
import com.yimu.ebook.domain.model.AdModel;
import com.yimu.ebook.domain.vo.AdHannelsResponse;
import com.yimu.ebook.domain.vo.AdResponse;
import com.yimu.ebook.domain.vo.AdWithHannelsResponse;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.service.AdService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "ad", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description= "广告业务")
public class AdController {

    private final AdService adService;

    public AdController(AdService adService) {
        this.adService = adService;
    }

    @ApiOperation(value = "按类型随机返回广告信息")
    @GetMapping
    public Result getRandomAd() {
        return Result.success(convertFromModel(adService.getRandomAdGroupByType()));
    }

    @ApiOperation(value = "根据渠道标识获取信息")
    @GetMapping("/{sign}")
    public Result getHannels(@ApiParam("渠道标识") @PathVariable String sign) {
        return Result.success(convertFromHannelsModel(adService.getAdHannels(sign)));
    }

    @ApiOperation(value = "获取广告信息和渠道信息")
    @GetMapping("/{sign}/mixed")
    public Result getAdWithHannels(@ApiParam("渠道标识") @PathVariable String sign) {
        AdHannelsModel adHannels = adService.getAdHannels(sign);
        AdHannelsResponse hannels = convertFromHannelsModel(adHannels);
        if (hannels == null) {
            return Result.success(String.format("未查询到sign=%s对应的渠道信息", sign));
        }
        List<AdModel> adModels = new ArrayList<>();
        if (!StringUtils.isEmpty(adHannels.getLpicRtext())) {
            adModels.add(new AdModel(adHannels.getLpicRtext(), "Lpic_Rtext"));
        }
        if (!StringUtils.isEmpty(adHannels.getSuspension())) {
            adModels.add(new AdModel(adHannels.getSuspension(), "suspension"));
        }
        if (!StringUtils.isEmpty(adHannels.getThreePic())) {
            adModels.add(new AdModel(adHannels.getThreePic(), "three_pic"));
        }
        if (CollectionUtils.isEmpty(adModels)) {
           adModels = adService.getRandomAdGroupByType();
        }
        List<AdResponse> ads = convertFromModel(adModels);
        return Result.success(new AdWithHannelsResponse(ads, hannels));
    }

    private List<AdResponse> convertFromModel(List<AdModel> models) {
        List<AdResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            AdResponse response = convertFromModel(model);
            responses.add(response);
        });
        return responses;
    }

    private AdResponse convertFromModel(AdModel ad) {
        AdResponse response = new AdResponse();
        BeanUtils.copyProperties(ad, response);
        return response;
    }

    private List<AdHannelsResponse> convertFromHannelsModelList(List<AdHannelsModel> models) {
        List<AdHannelsResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            AdHannelsResponse response = convertFromHannelsModel(model);
            responses.add(response);
        });
        return responses;
    }

    private AdHannelsResponse convertFromHannelsModel(AdHannelsModel model) {
        if (model == null) {
            return null;
        }
        AdHannelsResponse response = new AdHannelsResponse();
        BeanUtils.copyProperties(model, response);
        return response;
    }


}
