package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.BookStatus;
import com.yimu.ebook.domain.model.CategoryModel;
import com.yimu.ebook.domain.model.SubCategoryModel;
import com.yimu.ebook.domain.vo.*;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.CategorySubService;
import com.yimu.ebook.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "/category", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "分类管理")
public class CategoryController {

    private final CategoryService categoryService;

    private final CategorySubService categorySubService;

    @Autowired
    public CategoryController(CategoryService categoryService, CategorySubService categorySubService) {
        this.categoryService = categoryService;
        this.categorySubService = categorySubService;
    }

    @ApiOperation(value = "分类详情初始化")
    @GetMapping("/{channelId}/init")
    public Result getInitCategory(@PathVariable Integer channelId) throws BusinessException {
        InitCategoryResponse response = new InitCategoryResponse();
        // category
        List<CategoryModel> list = Optional.ofNullable(categoryService.getByChannel(channelId)).orElse(Collections.emptyList());
        response.setCategories(convertFromModel(list));
        // book status
        List<BookStatusResponse> statuses = new ArrayList<>();
        statuses.add(new BookStatusResponse(BookStatus.NO_STATUS.getId(), BookStatus.NO_STATUS.getName()));
        statuses.add(new BookStatusResponse(BookStatus.FINISHED.getId(), BookStatus.FINISHED.getName()));
        statuses.add(new BookStatusResponse(BookStatus.UNFINISHED.getId(), BookStatus.UNFINISHED.getName()));
        response.setBookStatuses(statuses);
        // sort by keyword
        response.setSortKeywords(Arrays.asList(new KeywordResponse("hot"), new KeywordResponse("favorites")));

        return Result.success(response);
    }

    @ApiOperation(value = "查询所有父分类")
    @GetMapping("/parent")
    public Result getAll() {
        List<CategoryModel> list = Optional.ofNullable(categoryService.selectAllActive()).orElse(Collections.emptyList());
        return Result.success(convertFromParentModel(list));
    }

    @ApiOperation(value = "根据用户频道返回父分类")
    @GetMapping("/{channelId}/parent")
    public Result getCategoriesByUser(@PathVariable Integer channelId) throws BusinessException {
        List<CategoryModel> list = Optional.ofNullable(categoryService.getByChannel(channelId)).orElse(Collections.emptyList());
        return Result.success(convertFromParentModel(list));
    }

    @ApiOperation(value = "根据用户频道返回父分类及子分类")
    @GetMapping("/{channelId}/parent_sub")
    public Result getCategoriesAndSubByUser(@PathVariable Integer channelId) throws BusinessException {
        List<CategoryModel> list = Optional.ofNullable(categoryService.getByChannel(channelId)).orElse(Collections.emptyList());
        return Result.success(convertFromModelWithSub(list));
    }

    @ApiOperation(value = "返回所有分类及子类")
    @GetMapping("/category_sub")
    public Result getAllAndSub() {
        List<CategoryModel> list = Optional.ofNullable(categoryService.getAllAndSub()).orElse(Collections.emptyList());
        return Result.success(convertFromModelWithSub(list));
    }

    @ApiOperation(value = "根据父分类查询子分类")
    @GetMapping("/{parentId}/sub")
    public Result getByParentId(@PathVariable Integer parentId) {
        List<SubCategoryModel> list = Optional.ofNullable(categorySubService.getByCategoryId(parentId)).orElse(Collections.emptyList());
        return Result.success(convertFromSubModel(list));
    }

    private List<CategoryAndSubResponse> convertFromModel(List<CategoryModel> list) {
        List<CategoryAndSubResponse> responses = new ArrayList<>();
        list.forEach(model -> {
            CategoryAndSubResponse response = new CategoryAndSubResponse();
            BeanUtils.copyProperties(model, response);
            response.setSub(convertFromSubModel(model.getSubCategoryModelList()));
            responses.add(response);
        });
        return responses;
    }

    private List<CategoryResponse> convertFromParentModel(List<CategoryModel> subModels) {
        List<CategoryResponse> responses = new ArrayList<>();
        subModels.forEach(subModel -> {
            CategoryResponse response = new CategoryResponse();
            BeanUtils.copyProperties(subModel, response);
            responses.add(response);
        });
        return responses;
    }

    private List<CategoryResponse> convertFromSubModel(List<SubCategoryModel> subModels) {
        List<CategoryResponse> responses = new ArrayList<>();
        subModels.forEach(subModel -> {
            CategoryResponse response = new CategoryResponse();
            BeanUtils.copyProperties(subModel, response);
            responses.add(response);
        });
        return responses;
    }

    private List<CategoryAndSubResponse> convertFromModelWithSub(List<CategoryModel> list) {
        List<CategoryAndSubResponse> responses = new ArrayList<>();
        list.forEach(model -> {
            CategoryAndSubResponse response = new CategoryAndSubResponse();
            BeanUtils.copyProperties(model, response);
            List<SubCategoryModel> subModels = Optional.ofNullable(model.getSubCategoryModelList()).orElse(Collections.emptyList());
            List<CategoryResponse> sub = convert(subModels);
            response.setSub(sub);
            responses.add(response);
        });
        return responses;
    }

    private List<CategoryResponse> convert(List<SubCategoryModel> models) {
        List<CategoryResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            CategoryResponse response = new CategoryResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }
}
