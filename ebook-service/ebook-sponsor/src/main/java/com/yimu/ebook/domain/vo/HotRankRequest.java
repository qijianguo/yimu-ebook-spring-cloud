package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HotRankRequest {

    private Integer pageNum;

    private Integer size;

    private Integer channelId;


    public void defPage(Integer pageSize) {
        if (size == null || size == 0) {
            size = pageSize;
        }
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
    }

    public HotRankRequest(Integer channelId) {
        this.channelId = channelId;
    }
}
