package com.yimu.ebook.domain.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetChapterRequest {

  /**
   * 章节内容URL
   */
  private List<String> contentUrls;

  public boolean validate() {
    return !CollectionUtils.isEmpty(contentUrls);
  }

}
