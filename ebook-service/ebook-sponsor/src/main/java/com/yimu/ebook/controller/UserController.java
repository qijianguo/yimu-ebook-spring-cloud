package com.yimu.ebook.controller;

import com.github.qcloudsms.SmsSingleSenderResult;
import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.annotation.CurrentUser;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.LoginRequest;
import com.yimu.ebook.domain.vo.LoginResponse;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.domain.vo.UserRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.UserService;
import com.yimu.ebook.util.TxSmsUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "/user", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "用户管理")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "根据手机获取验证码")
    @GetMapping("/{telphone}/code")
    public Result getTelphoneCode(@PathVariable String telphone) throws BusinessException {
        int code = userService.getTelphoneCode(telphone);
        // TODO 调用短信
        SmsSingleSenderResult smsSingleSenderResult = TxSmsUtils
            .seedMessage(telphone, String.valueOf(code));
        if (smsSingleSenderResult.result == 0) {
            return Result.success();
        } else {
            throw new BusinessException(EmBusinessError.QUERY_CODE_FILED);
        }
    }

    @ApiOperation(value = "登录：根据手机号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "telphone", value = "长度为11位的手机号码", required = true, dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "验证码", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", dataType = "int", paramType = "query")
    })
    @PostMapping("/telphone/login")
    public Result loginByTelphone(LoginRequest request) throws BusinessException {
        UserModel userModel = userService.loginByTelphone(request);
        return Result.success(convertFromModel(userModel));
    }

    @ApiOperation(value = "登录：根据微信")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "微信号", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/wx/login")
    public Result loginByWeiXin(String code) throws BusinessException {
        UserModel userModel = userService.loginByWeiXin(code);
        return Result.success(convertFromModel(userModel));
    }

    @ApiOperation(value = "修改用户名称、性别、年龄、频道")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "name", value = "用户名", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "gender", value = "性别：男1、女2", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "age", value = "年龄（0-150）", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", dataType = "int", paramType = "query")
    })
    @PostMapping
    @Authentication(roleIds = {Role.ALL})
    public Result updateUserInfo(@ApiIgnore @CurrentUser UserModel userModel, UserRequest request) throws BusinessException {
        return Result.success(convertFromModel(userService.updateUserInfo(userModel, request)));
    }

    @ApiOperation(value = "退出登陆")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
    })
    @DeleteMapping
    @Authentication(roleIds = {Role.ALL})
    public Result logout(@ApiIgnore @CurrentUser UserModel userModel) {
        userService.logout(userModel);
        return Result.success("退出成功");
    }

    private LoginResponse convertFromModel(UserModel userModel) {
        return new LoginResponse(userModel.getToken(), userModel.getName(), userModel.getHeadImgUrl(), userModel.getChannelId());
    }
}
