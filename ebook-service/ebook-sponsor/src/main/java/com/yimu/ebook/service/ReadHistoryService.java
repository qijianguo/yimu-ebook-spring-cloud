package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.ReadHistoryModel;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateReadHistoryRequest;
import com.yimu.ebook.domain.vo.GetHistoryRequest;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface ReadHistoryService {

    /**
     * 添加阅读历史
     */
    void saveOrUpdate(UserModel userModel, CreateReadHistoryRequest request) throws BusinessException;

    List<ReadHistoryModel> getByPage(UserModel userModel, GetHistoryRequest request) throws BusinessException;

    long getCount(UserModel userModel, GetHistoryRequest request) throws BusinessException;

    ReadHistoryModel selectByUserIdsAndBookId(Long userId, Long bookId);
}
