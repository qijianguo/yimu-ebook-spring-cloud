package com.yimu.ebook.task;

import com.yimu.ebook.domain.dataobject.BookSizeByCategoryDo;
import com.yimu.ebook.domain.model.CategoryModel;
import com.yimu.ebook.service.BookService;
import com.yimu.ebook.service.CategoryService;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 同步分类：显示分类中active=0的分类
 * @author Angus
 */
@Component
@Slf4j
public class SyncCategoryActive {

  private final CategoryService categoryService;
  private final BookService bookService;

  public SyncCategoryActive(CategoryService categoryService,
      BookService bookService) {
    this.categoryService = categoryService;
    this.bookService = bookService;
  }

  /**
   * 更新排名
   */
  @Scheduled(cron = "0 1 * * * *")
  public void updateCategoryActive() {
    List<CategoryModel> allCategory = categoryService.getAll();

    List<BookSizeByCategoryDo> bookSizeByCategory = Optional.ofNullable(bookService.getBookSizeByCategory()).orElse(
        Collections.emptyList());

    List<Integer> collect = bookSizeByCategory.stream().map(BookSizeByCategoryDo::getCategoryId)
        .collect(Collectors.toList());
    allCategory.forEach(category -> {
      if (!collect.contains(category.getId())) {
        category.setActive(0);
      } else {
        category.setActive(1);
      }
    });
    categoryService.updateCategory(allCategory);

  }

}
