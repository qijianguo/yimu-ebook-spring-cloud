package com.yimu.ebook.domain.vo;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryBookUpdateRequest {

    private List<Long> bookIds;

    public boolean validate() {
        return !CollectionUtils.isEmpty(bookIds);
    }

}
