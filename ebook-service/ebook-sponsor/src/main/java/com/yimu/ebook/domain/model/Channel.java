package com.yimu.ebook.domain.model;

import lombok.Getter;

/**
 * 性别
 */
@Getter
public enum Channel {
    MALE_FEMALE(0, "男频女频"),
    MALE(1, "男频"),
    FEMALE(2, "女频");
    private Integer id;

    private String name;

    Channel(int id, String name) {
        this.id = id;
        this.name = name;
    }
}