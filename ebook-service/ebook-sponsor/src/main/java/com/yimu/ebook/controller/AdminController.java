package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AdminLoginRequest;
import com.yimu.ebook.domain.vo.AdminLoginResponse;
import com.yimu.ebook.domain.vo.AdminRequest;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.service.AdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "admin", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description= "管理员管理")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @ApiOperation(value = "超级管理员：创建管理员用户")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "id", value = "用户ID（可选：传递则修改，不传递则新增）", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "password", value = "用户名", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "telphone", value = "电话号码（可选）", dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "email", value = "邮箱（可选）", dataType = "string", paramType = "query"),
    })
    @PostMapping
    @Authentication(roleIds = Role.ADMIN)
    public Result saveOrUpdate(AdminRequest request) {
        adminService.saveOrUpdate(request);
        return Result.success("操作成功");
    }

    @ApiOperation(value = "管理员登陆")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "password", value = "密码，必须大于8位", required = true, dataType = "string", paramType = "query"),
    })
    @PostMapping("/login")
    public Result login(AdminLoginRequest request) {
        UserModel userModel = adminService.login(request);
        return Result.success(convertFromUserModel(userModel));
    }

    private AdminLoginResponse convertFromUserModel(UserModel userModel) {
        AdminLoginResponse response = new AdminLoginResponse();
        response.setUsername(userModel.getName());
        response.setToken(userModel.getToken());
        response.setRoleId(userModel.getRoleId());
        return response;
    }


}
