package com.yimu.ebook.domain.model;

import lombok.Data;

import java.util.Date;

@Data
public class BookChapterModel {

    private Long id;

    private Integer chapterId;

    private String title;

    private String contentUrl;

    private Long bookId;

    private Date createTime;

}
