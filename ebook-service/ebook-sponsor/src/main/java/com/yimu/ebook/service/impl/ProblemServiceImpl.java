package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.ProblemDOMapper;
import com.yimu.ebook.domain.dataobject.ProblemAnswerDO;
import com.yimu.ebook.domain.dataobject.ProblemDO;
import com.yimu.ebook.domain.model.ProblemAnswerModel;
import com.yimu.ebook.domain.model.ProblemModel;
import com.yimu.ebook.service.ProblemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
public class ProblemServiceImpl implements ProblemService {

    private final ProblemDOMapper problemDOMapper;

    @Autowired
    public ProblemServiceImpl(ProblemDOMapper problemDOMapper) {
        this.problemDOMapper = problemDOMapper;
    }

    @Override
    public List<ProblemModel> getAll() {
        List<ProblemDO> problemDOS = Optional.ofNullable(problemDOMapper.selectAll()).orElse(Collections.emptyList());
        return convertFromDataObjectList(problemDOS);
    }

    private List<ProblemModel> convertFromDataObjectList(List<ProblemDO> dataObjectList) {
        List<ProblemModel> problemModels = new ArrayList<>();
        dataObjectList.forEach(dataObject -> {
            ProblemModel problemModel = new ProblemModel();
            BeanUtils.copyProperties(dataObject, problemModel);
            List<ProblemAnswerDO> answerDOS = dataObject.getList();
            if (!CollectionUtils.isEmpty(answerDOS)) {
                List<ProblemAnswerModel> models = convertFromAnswerDataObjectList(answerDOS);
                problemModel.setAnswerModelList(models);
            }
            problemModels.add(problemModel);
        });
        return problemModels;
    }

    private List<ProblemAnswerModel> convertFromAnswerDataObjectList(List<ProblemAnswerDO> dataObjects) {
        List<ProblemAnswerModel> models = new ArrayList<>();
        dataObjects.forEach(dataObject -> {
            ProblemAnswerModel model = new ProblemAnswerModel();
            BeanUtils.copyProperties(dataObject, model);
            models.add(model);
        });
        return models;
    }

}
