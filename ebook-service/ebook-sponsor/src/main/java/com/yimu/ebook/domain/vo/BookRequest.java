package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookRequest {

    private Integer categoryId;

    private Integer subCategoryId;

    private Integer channelId;

    private String sortKeyword;

    private String sortBy;

    private Integer pageNum;

    private Integer size;

    private Integer bookStatus;

    private Integer ipControl;

    private Integer active;

    public void init() {
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (size == null || size == 0) {
            size = 10;
        }
        if (sortKeyword == null) {
            sortKeyword = "hot";
        }
    }
}
