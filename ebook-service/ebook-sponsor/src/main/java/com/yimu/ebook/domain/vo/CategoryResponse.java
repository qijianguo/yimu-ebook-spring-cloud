package com.yimu.ebook.domain.vo;

import lombok.Data;

@Data
public class CategoryResponse {

    /**
     * 分类ID
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类默认图片
     */
    private String image;
}
