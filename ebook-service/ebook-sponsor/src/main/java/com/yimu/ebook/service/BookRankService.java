package com.yimu.ebook.service;

import com.yimu.ebook.domain.dataobject.MonthHotRankDO;
import com.yimu.ebook.domain.model.BookRank;
import com.yimu.ebook.domain.model.HotRankModel;
import com.yimu.ebook.domain.vo.CreateHotRankRequest;
import com.yimu.ebook.domain.vo.HotRankRequest;
import com.yimu.ebook.exception.BusinessException;

import java.util.Date;
import java.util.List;

/**
 * 人气排名
 * @author Angus
 */
public interface BookRankService {

    List<BookRank> getAllRankType();

    /**
     * 增加人气值
     */
    void followBook(Integer bookId) throws BusinessException;

    /**
     * 人气排行：按月排名取TOP
     */
    List getMonthHotRank(HotRankRequest request) throws BusinessException;

    /**
     * 周排行：按周排名取TOP
     */
    List<HotRankModel>  getWeekHotRank(HotRankRequest request) throws BusinessException;

    /**
     * 新书排行：半年内上架排名TOP
     */
    List<HotRankModel>  getNewbookHotRank(HotRankRequest request) throws BusinessException;

    /**
     * 新书排行：半年内上架排名TOP
     */
    List<HotRankModel> getEditorRecommend(HotRankRequest request) throws BusinessException;

    /**
     * 热门搜索：搜索排名
     */
    List<HotRankModel> getHotSearch(HotRankRequest request) throws BusinessException;

    /**
     * 总排名
     */
    List<HotRankModel> getTotalRank(HotRankRequest request) throws BusinessException;

    void saveMonthHotRank(List<HotRankModel> models);

    void saveWeekHotRank(List<HotRankModel> models);

    void saveNewBookHotRank(List<HotRankModel> models);

    void saveHotSearchRank(List<HotRankModel> models);

    void saveTotalRank(List<HotRankModel> models);

    void saveEditorRecommend(List<HotRankModel> models);

    /**
     * 获取最近更新过的一条：是否今天已经更新过了
     * @return
     */
    MonthHotRankDO getLatestOne();

    Integer checkBookFrom(Long bookId);

    void saveHotRank(CreateHotRankRequest request);

    int deleteWeekRankBeforeTime(Date date);
    int deleteMonthRankBeforeTime(Date date);
    int deleteEditorRecommendBeforeTime(Date date);
    int deleteHotSearchBeforeTime(Date date);
    int deleteNewbookBeforeTime(Date date);
    int deleteTotalBeforeTime(Date date);
}
