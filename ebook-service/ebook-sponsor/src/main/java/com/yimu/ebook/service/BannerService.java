package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.BannerModel;
import com.yimu.ebook.domain.model.BannerType;
import com.yimu.ebook.domain.vo.BannerRequest;
import com.yimu.ebook.domain.vo.CreateBannerRequest;
import com.yimu.ebook.exception.BusinessException;
import java.util.List;

/**
 * Banner业务接口
 * @author Angus
 */
public interface BannerService {

    List<BannerType> getAllBannerTypes();

    List<BannerModel> getBannerByCondition(BannerRequest request) throws BusinessException;

    void save(CreateBannerRequest request);

}
