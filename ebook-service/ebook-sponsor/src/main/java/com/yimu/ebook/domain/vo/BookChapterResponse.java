package com.yimu.ebook.domain.vo;

import lombok.Data;

@Data
public class BookChapterResponse {

    private Integer id;

    private String title;

    private String contentUrl;

}
