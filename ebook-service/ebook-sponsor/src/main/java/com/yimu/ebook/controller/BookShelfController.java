package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.annotation.CurrentUser;
import com.yimu.ebook.domain.model.BookShelfModel;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.*;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BookShelfService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Angus
 */
@RestController
@CrossOrigin
@Api(description = "书架管理")
@RequestMapping(value = "/bookShelf", produces = MediaType.TEXT_HTML_VALUE)
public class BookShelfController {

    private final BookShelfService bookShelfService;

    @Autowired
    public BookShelfController(BookShelfService bookShelfService) {
        this.bookShelfService = bookShelfService;
    }

    @ApiOperation(value = "批量增加")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "bookId", value = "书籍ID", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "createTime", value = "创建时间,时间戳", dataType = "long", paramType = "query"),
    })
    @PostMapping
    @Authentication(roleIds = {Role.ALL})
    public Result addBookShelf(@ApiIgnore @CurrentUser UserModel userModel, @RequestBody AddBookShelfRequest request) throws BusinessException {
        bookShelfService.addBookShelf(userModel, request);
        return Result.success("添加成功");
    }

    @ApiOperation(value = "查询书架")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10或12", dataType = "string", paramType = "query"),
    })
    @GetMapping
    @Authentication(roleIds = {Role.ALL})
    public Result getBookShelf(@ApiIgnore @CurrentUser UserModel userModel, GetBookShelfRequest request) throws BusinessException {
        List<BookShelfModel> list = bookShelfService.getByUserId(userModel, request);
        List<BookShelfResponse> data = convertFromModelList(list);
        int count = bookShelfService.selectCountByUserIdAndBookId(userModel.getId(), null);
        return Result.success(new Page(request.getPageNum(), request.getSize(), count, data));
    }

    @ApiOperation(value = "查询书架书籍个数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
    })
    @GetMapping("/count")
    @Authentication(roleIds = {Role.ALL})
    public Result getBookShelfSize(@ApiIgnore @CurrentUser UserModel userModel) throws BusinessException {
        int count = bookShelfService.selectCountByUserIdAndBookId(userModel.getId(), null);
        return Result.success(count);
    }


    @ApiOperation(value = "批量删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "bookIds", value = "书籍ID", dataType = "long", paramType = "query"),
    })
    @DeleteMapping
    @Authentication(roleIds = {Role.ALL})
    public Result deleteBookShelf(@ApiIgnore @CurrentUser UserModel userModel, @RequestBody DeleteBookShelfRequest request) throws BusinessException {
        bookShelfService.deleteBookFromShelf(userModel, request);
        return Result.success("删除成功");
    }

    private List<BookShelfResponse> convertFromModelList(List<BookShelfModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return Collections.emptyList();
        }
        List<BookShelfResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            BookShelfResponse response = new BookShelfResponse();
            BeanUtils.copyProperties(model, response);
            response.setTime(model.getCreateTime());
            responses.add(response);
        });
        return responses;
    }
}
