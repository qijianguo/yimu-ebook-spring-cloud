package com.yimu.ebook.domain.vo;

import lombok.Data;

@Data
public class BookChapterRequest {

    private Integer pageNum;

    private Integer size;

    private Long bookId;

    private void defPage() {
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (size == null || size == 0) {
            size = 10;
        }
    }

    public boolean validate() {
        if (bookId == null) {
            return false;
        }
        defPage();
        return true;
    }

}
