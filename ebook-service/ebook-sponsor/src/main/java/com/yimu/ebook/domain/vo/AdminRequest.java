package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminRequest {

  private Integer id;

  private String username;

  private String password;

  private String telphone;

  private String email;

  private Integer roleId;

  private Integer active;

  public boolean validate() {
    return username != null && password != null && roleId != null && (active == null || active == 0 || active == 1);
  }

}
