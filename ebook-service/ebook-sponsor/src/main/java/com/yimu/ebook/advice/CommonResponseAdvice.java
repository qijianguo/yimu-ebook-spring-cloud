package com.yimu.ebook.advice;

import com.alibaba.fastjson.JSON;
import com.yimu.ebook.annotation.IgnoreResponseAdvice;
import com.yimu.ebook.interceptor.AppVersionCheckInterceptor;
import com.yimu.ebook.util.AesUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import java.util.Objects;

@RestControllerAdvice
@Slf4j
public class CommonResponseAdvice implements ResponseBodyAdvice {

    private String aesKey = "1234567890000000";

    /**
     * 判断是否拦截
     */
    @Override
    @SuppressWarnings("all")
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        // 如果方法或类中标注了 IgnoreResponseAdvice 则不进行拦截
        if (Objects.requireNonNull(methodParameter.getMethod()).isAnnotationPresent(IgnoreResponseAdvice.class)) {
            return false;
        }
        if (methodParameter.getDeclaringClass().isAnnotationPresent(IgnoreResponseAdvice.class)) {
            return false;
        }

        return true;
    }

    @Nullable
    @Override
    @SuppressWarnings("all")
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
        String message = JSON.toJSONString(o);
        // log.info("加密前：{}", message);
        String encrypt = "";
        try {
            //  String password = "1234567890000000";
            encrypt = AesUtils.encrypt(message, aesKey);
            // log.info("加密后：{}", encrypt);
            // String de = AesUtils.decrypt(encrypt, "1234567890000000");
            // log.info("解密后：{}", de);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), encrypt);
        }
        if(AppVersionCheckInterceptor.ipControl != null) {
            AppVersionCheckInterceptor.ipControl.remove();
        }
        return o;
    }
/*
    public static void main(String[] args) throws Exception {
        String password = "1234567890000000";
        System.out.println("秘钥为：" + password);
        String source = JSON.toJSONString(Result.success());
        System.out.println("加密前：" + source);

        String enSour = AesUtils.encrypt(source, password);
        System.out.println("加密后：" + enSour);

        String deSour = AesUtils.decrypt(enSour, password);
        System.out.println("解密后：" + deSour);
    }*/
}
