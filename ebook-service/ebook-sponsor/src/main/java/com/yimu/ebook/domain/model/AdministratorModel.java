package com.yimu.ebook.domain.model;

import com.yimu.ebook.domain.vo.RedisEntity;
import java.util.Date;
import lombok.Data;

/**
 * @author Angus
 */
@Data
public class AdministratorModel implements RedisEntity {

    private Long id;

    private String username;

    private String password;

    private String token;

    private Date createDate;

    @Override
    public String generateRedisKey() {
        return String.format("administrator_info:%s:token", token);
    }

    @Override
    public String generateRedisKey(Object token) {
        return String.format("administrator_info:%s:token", token);
    }

}