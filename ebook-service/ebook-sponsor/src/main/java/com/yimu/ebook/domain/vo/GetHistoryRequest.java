package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetHistoryRequest {

    private Integer pageNum;

    private Integer size;

    public void defPage(Integer defaultSize) {
        if (size == null || size == 0) {
            size = defaultSize;
        }
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
    }

}
