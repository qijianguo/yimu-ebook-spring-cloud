package com.yimu.ebook.domain.model;

import lombok.Getter;

/**
 * 图书状态
 * @author Angus
 */
@Getter
public enum BookStatus {

    NO_STATUS(0, "不限状态"),
    FINISHED(1, "完结"),
    UNFINISHED(2, "连载");
    private Integer id;

    private String name;

    BookStatus(int id, String name) {
        this.id = id;
        this.name = name;
    }
}