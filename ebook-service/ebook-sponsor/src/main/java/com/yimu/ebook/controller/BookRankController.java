package com.yimu.ebook.controller;

import com.google.common.collect.ImmutableList;
import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.domain.model.BannerModel;
import com.yimu.ebook.domain.model.HotRankModel;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.vo.BannerRequest;
import com.yimu.ebook.domain.vo.BannerSimpleResponse;
import com.yimu.ebook.domain.vo.BookCityButton;
import com.yimu.ebook.domain.vo.BookCityRequest;
import com.yimu.ebook.domain.vo.BookCityResponse;
import com.yimu.ebook.domain.vo.CreateHotRankRequest;
import com.yimu.ebook.domain.vo.HotRankRequest;
import com.yimu.ebook.domain.vo.HotRankResponse;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BannerService;
import com.yimu.ebook.service.BookRankService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "/rank", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "热度排名")
public class BookRankController {

    private final BookRankService bookRankService;

    private final BannerService bannerService;

    @Autowired
    public BookRankController(BookRankService bookRankService, BannerService bannerService) {
        this.bookRankService = bookRankService;
        this.bannerService = bannerService;
    }

    @ApiOperation(value = "所有排行榜：包含人气推荐（monthRank）/本周最热（weekRank）/新书推荐（newbookRank）/小编推荐（editorRecommend）/热门搜索（hotSearch）/ 大家都在看（totalRank）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10或12", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", dataType = "int", paramType = "query")
    })
    @GetMapping
    public Result getBookCity(BookCityRequest request) throws BusinessException {
        List<BookCityResponse> list = new ArrayList<>();

        // Banner
        List<BannerModel> bannerMod = Optional.ofNullable(bannerService.getBannerByCondition(new BannerRequest(request.getChannelId(), 5))).orElse(Collections.emptyList());
        if (!CollectionUtils.isEmpty(bannerMod)) {
            list.add(new BookCityResponse("banner", "Banner", "banner", "roll", "",
                convertFromBannerModel(bannerMod)));
        }

        // Button
        list.add(new BookCityResponse("category", "按钮", "category", "roll", "", convertFromButtonModel()));

        // Rank
        HotRankRequest request1 = new HotRankRequest(request.getChannelId());
        list.add(new BookCityResponse("monthRank", "人气推荐","v5h2", "", "", convertFromModel(bookRankService.getMonthHotRank(request1))));
        list.add(new BookCityResponse("weekRank", "本周最热", "h", "", "/sponsor/rank/week", convertFromModel(bookRankService.getWeekHotRank(request1))));
        list.add(new BookCityResponse("newbookRank", "新书推荐", "h", "", "/sponsor/rank/newbook", convertFromModel(bookRankService.getNewbookHotRank(request1))));
        list.add(new BookCityResponse("editorRecommend", "小编推荐", "v", "", "/sponsor/rank/editorRecommend", convertFromModel(bookRankService.getEditorRecommend(request1))));
        list.add(new BookCityResponse("hotSearch", "搜索热门","v2h4", "", "/sponsor/rank/hotSearch", convertFromModel(bookRankService.getHotSearch(request1))));
        list.add(new BookCityResponse("totalRank", "大家都在看", "v", "", "", convertFromModel(bookRankService.getTotalRank(request1))));
        return Result.success(list);
    }

    @ApiOperation(value = "人气推荐/月度排名（monthRank）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是12", dataType = "string", paramType = "query", defaultValue = "12"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/month")
    public Result getMonthRank(HotRankRequest request) throws BusinessException {
        return Result.success(convertFromModel(bookRankService.getMonthHotRank(request)));
    }

    @ApiOperation(value = "本周最热/本周排名（weekRank）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/week")
    public Result getWeekRank(HotRankRequest request) throws BusinessException {
        return Result.success(convertFromModel(bookRankService.getWeekHotRank(request)));
    }

    @ApiOperation(value = "新书推荐/半年内上架书籍排行（newbookRank）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是12", dataType = "string", paramType = "query", defaultValue = "12"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/newbook")
    public Result getNewBookRank(HotRankRequest request) throws BusinessException {
        return Result
            .success(convertFromModel(bookRankService.getNewbookHotRank(request)));
    }

    @ApiOperation(value = "小编推荐（editorRecommend）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是12", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/editorRecommend")
    public Result getRecommendRank(HotRankRequest request) throws BusinessException {
        return Result
            .success(convertFromModel(bookRankService.getEditorRecommend(request)));
    }

    @ApiOperation(value = "热门搜索/搜索排行（hotSearch）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/hotSearch")
    public Result getSearchRank(HotRankRequest request) throws BusinessException {
        return Result.success(convertFromModel(bookRankService.getHotSearch(request)));
    }

    @ApiOperation(value = "总排行/大家都在看（totalRank）")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "10"),
        @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "string", paramType = "query")
    })
    @GetMapping("/total")
    public Result getHotRank(HotRankRequest request) throws BusinessException {
        return Result.success(convertFromModel(bookRankService.getTotalRank(request)));
    }

    @ApiOperation(value = "添加排行")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "bookId", value = "书籍ID", required = true, dataType = "long", paramType = "query"),
        @ApiImplicitParam(name = "type", value = "人气推荐（monthRank）/本周最热（weekRank）/新书推荐（newbookRank）/小编推荐（editorRecommend）/热门搜索（hotSearch）/ 大家都在看（totalRank）",
            required = true, dataType = "string", paramType = "query")
    })
    @PostMapping
    @Authentication(roleIds = Role.ADMIN)
    public Result saveHotRank(CreateHotRankRequest request) throws BusinessException {
        bookRankService.saveHotRank(request);
        return Result.success();
    }

    private List<HotRankResponse> convertFromModel(List<HotRankModel> modelList) {
        if (CollectionUtils.isEmpty(modelList)) {
            return ImmutableList.of();
        }
        List<HotRankResponse> responses = new ArrayList<>();
        modelList.forEach(model -> {
            HotRankResponse response = new HotRankResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }

    private List<BannerSimpleResponse> convertFromBannerModel(List<BannerModel> models) {
        List<BannerSimpleResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            BannerSimpleResponse response = new BannerSimpleResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }

    private List<BookCityButton> convertFromButtonModel() {
        List<BookCityButton> list = new ArrayList<>();
        list.add(new BookCityButton("排行", "/activity/rank", "http://ebook-image-1259465809.file.myqcloud.com/category_icons/home_ranking%403x.png"));
        list.add(new BookCityButton("分类", "/fragment/home_category", "http://ebook-image-1259465809.file.myqcloud.com/category_icons/home_class%403x.png"));
        list.add(new BookCityButton("完结", "/activity/entirely", "http://ebook-image-1259465809.file.myqcloud.com/category_icons/home_close%403x.png"));
        list.add(new BookCityButton("新书", "/activity/new_book", "http://ebook-image-1259465809.file.myqcloud.com/category_icons/home_newbook%403x.png"));
        return list;
    }
}
