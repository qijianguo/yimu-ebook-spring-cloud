package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateBookRequest {

    private Long bookId;

    private String name;

    private String author;

    private String cover;

    private Double wordSize;

    private Long hot;

    private Long favorites;

    private Double score;

    private String summary;

    private Date createTime;

    private Date updateTime;

    private Integer categoryId;

    private Integer subCategoryId;

    private Integer channelId;

    private Integer bookStatusId;

    /**
     * 书籍审核状态：-3审核中/-2审核成功/-1审核失败/0下架/1上架
     */
    private Integer active;

    public boolean validate() {
        if (bookId == null) {
            return false;
        }
        return true;
    }


}
