package com.yimu.ebook.domain.model;

import lombok.Getter;

/**
 * 性别
 */
@Getter
public enum  Gender {
    MALE(1, "男生"),
    FEMALE(2, "女生");
    private Integer type;

    private String name;

    Gender(int type, String name) {
        this.type = type;
        this.name = name;
    }
}