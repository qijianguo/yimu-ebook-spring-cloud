package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.ChannelDOMapper;
import com.yimu.ebook.domain.dataobject.ChannelDO;
import com.yimu.ebook.domain.model.ChannelModel;
import com.yimu.ebook.service.ChannelService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
public class ChannelServiceImpl implements ChannelService {

    private final ChannelDOMapper channelDOMapper;

    @Autowired
    public ChannelServiceImpl(ChannelDOMapper channelDOMapper) {
        this.channelDOMapper = channelDOMapper;
    }

    @Override
    public List<ChannelModel> getAll() {
        List<ChannelDO> list = Optional.ofNullable(channelDOMapper.selectAll()).orElse(Collections.emptyList());
        return convertFromDataObject(list);
    }

    private List<ChannelModel> convertFromDataObject(List<ChannelDO> dataObjects) {
        List<ChannelModel> models = new ArrayList<>();
        dataObjects.forEach(dataObject -> {
            ChannelModel model = new ChannelModel();
            BeanUtils.copyProperties(dataObject, model);
            models.add(model);
        });
        return models;
    }
}
