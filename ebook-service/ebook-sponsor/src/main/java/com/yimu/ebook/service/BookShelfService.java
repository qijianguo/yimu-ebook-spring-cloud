package com.yimu.ebook.service;

import com.yimu.ebook.domain.dataobject.BookShelfDO;
import com.yimu.ebook.domain.model.BookShelfModel;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AddBookShelfRequest;
import com.yimu.ebook.domain.vo.DeleteBookShelfRequest;
import com.yimu.ebook.domain.vo.GetBookShelfRequest;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * 书架接口
 * @author Angus
 */
public interface BookShelfService {

    /**
     * 添加书架
     */
    void addBookShelf(UserModel userModel, AddBookShelfRequest request) throws BusinessException;

    /**
     * 查询书架
     */
    List<BookShelfModel> getByUserId(UserModel userModel, GetBookShelfRequest request) throws BusinessException;

    /**
     * 查询书架书目
     */
    int selectCountByUserIdAndBookId(Long userId, Long bookId) throws BusinessException;

    int addBookShelf(List<BookShelfDO> list) throws BusinessException;

    /**
     * 批量删除
     */
    void deleteBookFromShelf(UserModel userModel, DeleteBookShelfRequest request) throws BusinessException;
}
