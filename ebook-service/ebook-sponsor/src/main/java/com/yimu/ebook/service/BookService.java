package com.yimu.ebook.service;

import com.yimu.ebook.domain.dataobject.BookChapterDO;
import com.yimu.ebook.domain.dataobject.BookSizeByCategoryDo;
import com.yimu.ebook.domain.model.BookChapterModel;
import com.yimu.ebook.domain.model.BookModel;
import com.yimu.ebook.domain.model.RecommendedBooksRequest;
import com.yimu.ebook.domain.vo.*;
import com.yimu.ebook.exception.BusinessException;
import java.util.List;

/**
 * @author Angus
 */
public interface BookService {

    /**
     * 保存书籍
     * @throws BusinessException
     */
    void update(CreateBookRequest request) throws BusinessException;

    /**
     * 按类别和频道查询
     */
    List<BookModel> getBookListByCondition(BookRequest bookRequest) throws BusinessException;

    /**
     * 查看书籍详情
     */
    BookModel getBookDetailByUuid(String uuid) throws BusinessException;

    /**
     * 查看书籍详情
     */
    BookModel getBookDetailById(Long id) throws BusinessException;

    /**
     * 查询数据的总章数
     */
    int getCountChapterByBookId(Long bookId);

    /**
     * 查询章节
     */
    List<BookChapterModel> getBookChapter(BookChapterRequest request) throws BusinessException;

    /**
     * 更新solr
     * @throws BusinessException
     */
    void setSolr() throws BusinessException;

    /**
     * 按关键字从solr中搜索
     * @return
     */
    List searchFromSolr(SearchRequest request) throws BusinessException;

    /**
     * 保存章节内容
     * @param request
     * @throws BusinessException
     */
    void update(CreateBookChapterRequest request) throws BusinessException;

    /**
     * 按书籍查询同类别/同频道/.的书籍推荐
     * @param request
     * @return
     * @throws BusinessException
     */
    List<BookModel> getRecommendedBooks(RecommendedBooksRequest request)
        throws BusinessException;

    List<BookSizeByCategoryDo> getBookSizeByCategory();

    List<BookChapterDO> getByRegexp(String regexp);

    /**
     * 更新章节信息
     * @param regexp
     */
    void updateChapterByRegexp(String regexp);

    /**
     * 查询书籍的更新状态
     * @param request
     * @return
     */
    List<QueryBookUpdateResponse> checkBookUpdate(QueryBookUpdateRequest request);

    /**
     * 更新章节内容
     * @param request
     */
    void updateChapterContent(UpdateChapterContentRequest request) throws Exception;

    String getChapterContent(String contentUrl);

}
