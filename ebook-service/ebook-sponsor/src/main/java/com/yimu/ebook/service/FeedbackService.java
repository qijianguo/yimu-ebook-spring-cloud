package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateFeedbackRequest;
import com.yimu.ebook.exception.BusinessException;

/**
 * @author Angus
 */
public interface FeedbackService {

    /**
     * 保存意见反馈
     */
    void save(UserModel userModel, CreateFeedbackRequest request) throws BusinessException;
}
