package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AllBookRankResponse {

    private String type;

    private String name;

    private String url;

    private List<HotRankResponse> topRank;
}
