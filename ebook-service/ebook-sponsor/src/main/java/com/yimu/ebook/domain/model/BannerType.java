package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BannerType {

    private Integer type;

    private String name;

}
