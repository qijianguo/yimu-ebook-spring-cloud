package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdModel {

  private Integer id;

  private String tfid;

  private String type;

  private Integer status;

  public AdModel(String tfid, String type) {
    this.tfid = tfid;
    this.type = type;
  }
}
