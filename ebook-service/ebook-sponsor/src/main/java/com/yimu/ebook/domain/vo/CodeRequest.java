package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CodeRequest {

    private String telphone;

    public boolean validate() {
        return null != telphone && telphone.length() == 11;
    }

}
