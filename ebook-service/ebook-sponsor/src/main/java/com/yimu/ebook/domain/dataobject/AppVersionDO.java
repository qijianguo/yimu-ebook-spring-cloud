package com.yimu.ebook.domain.dataobject;

import lombok.Data;

import java.util.Date;

@Data
public class AppVersionDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_app_version.id
     *
     * @mbg.generated Wed Jun 05 17:13:29 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_app_version.platform
     *
     * @mbg.generated Wed Jun 05 17:13:29 CST 2019
     */
    private String platform;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_app_version.version_code
     *
     * @mbg.generated Wed Jun 05 17:13:29 CST 2019
     */
    private String versionCode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_app_version.version_name
     *
     * @mbg.generated Wed Jun 05 17:13:29 CST 2019
     */
    private String versionName;

    private String title;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_app_version.description
     *
     * @mbg.generated Wed Jun 05 17:13:29 CST 2019
     */
    private String description;

    private String link;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_app_version.create_time
     *
     * @mbg.generated Wed Jun 05 17:13:29 CST 2019
     */
    private Date createTime;

    private Integer forceUpdate;

    private Integer inReview;
}