package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 手机验证码记录实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TelphoneCodeRecord {

    /**
     * 验证码
     */
    private int code;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 最近一次时间
     */
    private Long latestTime;

    /**
     * 请求的次数
     */
    private int times;
}
