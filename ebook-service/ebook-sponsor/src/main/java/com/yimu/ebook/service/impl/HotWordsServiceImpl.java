package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.HotWordsDOMapper;
import com.yimu.ebook.domain.dataobject.HotWordsDO;
import com.yimu.ebook.domain.model.HotWordsModel;
import com.yimu.ebook.service.HotWordsService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Angus
 */
@Service
public class HotWordsServiceImpl implements HotWordsService {

    private final HotWordsDOMapper hotWordsDOMapper;

    public HotWordsServiceImpl(
        HotWordsDOMapper hotWordsDOMapper) {
        this.hotWordsDOMapper = hotWordsDOMapper;
    }

    @Override
    public List<HotWordsModel> getAll() {
        HotWordsDO condition = new HotWordsDO();
        List<HotWordsDO> hotWordsDOS = hotWordsDOMapper.selectByCondition(condition);
        return convertFromDataObject(hotWordsDOS);
    }

    private List<HotWordsModel> convertFromDataObject(List<HotWordsDO> hotWordsDOS) {
        if (CollectionUtils.isEmpty(hotWordsDOS)) {
            return Collections.emptyList();
        }
        List<HotWordsModel> models = new ArrayList<>();
        hotWordsDOS.forEach(hotWordsDO -> {
            HotWordsModel model = new HotWordsModel();
            BeanUtils.copyProperties(hotWordsDO, model);
            models.add(model);
        });
        return models;
    }
}
