package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HotRankModel {

    private Integer id;

    /**
     * 热度
     */
    private Long hot;

    /**
     * 日期
     */
    private Date date;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 书名
     */
    private String bookName;

    /**
     * 频道ID
     */
    private Integer channelId;

    /**
     * 图书ID
     */
    private Long bookId;

    /**
     * 封面
     */
    private String cover;

    /**
     * 简介
     */
    private String summary;

    /**
     * 作者
     */
    private String author;

    private String categoryName;

    private String status;

    private Integer ipControl;
}
