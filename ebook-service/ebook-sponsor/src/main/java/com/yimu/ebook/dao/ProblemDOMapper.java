package com.yimu.ebook.dao;

import com.yimu.ebook.domain.dataobject.ProblemAnswerDO;
import com.yimu.ebook.domain.dataobject.ProblemDO;
import com.yimu.ebook.domain.dataobject.ProblemDOExample;
import java.util.List;

public interface ProblemDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    int insert(ProblemDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    int insertSelective(ProblemDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    List<ProblemDO> selectByExample(ProblemDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    ProblemDO selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    int updateByPrimaryKeySelective(ProblemDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_problem
     *
     * @mbg.generated Wed Jun 05 16:26:25 CST 2019
     */
    int updateByPrimaryKey(ProblemDO record);

    List<ProblemDO> selectAll();
}