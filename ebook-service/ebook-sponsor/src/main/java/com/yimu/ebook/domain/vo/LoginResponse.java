package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponse {

    private String token;

    private String name;

    private String headImgUrl;

    private Integer channelId;

}
