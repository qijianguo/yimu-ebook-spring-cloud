package com.yimu.ebook.dao;

import com.yimu.ebook.domain.dataobject.AdHannelsDO;
import com.yimu.ebook.domain.dataobject.AdHannelsDOExample;
import com.yimu.ebook.domain.dataobject.AdHannelsDOKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AdHannelsDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    int deleteByPrimaryKey(AdHannelsDOKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    int insert(AdHannelsDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    int insertSelective(AdHannelsDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    List<AdHannelsDO> selectByExample(AdHannelsDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    AdHannelsDO selectByPrimaryKey(AdHannelsDOKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    int updateByPrimaryKeySelective(AdHannelsDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_ad_hannels
     *
     * @mbg.generated Thu Jul 18 16:14:52 CST 2019
     */
    int updateByPrimaryKey(AdHannelsDO record);

    AdHannelsDO selectBySign(@Param("sign") String sign);
}