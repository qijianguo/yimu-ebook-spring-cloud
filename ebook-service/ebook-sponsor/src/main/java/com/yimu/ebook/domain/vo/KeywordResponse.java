package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KeywordResponse {

    private String name;
}
