package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.BookDOMapper;
import com.yimu.ebook.dao.BookShelfDOMapper;
import com.yimu.ebook.domain.dataobject.BookShelfDO;
import com.yimu.ebook.domain.model.BookAudit;
import com.yimu.ebook.domain.model.BookShelfModel;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AddBookShelfRequest;
import com.yimu.ebook.domain.vo.DeleteBookShelfRequest;
import com.yimu.ebook.domain.vo.GetBookShelfRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.BookShelfService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * @author Angus
 */
@Service
@Slf4j
public class BookShelfServiceImpl implements BookShelfService {

    private final BookShelfDOMapper bookShelfDOMapper;
    private final BookDOMapper bookDOMapper;

    @Autowired
    public BookShelfServiceImpl(BookShelfDOMapper bookShelfDOMapper, BookDOMapper bookDOMapper) {
        this.bookShelfDOMapper = bookShelfDOMapper;
        this.bookDOMapper = bookDOMapper;
    }

    @Override
    public void addBookShelf(UserModel userModel, AddBookShelfRequest request) throws BusinessException {
        if(!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<BookShelfModel> bookShelfModels = convertFromRequest(userModel, request);
        // 查询所有的书籍
//        List<BookShelfDO> bookShelfDOS = Optional.ofNullable(
//                bookShelfDOMapper.selectByUserId(request.getUserId(), null, null))
//                .orElse(Collections.emptyList()
//        );
//        List<Long> filterBookIds = new ArrayList<>();
//        // 将已存在的书籍重新设置时间
//        List<BookShelfDO> updateDOs = new ArrayList<>();
//        List<BookShelfModel> finalBookShelfModels = bookShelfModels;
//        bookShelfDOS.forEach(bookShelfDO ->
//                finalBookShelfModels.forEach(model -> {
//                    if (bookShelfDO.getBookId().equals(model.getBookId())) {
//                        filterBookIds.add(model.getBookId());
//                        updateDOs.add(bookShelfDO);
//                    }
//        }));

//        List<BookShelfModel> list = new ArrayList<>();
//        for (BookShelfModel model : bookShelfModels) {
//            if (!filterBookIds.contains(model.getBookId())) {
//                list.add(model);
//            }
//        }
//        bookShelfModels = list;

        // 将新增的和修改了的一起入库
//        List<BookShelfDO> newDOs = Optional.ofNullable(convertFromModelList(bookShelfModels)).orElse(Collections.emptyList());
//        updateDOs.addAll(newDOs);
//        if (!CollectionUtils.isEmpty(updateDOs)) {
//            int value = addBookShelf(updateDOs);
//            if (value <= 0) {
//                throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "书架更新失败，请重试");
//            }
//        }

        // 将新增的追加到所有的里面，按时间倒序排序后返回客户端（分页）
//        bookShelfDOS.addAll(newDOs);
//        bookShelfDOS = bookShelfDOS.stream().sorted((bf1, bf2) -> {
//            long bf1Time = bf1.getCreateTime().getTime();
//            long bf2Time = bf2.getCreateTime().getTime();
//            if (bf1Time == bf2Time) {
//                return 0;
//            } else if (bf1Time > bf2Time) {
//                return -1;
//            }
//            return 1;
//        }).collect(Collectors.toList());
//        return convertFromDataObjectList(bookShelfDOS);

        List<BookShelfDO> updateDOs = convertFromModelList(bookShelfModels);
        addBookShelf(updateDOs);
    }

    @Override
    public List<BookShelfModel> getByUserId(UserModel userModel, GetBookShelfRequest request) {
        request.defPage(10);
        List<BookShelfDO> bookShelfDOS = Optional.ofNullable(bookShelfDOMapper.selectByUserId(userModel.getId(),
                request.getPageNum(), request.getSize())).orElse(Collections.emptyList());
        return convertFromDataObjectList(bookShelfDOS);
    }

    @Override
    public int selectCountByUserIdAndBookId(Long userId, Long bookId) {
        return bookShelfDOMapper.selectCountByUserIdAndBookId(userId, bookId);
    }

    private List<BookShelfModel> convertFromRequest(UserModel userModel, AddBookShelfRequest request) {
        List<BookShelfModel> models = new ArrayList<>();
        request.getShelves().forEach(shelfRequest -> {
            BookShelfModel model = new BookShelfModel();
            BeanUtils.copyProperties(shelfRequest, model);
            model.setUserId(userModel.getId());
            model.setCreateTime(new Date(shelfRequest.getCreateTime()));
            models.add(model);
        });
        return models;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public int addBookShelf(List<BookShelfDO> list) throws BusinessException {
        if (CollectionUtils.isEmpty(list)) {
            return 0;
        }
        // TODO 检查书籍是否存在: 后台书籍删除后则无法
        List<Long> bookIds = list.stream().map(BookShelfDO::getBookId).collect(Collectors.toList());
        Integer active = BookAudit.SHELVE.getId();
        List<Long> existBookIds = bookDOMapper.checkBookExistsAndHasPermission(bookIds, active);
        if (CollectionUtils.isEmpty(bookIds) || CollectionUtils.isEmpty(existBookIds)) {
            log.info("无法加入书籍啊，因为未匹配到相应的书籍, id为：{}", bookIds);
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "无法加入书籍啊，因为未匹配到相应的书籍");
        }
        list.forEach(bookShelfDOMapper::saveOrUpdate);
        // TODO 后续加入到redis
        return 1;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteBookFromShelf(UserModel userModel, DeleteBookShelfRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        // 根据用户查询要删除的记录是否存在
        List<Long> ids = Optional.ofNullable(bookShelfDOMapper.selectByUserId(userModel.getId(), 1, Integer.MAX_VALUE)).orElse(Collections.emptyList()).stream().map(BookShelfDO::getBookId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(ids)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "未查询到用户的书架信息");
        }
        AtomicBoolean notExists = new AtomicBoolean(false);
        request.getBookIds().forEach(id -> {
            if (!ids.contains(id)) {
                notExists.set(true);
            }
        });
        if (notExists.get()) {
            // throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "要删除的记录不存在");
            log.info("要删除的记录不存在");
            return;
        }
        int state = bookShelfDOMapper.deleteBatch(request.getBookIds(), userModel.getId());
        if (state <= 0) {
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "删除失败，请重试");
        }
    }

    private List<BookShelfDO> convertFromModelList(List<BookShelfModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return Collections.emptyList();
        }
        List<BookShelfDO> dataObjects = new ArrayList<>();
        models.forEach(model -> {
            BookShelfDO bookShelfDO = new BookShelfDO();
            BeanUtils.copyProperties(model, bookShelfDO);
            bookShelfDO.setCreateTime(model.getCreateTime());
            dataObjects.add(bookShelfDO);
        });
        return dataObjects;
    }

    private List<BookShelfModel> convertFromDataObjectList(List<BookShelfDO> dataObjectList) {
        if (CollectionUtils.isEmpty(dataObjectList)) {
            return Collections.emptyList();
        }
        List<BookShelfModel> models = new ArrayList<>();
        dataObjectList.forEach(dataObject -> {
            BookShelfModel model = new BookShelfModel();
            BeanUtils.copyProperties(dataObject, model);
            if (dataObject.getBookDO() != null) {
                model.setBookName(dataObject.getBookDO().getName());
                model.setBookCover(dataObject.getBookDO().getCover());
            }
            model.setCreateTime(dataObject.getCreateTime());
            models.add(model);
        });
        return models;
    }
}
