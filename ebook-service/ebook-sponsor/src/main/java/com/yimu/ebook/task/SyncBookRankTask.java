package com.yimu.ebook.task;

import com.yimu.ebook.domain.dataobject.MonthHotRankDO;
import com.yimu.ebook.domain.model.BookModel;
import com.yimu.ebook.domain.model.HotRankModel;
import com.yimu.ebook.domain.vo.BookRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.BookRankService;
import com.yimu.ebook.service.BookService;
import com.yimu.ebook.util.RedisKeyUtils;
import com.yimu.ebook.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import java.time.LocalDate;
import java.util.*;

/**
 * @author Angus
 */
@Component
@Slf4j
public class SyncBookRankTask {

    private final BookService bookService;

    private final BookRankService bookRankService;

    private final RedisTemplate redisTemplate;

    public SyncBookRankTask(BookService bookService, BookRankService bookRankService,
        RedisTemplate redisTemplate) {
        this.bookService = bookService;
        this.bookRankService = bookRankService;
        this.redisTemplate = redisTemplate;
    }

    /**
     * 更新排名
     */
    @Scheduled(cron = "0 1 * * * *")
    @SuppressWarnings("all")
    public void updateBookRank() {
        // 检查是否已经更新过
        MonthHotRankDO latestOne = bookRankService.getLatestOne();
        if (latestOne != null) {
            LocalDate localDate = TimeUtil.convertDate2LocalDate(latestOne.getDate());
            LocalDate now = LocalDate.now();
            if (localDate.isEqual(now)) {
                log.info("今日已经更新过排名了！");
                return;
            }
        }
        List<Integer> ipControls = Arrays.asList(0,1,2,3);
        List<Integer> channels = Arrays.asList(1,2);
        BookRequest request = new BookRequest();
        channels.forEach(channel -> {
            request.setChannelId(channel);
            request.setSize(1000);
            ipControls.forEach(ipControl -> {
                final List<BookModel> bookList;
                try {
                    request.setIpControl(ipControl);
                    bookList = bookService.getBookListByCondition(request);
                    List<HotRankModel> hotRankModels = convertFromBookModel(bookList);
                    Collections.shuffle(hotRankModels);
                    List<List<HotRankModel>> lists = get(hotRankModels);
                    if (CollectionUtils.isEmpty(lists)) {
                        log.error("channel={} ipControl={} 同步排名出现问题了", channel, ipControl);
                        return;
                    }
                    bookRankService.saveTotalRank(lists.get(0));
                    bookRankService.saveMonthHotRank(lists.get(1));
                    bookRankService.saveWeekHotRank(lists.get(2));
                    bookRankService.saveHotSearchRank(lists.get(3));
                    bookRankService.saveNewBookHotRank(lists.get(4));
                    bookRankService.saveEditorRecommend(lists.get(5));

                    redisTemplate.opsForList().leftPush(RedisKeyUtils.RANK_TOTAL, lists.get(0));
                    redisTemplate.opsForList().trim(RedisKeyUtils.RANK_TOTAL, 0, lists.get(0).size());

                    redisTemplate.opsForList().leftPush(RedisKeyUtils.RANK_MONTH, lists.get(1));
                    redisTemplate.opsForList().trim(RedisKeyUtils.RANK_MONTH, 0, lists.get(1).size());

                    redisTemplate.opsForList().leftPush(RedisKeyUtils.RANK_WEEK, lists.get(2));
                    redisTemplate.opsForList().trim(RedisKeyUtils.RANK_WEEK, 0, lists.get(2).size());

                    redisTemplate.opsForList().leftPush(RedisKeyUtils.RANK_SEARCH, lists.get(3));
                    redisTemplate.opsForList().trim(RedisKeyUtils.RANK_SEARCH, 0, lists.get(3).size());

                    redisTemplate.opsForList().leftPush(RedisKeyUtils.RANK_NEWBOOK, lists.get(4));
                    redisTemplate.opsForList().trim(RedisKeyUtils.RANK_NEWBOOK, 0, lists.get(4).size());

                    redisTemplate.opsForList().leftPush(RedisKeyUtils.RANK_EDIT_RECOMMEND, lists.get(5));
                    redisTemplate.opsForList().trim(RedisKeyUtils.RANK_EDIT_RECOMMEND, 0, lists.get(5).size());
                } catch (BusinessException e) {
                    log.error("更新书籍失败", e.getMessage());
                }
            });
        });
    }

    private List<List<HotRankModel>> get(List<HotRankModel> hotRankModels) {
        final int rankSize = 6;
        if (CollectionUtils.isEmpty(hotRankModels) || hotRankModels.size() < rankSize) {
            return null;
        }
        List<List<HotRankModel>> list = new ArrayList<>(rankSize);
        for (int i = 0; i < rankSize; i++) {
            List<HotRankModel> single = new ArrayList<>();
            list.add(single);
        }
        for (int i = 0; i < hotRankModels.size(); i++) {
            int num = i % rankSize;
            list.get(num).add(hotRankModels.get(i));
        }
        return list;
    }

    private List convertFromBookModel(List<BookModel> bookModels) {
        if (CollectionUtils.isEmpty(bookModels)) {
            return Collections.EMPTY_LIST;
        }
        List<HotRankModel> list = new ArrayList<>();
        bookModels.forEach(bookModel -> {
            HotRankModel hotRankModel = new HotRankModel();
            hotRankModel.setBookId(bookModel.getId());
            hotRankModel.setBookName(bookModel.getName());
            hotRankModel.setAuthor(bookModel.getAuthor());
            hotRankModel.setHot(bookModel.getHot());
            hotRankModel.setCreateTime(new Date());
            hotRankModel.setChannelId(bookModel.getChannelId());
            hotRankModel.setDate(TimeUtil.convertLocalDate2Date(LocalDate.now()));
            hotRankModel.setIpControl(bookModel.getIpControl());
            list.add(hotRankModel);
        });
        return list;
    }

    public void deleteBookRank() {
        Date date = TimeUtil.convertLocalDate2Date(LocalDate.now());
        bookRankService.deleteWeekRankBeforeTime(date);
        bookRankService.deleteEditorRecommendBeforeTime(date);
        bookRankService.deleteHotSearchBeforeTime(date);
        bookRankService.deleteMonthRankBeforeTime(date);
        bookRankService.deleteNewbookBeforeTime(date);
        bookRankService.deleteTotalBeforeTime(date);
    }
}
