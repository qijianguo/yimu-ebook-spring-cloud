package com.yimu.ebook.domain.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdWithHannelsResponse {

  private List<AdResponse> ads;

  private AdHannelsResponse hannels;

}
