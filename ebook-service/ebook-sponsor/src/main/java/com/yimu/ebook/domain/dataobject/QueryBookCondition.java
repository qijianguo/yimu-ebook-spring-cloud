package com.yimu.ebook.domain.dataobject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryBookCondition {

  /**
   * 频道：1男频 2女频
   */
  private Integer channelId;

  /**
   * 分类ID
   */
  private Integer categoryId;

  /**
   * 子分类ID
   */
  private Integer subCategoryId;

  /**
   * 排序关键字：expire/create_time
   */
  private String sortKeyword;

  /**
   * 排序方式：升序asc/降序desc
   */
  private String sortBy;

  /**
   * 书籍状态
   */
  private Integer bookStatus;

  private Integer IpControl;

  /**
   * 是否应用
   */
  private Integer active;


  /**
   * 当前页
   */
  private Integer pageNum;

  /**
   * 每页显示数
   */
  private Integer size;

}
