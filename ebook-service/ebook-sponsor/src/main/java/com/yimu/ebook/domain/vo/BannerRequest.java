package com.yimu.ebook.domain.vo;

import java.util.ArrayList;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BannerRequest {

    /**
     * 频道：1男频 2女频
     */
    private Integer channelId;

    /**
     * 是否启用 0否 1是
     */
    private Integer active;

    /**
     * 排序关键字：expire/create_time
     */
    private String sortKeyword;

    /**
     * 排序方式：升序asc/降序desc
     */
    private String sortBy;

    /**
     * 是否排除已过期的：0否， 1是
     */
    private Integer excludeExpired;

    /**
     * 当前页
     */
    private Integer pageNum;

    /**
     * 每页显示数
     */
    private Integer size;

    public BannerRequest(Integer channelId, Integer size) {
        this.channelId = channelId;
        this.size = size;
    }

    public boolean validate() {
        /*if (null == channelId) {
            return false;
        }*/
        if (pageNum == null || pageNum == 0) {
            pageNum = 1;
        }
        if (size == null || size == 0) {
            size = 5;
        }
        return true;
    }

}
