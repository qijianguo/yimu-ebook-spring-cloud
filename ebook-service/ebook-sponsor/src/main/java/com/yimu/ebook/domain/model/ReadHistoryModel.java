package com.yimu.ebook.domain.model;

import lombok.Data;

import java.util.Date;

@Data
public class ReadHistoryModel {

    private Integer id;

    private Long userId;

    private Long bookId;

    private Integer chapterId;

    private Date time;

    private String bookName;

    private String bookCover;

    /**
     * 简介
     */
    private String bookSummary;

    /**
     * 章节标题
     */
    private String chapterTitle;

    private String chapterContent;

}