package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.ProblemModel;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface ProblemService {

    List<ProblemModel> getAll() throws BusinessException;
}
