package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.RoleDOMapper;
import com.yimu.ebook.dao.RolePermissionDOMapper;
import com.yimu.ebook.domain.dataobject.RoleDO;
import com.yimu.ebook.domain.dataobject.RolePermissionDO;
import com.yimu.ebook.domain.vo.RoleRequest;
import com.yimu.ebook.domain.vo.UpdateRoleRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.RoleService;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 * @author Angus
 */
@Service
public class RoleServiceImpl implements RoleService {

  private final RoleDOMapper roleDOMapper;
  private final RolePermissionDOMapper rolePermissionDOMapper;

  public RoleServiceImpl(
      RoleDOMapper roleDOMapper, RolePermissionDOMapper rolePermissionDOMapper) {
    this.roleDOMapper = roleDOMapper;
    this.rolePermissionDOMapper = rolePermissionDOMapper;
  }

  @Override
  @Transactional(rollbackOn = BusinessException.class)
  public void saveRole(RoleRequest request) {
    if (!request.validateSave()) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
    }
    RoleDO roleDO = roleDOMapper.selectByName(request.getName());
    if(roleDO != null) {
      throw new BusinessException(EmBusinessError.ROLE_ALREADY_EXISTS);
    }
    roleDO = new RoleDO();
    roleDO.setName(request.getName());
    roleDO.setDescription(request.getDescription());
    roleDOMapper.insertSelective(roleDO);
    Integer roleId = roleDO.getId();
    List<RolePermissionDO> list = new ArrayList<>();
    request.getPermissionIds().forEach(permissionId -> {
      RolePermissionDO rolePermissionDO = new RolePermissionDO();
      rolePermissionDO.setPermissionId(permissionId);
      rolePermissionDO.setRoleId(roleId);
      list.add(rolePermissionDO);
    });
    rolePermissionDOMapper.insertBatch(list);
  }

  @Override
  @Transactional(rollbackOn = BusinessException.class)
  public void updateRole(UpdateRoleRequest request) {
    if (!request.validateUpdate()) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
    }
    RoleDO roleDO = roleDOMapper.selectByPrimaryKey(request.getId());
    if(roleDO == null) {
      throw new BusinessException(EmBusinessError.ROLE_NOT_EXISTS);
    }
    if (request.getDescription() != null) {
      roleDO.setDescription(request.getDescription());
    }
    if (request.getName() != null) {
      roleDO.setName(request.getName());
    }
    roleDOMapper.updateByPrimaryKeySelective(roleDO);
    if (request.getPermissionIds() != null) {
      // TODO: 删除原来的
      rolePermissionDOMapper.deleteByRoleId(roleDO.getId());
      List<RolePermissionDO> list = new ArrayList<>();
      request.getPermissionIds().forEach(permissionId -> {
        RolePermissionDO rolePermissionDO = new RolePermissionDO();
        rolePermissionDO.setPermissionId(permissionId);
        rolePermissionDO.setRoleId(roleDO.getId());
        list.add(rolePermissionDO);
      });
      rolePermissionDOMapper.insertBatch(list);
    }
  }

  @Override
  @Transactional(rollbackOn = BusinessException.class)
  public void deleteRole(Integer id) {
    RoleDO roleDO = roleDOMapper.selectByPrimaryKey(id);
    if (roleDO == null) {
      throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "删除的记录不存在");
    }
    roleDOMapper.deleteByPrimaryKey(roleDO.getId());
    rolePermissionDOMapper.deleteByRoleId(roleDO.getId());
  }

}
