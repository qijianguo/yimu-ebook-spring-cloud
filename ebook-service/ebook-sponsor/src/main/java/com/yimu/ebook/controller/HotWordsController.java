package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.HotWordsModel;
import com.yimu.ebook.domain.vo.HotWordsResponse;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.service.HotWordsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "hotWords", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "热词管理")
public class HotWordsController {

    @Autowired
    private HotWordsService hotWordsService;


    @ApiOperation(value = "查询关键词")
    @GetMapping
    public Result get() throws BusinessException {
        List<HotWordsModel> models = hotWordsService.getAll();
        return Result.success(convertFromModelList(models));
    }

    private List<HotWordsResponse> convertFromModelList(List<HotWordsModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return Collections.emptyList();
        }
        List<HotWordsResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            HotWordsResponse response = new HotWordsResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }

}
