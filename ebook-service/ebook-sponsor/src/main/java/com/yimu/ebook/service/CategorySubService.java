package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.SubCategoryModel;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface CategorySubService {

    /**
     * 根据父分类查询子分类列表
     */
    List<SubCategoryModel> getByCategoryId(Integer categoryId);

    /**
     * 根据子分类查询父类和子类
     */
    SubCategoryModel getSubCategory(Integer subCategoryId) throws BusinessException;

}
