package com.yimu.ebook.config;

import com.yimu.ebook.interceptor.AuthenticationInterceptor;
import com.yimu.ebook.interceptor.AppVersionCheckInterceptor;
import com.yimu.ebook.resolvers.CurrentUserMethodArgumentResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * 配置类，增加自定义拦截器和解析器
 * @author Angus
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    private final AuthenticationInterceptor authenticationInterceptor;


    private final CurrentUserMethodArgumentResolver currentUserMethodArgumentResolver;

    private final AppVersionCheckInterceptor appVersionCheckInterceptor;

    public MvcConfig(AuthenticationInterceptor authenticationInterceptor,
                     CurrentUserMethodArgumentResolver currentUserMethodArgumentResolver,
                     AppVersionCheckInterceptor appVersionCheckInterceptor) {
        this.authenticationInterceptor = authenticationInterceptor;
        this.currentUserMethodArgumentResolver = currentUserMethodArgumentResolver;
        this.appVersionCheckInterceptor = appVersionCheckInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authenticationInterceptor);
        registry.addInterceptor(appVersionCheckInterceptor);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(currentUserMethodArgumentResolver);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);

    }
}
