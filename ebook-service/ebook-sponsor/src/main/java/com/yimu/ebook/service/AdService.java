package com.yimu.ebook.service;

import com.yimu.ebook.domain.dataobject.AdDO;
import com.yimu.ebook.domain.model.AdHannelsModel;
import com.yimu.ebook.domain.model.AdModel;
import java.util.List;

/**
 * @author Angus
 */
public interface AdService {

  List<AdModel> getRandomAdGroupByType();

  /**
   * 获取频道
   * @param sign
   * @return
   */
  AdHannelsModel getAdHannels(String sign);

  /**
   * 根据推广表示查询
   * @param tfids
   * @return
   */
  List<AdModel> selectByTfidIn(List<String> tfids);
}
