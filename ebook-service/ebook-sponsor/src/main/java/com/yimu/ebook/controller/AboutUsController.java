package com.yimu.ebook.controller;


import com.yimu.ebook.domain.model.AboutUsModel;
import com.yimu.ebook.domain.vo.AboutUsResponse;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.service.AboutUsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "aboutUs", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description = "关于我们")
public class AboutUsController {

    @Autowired
    private AboutUsService aboutUsService;

    @ApiOperation(value = "获取联系方式")
    @GetMapping
    public Result getContact() {
        return Result.success(convertFromModel(aboutUsService.getAll()));
    }

    private List<AboutUsResponse> convertFromModel(List<AboutUsModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.EMPTY_LIST;
        }
        List<AboutUsResponse> responses = new ArrayList<>();
        list.forEach(model -> {
            AboutUsResponse response = new AboutUsResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }
}
