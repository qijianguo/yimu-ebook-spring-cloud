package com.yimu.ebook.service;

import com.yimu.ebook.domain.vo.RoleRequest;
import com.yimu.ebook.domain.vo.UpdateRoleRequest;

/**
 * @author Angus
 */
public interface RoleService {

  /**
   * 保存角色
   * @param request
   */
  void saveRole(RoleRequest request);

  /**
   * 修改角色
   * @param request
   */
  void updateRole(UpdateRoleRequest request);

  /**
   * 删除角色
   * @param id
   */
  void deleteRole(Integer id);
}
