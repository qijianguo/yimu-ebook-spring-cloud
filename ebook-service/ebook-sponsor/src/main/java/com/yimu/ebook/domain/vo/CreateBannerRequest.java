package com.yimu.ebook.domain.vo;

import com.yimu.ebook.domain.model.Channel;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateBannerRequest {

  private String description;

  private String image;

  private String link;

  private String startTime;

  private String endTime;

  private Integer forever;

  private Integer priority;

  private Integer channelId;

  private Integer active;

  public boolean validate() {
    return channelId != null && (Arrays.asList(Channel.MALE_FEMALE.getId(), Channel.MALE.getId(), Channel.FEMALE.getId()).contains(channelId))
        && image != null
        && link != null
        && startTime != null
        && priority != null
        && active != null && (active == 0 || active == 1)
        ;
  }
  
}
