package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.Authentication;
import com.yimu.ebook.annotation.CurrentUser;
import com.yimu.ebook.domain.model.BookAudit;
import com.yimu.ebook.domain.model.BookChapterModel;
import com.yimu.ebook.domain.model.BookModel;
import com.yimu.ebook.domain.model.ReadHistoryModel;
import com.yimu.ebook.domain.model.RecommendedBooksRequest;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.*;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.interceptor.AppVersionCheckInterceptor;
import com.yimu.ebook.service.BookService;
import com.yimu.ebook.service.BookShelfService;
import com.yimu.ebook.service.ReadHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "/book", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.TEXT_HTML_VALUE})
@CrossOrigin
@Api(description = "书籍管理")
public class  BookController {

    private final BookService bookService;

    private final ReadHistoryService readHistoryService;

    private final BookShelfService bookShelfService;

    @Autowired
    public BookController(BookService bookService, ReadHistoryService readHistoryService,
        BookShelfService bookShelfService) {
        this.bookService = bookService;
        this.readHistoryService = readHistoryService;
        this.bookShelfService = bookShelfService;
    }

    @ApiOperation(value = "查询书籍列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "categoryId", value = "父分类Id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "subCategoryId", value = "子分类Id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "bookStatus", value = "书籍状态：不限状态-1，连载0，完结1", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sortKeyword", value = "排序关键字：hot/favorites", dataType = "string", paramType = "query", defaultValue = "hot"),
            @ApiImplicitParam(name = "sortBy", value = "排序方式：升序asc/降序desc", dataType = "string", paramType = "query", defaultValue = "desc"),
    })
    @GetMapping("/list")
    public Result getBookList(BookRequest request) throws BusinessException {
        request.setIpControl(AppVersionCheckInterceptor.ipControl.get());
        request.setActive(BookAudit.SHELVE.getId());
        List<BookModel> list = bookService.getBookListByCondition(request);
        return Result.success(convertSimpleFromModelList(list));
    }

    @ApiOperation(value = "查询书籍详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authentication", value = "token", dataType = "string", paramType = "header"),
            @ApiImplicitParam(name = "updateTime", value = "更新时间", dataType = "long", paramType = "query"),
    })
    @GetMapping(value = "/{bookId}")
    @Authentication(roleIds = {Role.ALL})
    public Result getBookDetail(@ApiIgnore @CurrentUser UserModel userModel, @PathVariable Long bookId, Long updateTime) throws BusinessException {
        BookModel bookModel = bookService.getBookDetailById(bookId);
        // Andy: 方便更新书架的缓存信息
        if (updateTime != null && bookModel.getUpdateTime().getTime() == updateTime) {
            return Result.success();
        }
        BookResponse response = convertFromModel(bookModel);
        int chaptersCount = bookService.getCountChapterByBookId(bookId);
        response.setChaptersCount(chaptersCount);

        // 添加用户阅读历史：若用户已登陆，则添加书籍的最近章节内容
        if (userModel != null) {
            ReadHistoryModel readHistoryModel = readHistoryService.selectByUserIdsAndBookId(userModel.getId(), bookModel.getId());
            if (readHistoryModel != null) {
                response.setLatestReadChapterId(readHistoryModel.getChapterId());
            }
            // 是否收藏过
            int in = bookShelfService.selectCountByUserIdAndBookId(userModel.getId(), bookId) > 0 ? 1 : 0;
            response.setInBookShelf(in);
        }
        return Result.success(response);
    }

    @ApiOperation(value = "根据书籍推荐")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "4"),
        @ApiImplicitParam(name = "bookId", value = "书籍ID", required = true, dataType = "Long", paramType = "query"),
    })
    @GetMapping("/recommended")
    public Result getRecommendedBooks(RecommendedBooksRequest request) throws BusinessException {
        List<BookModel> list = bookService.getRecommendedBooks(request);
        return Result.success(convertSimpleFromModelList(list));
    }

    @ApiOperation(value = "分页查询章节列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "bookId", value = "书籍ID", required = true, dataType = "Long", paramType = "query"),
    })
    @GetMapping("/chapters")
    public Result getBookChapterList(BookChapterRequest request) throws BusinessException {
        List<BookChapterModel> list = bookService.getBookChapter(request);
        List<BookChapterResponse> responses = convertFromModel(list);
        return Result.success(
            new Page(request.getPageNum(), request.getSize(), bookService.getCountChapterByBookId(request.getBookId()), responses)
        );
    }

    @ApiOperation(value = "书籍检索功能")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "10"),
            @ApiImplicitParam(name = "highlight", value = "是否高亮：1开启 0不开启", dataType = "int", paramType = "query", defaultValue = "0"),
            @ApiImplicitParam(name = "keyword", value = "关键字，例如：斗破苍穹，可输入斗破、苍穹", required = true, dataType = "string", paramType = "query"),
    })
    @GetMapping("/search/name")
    public Result getBookChapterList(SearchRequest request) throws BusinessException {
        List<SearchResponse> list = bookService.searchFromSolr(request);
        return Result.success(list);
    }

    @ApiOperation(value = "修改书籍")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bookId", value = "书籍唯一ID", required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "书籍名称", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "author", value = "作者", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "cover", value = "封面", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "hot", value = "热度", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "favorites", value = "收藏", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "score", value = "评分", dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "summary", value = "简介", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "wordSize", value = "字数：单位万字", dataType = "double", paramType = "query"),
            @ApiImplicitParam(name = "createTime", value = "上架时间", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "updateTime", value = "更新时间", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "categoryId", value = "父分类Id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "subCategoryId", value = "子分类Id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "channelId", value = "频道ID：1男频 2女频", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "bookStatusId", value = "书籍状态：1连载 2完结", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "active", value = "书籍审核状态：-3审核中/-2审核成功/-1审核失败/0下架/1上架", dataType = "int", paramType = "query", defaultValue = "0"),
    })
    @PostMapping
    public Result update(CreateBookRequest request) throws BusinessException {
        bookService.update(request);
        return Result.success("保存成功");
    }

    @ApiOperation(value = "检查书籍更新状态")
    @GetMapping("/checkUpdate")
    public Result checkUpdate(QueryBookUpdateRequest request) throws BusinessException {
        List<QueryBookUpdateResponse> bookUpdate = bookService.checkBookUpdate(request);
        return Result.success(bookUpdate);
    }

    @ApiOperation(value = "更新章节内容")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "authentication", value = "token", required = true, dataType = "string", paramType = "header"),
        @ApiImplicitParam(name = "contentUrl", value = "章节URL", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "content", value = "章节内容", required = true, dataType = "string", paramType = "query"),
    })
    @PutMapping("/chapter/content")
    @Authentication(roleIds = Role.ADMIN)
    public Result updateChapterContent(UpdateChapterContentRequest request) throws BusinessException {
        try {
            bookService.updateChapterContent(request);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(EmBusinessError.UNKNOW_ERROR.getErrorCode(), "上传失败");
        }
        return Result.success("更新成功");
    }

    @ApiOperation(value = "查询章节内容")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "contentUrls", value = "章节URL", required = true, dataType = "list", paramType = "query"),
    })
    @GetMapping("/chapter/content")
    public Result getChapterContents(GetChapterRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<GetChapterResponse> responses = new ArrayList<>();
        request.getContentUrls().forEach(contentUrl -> {
            String chapterContent = bookService.getChapterContent(contentUrl);

            GetChapterResponse response = new GetChapterResponse();
            response.setContent(chapterContent);
            response.setContentUrl(contentUrl);
            responses.add(response);
        });
        return Result.success(responses);
    }

    private List<BookSimpleResponse> convertSimpleFromModelList(List<BookModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return Collections.emptyList();
        }
        List<BookSimpleResponse> responses = new ArrayList<>();
        models.forEach(model -> responses.add(convertSimpleFromModel(model)));
        return responses;
    }

    private BookResponse convertFromModel(BookModel bookModel) {
        if (bookModel == null) {
            return null;
        }
        BookResponse bookResponse = new BookResponse();
        BeanUtils.copyProperties(bookModel, bookResponse);
        bookResponse.setStatus(bookModel.getBookStatusId() == 1 ? "完结" : "连载");
        return bookResponse;
    }

    private BookSimpleResponse convertSimpleFromModel(BookModel bookModel) {
        if (bookModel == null) {
            return null;
        }
        BookSimpleResponse bookSimpleResponse = new BookSimpleResponse();
        BeanUtils.copyProperties(bookModel, bookSimpleResponse);
        bookSimpleResponse.setStatus(bookModel.getBookStatusId() == 1 ? "完结" : "连载");
        return bookSimpleResponse;
    }

    private List convertFromModel(List<BookChapterModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.EMPTY_LIST;
        }
        List<BookChapterResponse> result = new ArrayList<>();
        list.forEach(model -> {
            BookChapterResponse response = new BookChapterResponse();
            BeanUtils.copyProperties(model, response);
            response.setId(model.getChapterId());
            result.add(response);
        });

        return result;
    }


}
