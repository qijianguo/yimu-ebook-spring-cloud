package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.BannerDOMapper;
import com.yimu.ebook.domain.dataobject.BannerDO;
import com.yimu.ebook.domain.dataobject.QueryBannerCondition;
import com.yimu.ebook.domain.model.BannerModel;
import com.yimu.ebook.domain.model.BannerType;
import com.yimu.ebook.domain.model.Gender;
import com.yimu.ebook.domain.vo.BannerRequest;
import com.yimu.ebook.domain.vo.CreateBannerRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.BannerService;
import com.yimu.ebook.util.TimeUtil;
import java.util.Date;
import javax.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Angus
 */
@Service
public class BannerServiceImpl implements BannerService {

    private final BannerDOMapper bannerDOMapper;

    @Autowired
    public BannerServiceImpl(BannerDOMapper bannerDOMapper) {
        this.bannerDOMapper = bannerDOMapper;
    }

    @Override
    public List<BannerType> getAllBannerTypes() {
        List<BannerType> list = new ArrayList<>();
        list.add(new BannerType(Gender.MALE.getType(), Gender.MALE.getName()));
        list.add(new BannerType(Gender.FEMALE.getType(), Gender.FEMALE.getName()));
        return list;
    }

    @Override
    public List<BannerModel> getBannerByCondition(BannerRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        QueryBannerCondition condition = new QueryBannerCondition();
        BeanUtils.copyProperties(request, condition);
        List<BannerDO> list = bannerDOMapper.selectByCondition(condition);
        return convertFromDataObjectList(list);
    }

    @Override
    @Transactional(rollbackOn = BusinessException.class)
    public void save(CreateBannerRequest request) {
        if(!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        bannerDOMapper.insert(convertFromRequest(request));
    }

    private BannerDO convertFromRequest(CreateBannerRequest request) {
        BannerDO bannerDO = new BannerDO();
        BeanUtils.copyProperties(request, bannerDO);
        bannerDO.setCreateTime(new Date());
        bannerDO.setStartTime(TimeUtil.convertString2Date(request.getStartTime(), TimeUtil.YYYY_HH_MM));
        bannerDO.setEndTime(TimeUtil.convertString2Date(request.getEndTime(), TimeUtil.YYYY_HH_MM));
        return bannerDO;
    }

    private List<BannerModel> convertFromDataObjectList(List<BannerDO> bannerDOList) throws BusinessException {
        if (bannerDOList == null) {
            throw new BusinessException(EmBusinessError.NOT_FOUND_RESOURCE);
        }
        List<BannerModel> bannerModelList = new ArrayList<>();
        bannerDOList.forEach(bannerDO -> {
            BannerModel model = new BannerModel();
            BeanUtils.copyProperties(bannerDO, model);
            bannerModelList.add(model);
        });
        return bannerModelList;
    }
}
