package com.yimu.ebook.service.impl;

import com.yimu.ebook.constant.Constants;
import com.yimu.ebook.dao.AppVersionDOMapper;
import com.yimu.ebook.domain.dataobject.AppVersionDO;
import com.yimu.ebook.domain.model.AppVersionModel;
import com.yimu.ebook.domain.vo.UpdateVersionRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.AppVersionService;
import com.yimu.ebook.util.RedisKeyUtils;
import javax.transaction.Transactional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author Angus
 */
@Service
public class AppVersionServiceImpl implements AppVersionService {

    private final AppVersionDOMapper appVersionDOMapper;



    private final RedisTemplate redisTemplate;

    @Autowired
    public AppVersionServiceImpl(AppVersionDOMapper appVersionDOMapper, RedisTemplate redisTemplate) {
        this.appVersionDOMapper = appVersionDOMapper;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public AppVersionModel getByPlatform(String platform) throws BusinessException {
        if (platform == null || !Constants.APP_PLATFORMS.contains(platform)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        // 检查redis中是否存在
        Object object = redisTemplate.opsForValue().get(RedisKeyUtils.platformVersion(platform));
        AppVersionDO appVersionDO;
        if (object instanceof AppVersionDO) {
            appVersionDO = (AppVersionDO) object;
        } else {
            appVersionDO = appVersionDOMapper.selectByPlatformOrderByCreateTimeDesc(platform);
            redisTemplate.opsForValue().set(RedisKeyUtils.platformVersion(platform), appVersionDO);
        }
        return convertFromDataObject(appVersionDO);
    }

    @Override
    public void cacheAppVersion() {
        List<AppVersionDO> appVersionDOS = appVersionDOMapper.selectGroupByPlatformOrderByCreateTimeDesc();
        appVersionDOS.forEach(version -> redisTemplate.opsForValue().set(RedisKeyUtils.platformVersion(version.getPlatform()), version));
    }

    @Override
    @Transactional(rollbackOn = BusinessException.class)
    public void updateVersion(UpdateVersionRequest request) throws BusinessException{
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        int state = appVersionDOMapper.updateByPlatformSelective(convertFromRequest(request));
        if (state == 0) {
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "更新版本失败");
        }
        cacheAppVersion();
    }

    private AppVersionDO convertFromRequest(UpdateVersionRequest request) {
        AppVersionDO appVersionDO = new AppVersionDO();
        BeanUtils.copyProperties(request, appVersionDO);
        return appVersionDO;
    }

    private AppVersionModel convertFromDataObject(AppVersionDO appVersionDO) {
        if (appVersionDO == null) {
            return null;
        }
        AppVersionModel model = new AppVersionModel();
        BeanUtils.copyProperties(appVersionDO, model);
        return model;
    }
}
