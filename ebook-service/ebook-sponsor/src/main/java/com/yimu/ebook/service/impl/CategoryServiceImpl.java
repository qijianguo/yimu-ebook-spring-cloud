package com.yimu.ebook.service.impl;
import com.yimu.ebook.dao.CategoryDOMapper;
import com.yimu.ebook.domain.dataobject.CategoryDO;
import com.yimu.ebook.domain.model.CategoryModel;
import com.yimu.ebook.domain.model.SubCategoryModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryDOMapper categoryDOMapper;

    @Autowired
    public CategoryServiceImpl(CategoryDOMapper categoryDOMapper) {
        this.categoryDOMapper = categoryDOMapper;
    }

    @Override
    public List<CategoryModel> getAll() {
        List<CategoryDO> list = categoryDOMapper.selectAll();
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.EMPTY_LIST);
    }

    @Override
    public List<CategoryModel> selectAllActive() {
        List<CategoryDO> list = categoryDOMapper.selectAllActive();
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.EMPTY_LIST);
    }


    @Override
    public List<CategoryModel> getAllAndSub() {
        List<CategoryDO> list = categoryDOMapper.getAllAndSub();
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.EMPTY_LIST);
    }

    @Override
    public List<CategoryModel> getByChannel(Integer channelId) throws BusinessException {
        if(channelId == null) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "未选择频道");
        }
        List<CategoryDO> list = categoryDOMapper.selectByChannelId(channelId);
        return Optional.ofNullable(convertFromDataObjectList(list)).orElse(Collections.EMPTY_LIST);
    }

    @Override
    public CategoryModel getByParentId(Integer parentId) {
        CategoryDO categoryDO = categoryDOMapper.selectByPrimaryKey(parentId);
        return convertFromDataObject(categoryDO);
    }

    @Override
    public void updateCategory(List<CategoryModel> list) {
        List<CategoryDO> categoryModels = Optional.ofNullable(convertFromModelList(list)).orElse(Collections.emptyList());
        categoryModels.forEach(categoryDO -> categoryDOMapper.updateActiveByPrimaryKey(categoryDO.getId(), categoryDO.getActive()));
    }

    private List<CategoryDO> convertFromModelList(List<CategoryModel> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.EMPTY_LIST;
        }
        List<CategoryDO> models = new ArrayList<>();
        list.forEach(dataObject -> models.add(convertFromModel(dataObject)));
        return models;
    }

    private CategoryDO convertFromModel(CategoryModel model) {
        if (model == null) {
            return null;
        }
        CategoryDO categoryDO = new CategoryDO();
        BeanUtils.copyProperties(model, categoryDO);
        return categoryDO;
    }

    private List<CategoryModel> convertFromDataObjectList(List<CategoryDO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.EMPTY_LIST;
        }
        List<CategoryModel> models = new ArrayList<>();
        list.forEach(dataObject -> models.add(convertFromDataObject(dataObject)));
        return models;
    }

    private CategoryModel convertFromDataObject(CategoryDO dataObject) {
        if (dataObject == null) {
            return null;
        }
        CategoryModel model = new CategoryModel();
        BeanUtils.copyProperties(dataObject, model);
        List<SubCategoryModel> subModels = new CategorySubServiceImpl().convertFromDataObjectList(
                Optional.ofNullable(dataObject.getCategorySubList()).orElse(Collections.emptyList()));
        model.setSubCategoryModelList(subModels);
        return model;
    }

}

