package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAgreementModel {

  private Integer id;

  private String url;

  private String description;

  private String platform;
}
