package com.yimu.ebook.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class AddBookShelfRequest {

    private List<BookShelfRequest> shelves;

    public boolean validate() {
        return shelves != null;
    }
}
