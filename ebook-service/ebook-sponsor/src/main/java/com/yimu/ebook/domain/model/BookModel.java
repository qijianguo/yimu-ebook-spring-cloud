package com.yimu.ebook.domain.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BookModel {

    private Long id;

    private String uuid;

    /**
     * 书名
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    /**
     * 封面图片
     */
    private String cover;

    /**
     * 字数：单位（万字）
     */
    private Double wordSize;

    /**
     * 阅读人数
     */
    private Long hot;

    /**
     * 收藏人数
     */
    private Long favorites;

    /**
     * 评分
     */
    private BigDecimal score;

    /**
     * 简介
     */
    private String summary;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 分类ID
     */
    private Integer categoryId;

    private Integer subCategoryId;
    /**
     * 频道ID
     */
    private Integer channelId;

    /**
     * 书籍状态
     */
    private Integer bookStatusId;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 子分类名称
     */
    private String subCategoryName;

    private Integer ipControl;


}
