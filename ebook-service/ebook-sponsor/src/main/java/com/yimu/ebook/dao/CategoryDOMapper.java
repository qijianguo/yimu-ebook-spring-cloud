package com.yimu.ebook.dao;

import com.yimu.ebook.domain.dataobject.CategoryDO;
import com.yimu.ebook.domain.dataobject.CategoryDOExample;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CategoryDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    int insert(CategoryDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    int insertSelective(CategoryDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    List<CategoryDO> selectByExample(CategoryDOExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    CategoryDO selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    int updateByPrimaryKeySelective(CategoryDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_category
     *
     * @mbg.generated Mon May 27 13:28:52 CST 2019
     */
    int updateByPrimaryKey(CategoryDO record);

    /**
     * 查询所有
     * @return
     */
    List<CategoryDO> selectAll();

    List<CategoryDO> selectAllActive();

    /**
     * 查询所有
     * @return
     */
    List<CategoryDO> getAllAndSub();

    /**
     * 根据频道查询
     * @param channelId
     * @return
     */
    List<CategoryDO> selectByChannelId(Integer channelId);

    int updateActiveByPrimaryKey(@Param("id") Integer id, @Param("active") Integer active);


}