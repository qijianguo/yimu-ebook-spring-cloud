package com.yimu.ebook.service.impl;

import com.yimu.ebook.constant.Constants;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.TokenService;
import com.yimu.ebook.util.MD5Utils;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

/**
 * 通过Redis存储和验证token的实现类
 * @author Angus
 */
@Component
public class TokenServiceImpl implements TokenService {
    private final RedisTemplate redisTemplate;

    @Autowired
    public TokenServiceImpl(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void createToken(UserModel user) throws BusinessException {
        if (user == null) {
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        /*if (user.getTelphone() == null) {
            throw new BusinessException(EmBusinessError.USER_NOT_TELPHONE);
        }*/
        //使用uuid作为源token
        String token = UUID.randomUUID().toString();
        user.setToken(token);
        //存储到redis并设置过期时间
        redisTemplate.boundValueOps(user.generateRedisKey()).set(user, Constants.TOKEN_EXPIRES_HOUR, TimeUnit.HOURS);
    }


    @Override
    public UserModel checkToken(String token) throws BusinessException {
        if (token == null) {
            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN);
        }
        UserModel userModel = (UserModel) redisTemplate.opsForValue().get(new UserModel().generateRedisKey(token));
        if (userModel == null) {
            throw new BusinessException(EmBusinessError.USER_TOKEN_EXPIRE);
        }
        //如果验证成功，说明此用户进行了一次有效操作，延长token的过期时间
        redisTemplate.boundValueOps(userModel.generateRedisKey()).expire(Constants.TOKEN_EXPIRES_HOUR, TimeUnit.HOURS);
        return userModel;
    }

    @Override
    public void deleteToken(String token) {
        redisTemplate.delete(new UserModel().generateRedisKey(token));
    }
}
