package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AboutUsResponse {

  private String contactType;

  private String contact;

  private String description;

}
