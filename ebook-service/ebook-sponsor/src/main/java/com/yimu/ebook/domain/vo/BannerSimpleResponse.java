package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BannerSimpleResponse {

    private String image;

    private String link;

    private String name;

}
