package com.yimu.ebook.config;

import com.yimu.ebook.annotation.CurrentUser;
import com.yimu.ebook.domain.model.UserModel;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Angus
 */
@EnableSwagger2
@Configuration
@ConditionalOnProperty(name = "swagger.enable", havingValue = "true")
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return  new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.yimu.ebook.controller"))
                .paths(PathSelectors.regex("/ebook-sponsor/**"))
                .build()
                .ignoredParameterTypes(CurrentUser.class)
                .ignoredParameterTypes(UserModel.class);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("EBOOK—SPRING-CLOUD-API")
                .description("牛角读书服务接口")
                .contact("Angus")
                .version("1.0")
                .build();
    }
}