package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookShelfResponse {

    private Integer id;

    private Long bookId;

    private Integer chapterId;

    private String contentUrl;

    private String bookName;

    private String bookCover;

    private Date time;

}
