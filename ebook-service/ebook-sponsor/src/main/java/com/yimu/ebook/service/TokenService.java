package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.AdministratorModel;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.exception.BusinessException;

/**
 * 对Token进行操作的接口
 * @author Angus
 */
public interface TokenService {

    /**
     * 创建一个token关联上指定用户
     * @param user 用户
     * @return 生成的token
     */
    void createToken(UserModel user) throws BusinessException;

    /**
     * 检查token是否有效
     * @param token 登录用户的token
     * @return 有效返回用户, 无效则返回null
     */
     UserModel checkToken(String token) throws BusinessException;

    /**
     * 清除token
     * @param token 登录用户的token
     */
    void deleteToken(String token);

}