package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.AboutUsDOMapper;
import com.yimu.ebook.domain.dataobject.AboutUsDO;
import com.yimu.ebook.domain.model.AboutUsModel;
import com.yimu.ebook.service.AboutUsService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author Angus
 */
@Service
public class AboutUsServiceImpl implements AboutUsService {

  private final AboutUsDOMapper aboutUsDOMapper;

  public AboutUsServiceImpl(AboutUsDOMapper aboutUsDOMapper) {
    this.aboutUsDOMapper = aboutUsDOMapper;
  }

  @Override
  public List<AboutUsModel> getAll() {
    return convertFromDataObject(aboutUsDOMapper.selectAll());
  }

  private List<AboutUsModel> convertFromDataObject(List<AboutUsDO> list) {
    if (CollectionUtils.isEmpty(list)) {
      return Collections.emptyList();
    }
    List<AboutUsModel> models = new ArrayList<>();
    list.forEach(dataObject -> {
      AboutUsModel model = new AboutUsModel();
      BeanUtils.copyProperties(dataObject, model);
      models.add(model);
    });
    return models;
  }
}
