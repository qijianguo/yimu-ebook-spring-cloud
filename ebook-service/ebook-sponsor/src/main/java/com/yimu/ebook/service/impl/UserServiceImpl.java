package com.yimu.ebook.service.impl;

import static com.yimu.ebook.constant.Constants.MINUTE;
import static com.yimu.ebook.constant.Constants.TELPHONE_CODE_DAY_MAX_SIZE;
import static com.yimu.ebook.constant.Constants.TELPHONE_CODE_MINUTE_MAX_SIZE;
import static com.yimu.ebook.constant.Constants.TELPHONE_CODE_TIMEOUT_5_MINUTE;

import com.yimu.ebook.constant.Constants;
import com.yimu.ebook.dao.UserDOMapper;
import com.yimu.ebook.domain.dataobject.UserDO;
import com.yimu.ebook.domain.model.Role;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.LoginRequest;
import com.yimu.ebook.domain.vo.TelphoneCodeRecord;
import com.yimu.ebook.domain.vo.UserRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.TokenService;
import com.yimu.ebook.service.UserService;
import com.yimu.ebook.util.MD5Utils;
import com.yimu.ebook.util.RedisKeyUtils;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import weixin.popular.api.SnsAPI;
import weixin.popular.bean.BaseResult;
import weixin.popular.bean.sns.SnsToken;
import weixin.popular.bean.user.User;

/**
 * @author Angus
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final TokenService tokenService;
    private final UserDOMapper userDOMapper;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public UserServiceImpl(TokenService tokenService, UserDOMapper userDOMapper) {
        this.tokenService = tokenService;
        this.userDOMapper = userDOMapper;
    }

    @Override
    public int getTelphoneCode(String telphone) throws BusinessException {
        if (!validateTelphone(telphone)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        // 1分钟请求一次，一天最多请求10次， 5分钟过期时间
        Object object = redisTemplate.boundValueOps(RedisKeyUtils.userLoginCode(telphone)).get();
        TelphoneCodeRecord map = null;
        if (object instanceof TelphoneCodeRecord) {
            map = (TelphoneCodeRecord) object;
        }
        // 获取验证码
        int code = new Random().nextInt(9000) + 1000;
        Long now = System.currentTimeMillis();
        if (map != null) {
            Long latestTime = map.getLatestTime();
            long second = (now - latestTime) / 1000;
            log.info("花费的时间为：{} 秒", second);
            if ((now - latestTime) / TELPHONE_CODE_MINUTE_MAX_SIZE < 1) {
                throw new BusinessException(EmBusinessError.QUEST_CODE_FREQUENCY, String.format("验证码获取太频繁,请%s秒后重试", 60 - second));
            }

            if (map.getTimes() > TELPHONE_CODE_DAY_MAX_SIZE) {
                throw new BusinessException(EmBusinessError.QUERY_CODE_TIMES_MAXED);
            }
            map.setLatestTime(now);
            map.setTimes(map.getTimes() + 1);
            map.setCode(code);
            redisTemplate.boundValueOps(RedisKeyUtils.userLoginCode(telphone)).set(map);
        } else {
            map = new TelphoneCodeRecord(code, now, now, 1);
            redisTemplate.boundValueOps(RedisKeyUtils.userLoginCode(telphone)).set(map, Constants.TELPHONE_CODE_HOUR, TimeUnit.HOURS);
        }
        return code;
    }

    @Override
    @Transactional(rollbackOn = BusinessException.class)
    public UserModel loginByTelphone(LoginRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        Object obj = redisTemplate.boundValueOps(RedisKeyUtils.userLoginCode(request.getTelphone())).get();
        TelphoneCodeRecord map = null;
        if (obj instanceof TelphoneCodeRecord) {
            map = (TelphoneCodeRecord) obj;
        }
        if (map == null || request.getCode() != map.getCode()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "请填写正确的验证码");
        }
        Long now = System.currentTimeMillis();
        Long latestTime = map.getLatestTime();
        if ((now - latestTime) > TELPHONE_CODE_TIMEOUT_5_MINUTE) {
            log.info("花费的时间为：{} 秒", (now - latestTime) / MINUTE);
            throw new BusinessException(EmBusinessError.QUEST_CODE_FREQUENCY, "验证码超时");
        }
        // 是否已经注册过
        UserDO userDO = userDOMapper.selectByTelphone(request.getTelphone());
        if (userDO == null) {
            userDO = new UserDO(request.getTelphone(), MD5Utils.MD5(request.getTelphone()));
            Integer channelId = request.getChannelId();
            userDO.setGender(channelId);
            userDO.setChannelId(channelId);
            // TODO 暂时按频道选择图片
            userDO.setHeadImgUrl(getHeadImgUrl(channelId));
            int save = userDOMapper.insert(userDO);
            if (save == 0) {
                log.error("UserServiceImpl.loginByTelphone: 保存用户失败！");
                throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "保存用户失败");
            }
        }
        // 登陆后清除验证码
        redisTemplate.delete(RedisKeyUtils.userLoginCode(request.getTelphone()));

        // 登录成功
        UserModel userModel = convertFromDataObject(userDO);
        userModel.setRoleId(Role.ALL);
        tokenService.createToken(userModel);
        return userModel;
    }

    @Override
    public UserModel loginByWeiXin(String code) throws BusinessException {
        String strAppId = "";
        String strAppSecret = "";
        SnsToken token = SnsAPI.oauth2AccessToken(strAppId, strAppSecret, code);
        BaseResult baseResult = SnsAPI.auth(token.getAccess_token(), token.getOpenid());
        final String WINXIN_CODE = "0";
        final String WINXIN_MSG = "ok";
        if(WINXIN_CODE.equals(baseResult.getErrcode()) && WINXIN_MSG.equals(baseResult.getErrmsg())){
            User user = SnsAPI.userinfo(token.getAccess_token(), token.getOpenid(), "zh-CN");
            log.info("user: " + user.toString());
            UserDO userDO = userDOMapper.selectByWxOpenId("weixin", user.getOpenid());
            if (userDO == null) {
                // 首次登录
                userDO = new UserDO(user.getOpenid(), user.getHeadimgurl(), user.getNickname(), user.getSex());
                int save = userDOMapper.insert(userDO);
                if (save == 0) {
                    log.error("UserServiceImpl.loginByTelphone: 保存用户失败！");
                    throw new BusinessException(EmBusinessError.UNKNOW_ERROR);
                }
            }
            return convertFromDataObject(userDO);
        }else {
            throw new BusinessException(EmBusinessError.WINXIN_AUTHORIZATION_FAIL);
        }
    }

    @Override
    public UserModel updateUserInfo(UserModel userModel, UserRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        int state = userDOMapper.updateByPrimaryKeySelective(convertFromQueryObject(userModel, request));
        if (state <= 0) {
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "用户修改失败，请重试");
        }
        UserModel um = convertFromDataObject(
            userDOMapper.selectByPrimaryKey(userModel.getId()));
        um.setToken(userModel.getToken());
        redisTemplate.boundValueOps(um.generateRedisKey()).set(um, Constants.TOKEN_EXPIRES_HOUR, TimeUnit.HOURS);
        return um;
    }

    @Override
    public void logout(UserModel userModel) {
        tokenService.deleteToken(userModel.generateRedisKey());
    }

    private UserModel convertFromDataObject(UserDO userDO) throws BusinessException {
        if (userDO == null) {
            throw new BusinessException(EmBusinessError.DATA_FORMAT_ERROR);
        }
        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userDO, userModel);
        return userModel;
    }

    private boolean validateTelphone(String telphone) {
        final int telphoneSize = 11;
        if (null == telphone || telphone.length() != telphoneSize) {
            return false;
        }
        try{
            Long.parseLong(telphone);
        }catch (Exception e) {
            return false;
        }
        return true;
    }

    private UserDO convertFromQueryObject(UserModel userModel, UserRequest request) {
        UserDO userDO = new UserDO();
        userDO.setId(userModel.getId());
        userDO.setName(request.getName());
        userDO.setAge(request.getAge());
        userDO.setGender(request.getGender());
        userDO.setChannelId(request.getChannelId());
        // TODO 暂时按频道选择图片
        if (request.getChannelId() != null) {
            userDO.setHeadImgUrl(getHeadImgUrl(request.getChannelId()));
        }
        return userDO;
    }

    private String getHeadImgUrl(Integer gender) {
        String boy = "http://ebook-image-1259465809.file.myqcloud.com/head_img/me_preference_boy@3x.png";
        String girl = "http://ebook-image-1259465809.file.myqcloud.com/head_img/me_preference_girl@3x.png";
        switch (gender) {
            case 2:
                return girl;
            case 1:
            default:
                return boy;
        }
    }
}
