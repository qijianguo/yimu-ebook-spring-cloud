package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.FeedbackDOMapper;
import com.yimu.ebook.domain.dataobject.FeedbackDO;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.CreateFeedbackRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.FeedbackService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Angus
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackDOMapper feedbackDOMapper;

    @Autowired
    public FeedbackServiceImpl(FeedbackDOMapper feedbackDOMapper) {
        this.feedbackDOMapper = feedbackDOMapper;
    }

    @Override
    public void save(UserModel userModel, CreateFeedbackRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        feedbackDOMapper.insertSelective(convertFromRequest(userModel, request));

    }

    private FeedbackDO convertFromRequest(UserModel userModel, CreateFeedbackRequest request) {
        FeedbackDO feedbackDO = new FeedbackDO();
        BeanUtils.copyProperties(request, feedbackDO);
        feedbackDO.setStaus(0);
        feedbackDO.setCreateTime(new Date());
        feedbackDO.setUserId(userModel.getId());
        return feedbackDO;
    }
}
