package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.ResourceConfigModel;

import java.util.List;

/**
 * 资源文件配置
 * @author Angus
 */
public interface ResourceConfigService {

    /**
     * 获取资源文件
     */
    List<ResourceConfigModel> getResourceConfig();
}
