package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.HotWordsModel;

import java.util.List;

/**
 * @author Angus
 */
public interface HotWordsService {

    List<HotWordsModel> getAll();
}
