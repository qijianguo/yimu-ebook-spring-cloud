package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InitCategoryResponse {

    private List<CategoryAndSubResponse> categories;

    private List<BookStatusResponse> bookStatuses;

    private List<KeywordResponse> sortKeywords;
}
