package com.yimu.ebook.domain.dataobject;

import java.util.ArrayList;
import java.util.List;

public class UserDOExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public UserDOExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(Byte value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(Byte value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(Byte value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(Byte value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(Byte value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(Byte value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<Byte> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<Byte> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(Byte value1, Byte value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(Byte value1, Byte value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andAgeIsNull() {
            addCriterion("age is null");
            return (Criteria) this;
        }

        public Criteria andAgeIsNotNull() {
            addCriterion("age is not null");
            return (Criteria) this;
        }

        public Criteria andAgeEqualTo(Integer value) {
            addCriterion("age =", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotEqualTo(Integer value) {
            addCriterion("age <>", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThan(Integer value) {
            addCriterion("age >", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeGreaterThanOrEqualTo(Integer value) {
            addCriterion("age >=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThan(Integer value) {
            addCriterion("age <", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeLessThanOrEqualTo(Integer value) {
            addCriterion("age <=", value, "age");
            return (Criteria) this;
        }

        public Criteria andAgeIn(List<Integer> values) {
            addCriterion("age in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotIn(List<Integer> values) {
            addCriterion("age not in", values, "age");
            return (Criteria) this;
        }

        public Criteria andAgeBetween(Integer value1, Integer value2) {
            addCriterion("age between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andAgeNotBetween(Integer value1, Integer value2) {
            addCriterion("age not between", value1, value2, "age");
            return (Criteria) this;
        }

        public Criteria andTelphoneIsNull() {
            addCriterion("telphone is null");
            return (Criteria) this;
        }

        public Criteria andTelphoneIsNotNull() {
            addCriterion("telphone is not null");
            return (Criteria) this;
        }

        public Criteria andTelphoneEqualTo(String value) {
            addCriterion("telphone =", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneNotEqualTo(String value) {
            addCriterion("telphone <>", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneGreaterThan(String value) {
            addCriterion("telphone >", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneGreaterThanOrEqualTo(String value) {
            addCriterion("telphone >=", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneLessThan(String value) {
            addCriterion("telphone <", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneLessThanOrEqualTo(String value) {
            addCriterion("telphone <=", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneLike(String value) {
            addCriterion("telphone like", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneNotLike(String value) {
            addCriterion("telphone not like", value, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneIn(List<String> values) {
            addCriterion("telphone in", values, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneNotIn(List<String> values) {
            addCriterion("telphone not in", values, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneBetween(String value1, String value2) {
            addCriterion("telphone between", value1, value2, "telphone");
            return (Criteria) this;
        }

        public Criteria andTelphoneNotBetween(String value1, String value2) {
            addCriterion("telphone not between", value1, value2, "telphone");
            return (Criteria) this;
        }

        public Criteria andAvatarIsNull() {
            addCriterion("headImgUrl is null");
            return (Criteria) this;
        }

        public Criteria andAvatarIsNotNull() {
            addCriterion("headImgUrl is not null");
            return (Criteria) this;
        }

        public Criteria andAvatarEqualTo(String value) {
            addCriterion("headImgUrl =", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarNotEqualTo(String value) {
            addCriterion("headImgUrl <>", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarGreaterThan(String value) {
            addCriterion("headImgUrl >", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarGreaterThanOrEqualTo(String value) {
            addCriterion("headImgUrl >=", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarLessThan(String value) {
            addCriterion("headImgUrl <", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarLessThanOrEqualTo(String value) {
            addCriterion("headImgUrl <=", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarLike(String value) {
            addCriterion("headImgUrl like", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarNotLike(String value) {
            addCriterion("headImgUrl not like", value, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarIn(List<String> values) {
            addCriterion("headImgUrl in", values, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarNotIn(List<String> values) {
            addCriterion("headImgUrl not in", values, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarBetween(String value1, String value2) {
            addCriterion("headImgUrl between", value1, value2, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andAvatarNotBetween(String value1, String value2) {
            addCriterion("headImgUrl not between", value1, value2, "headImgUrl");
            return (Criteria) this;
        }

        public Criteria andRegistermodeIsNull() {
            addCriterion("registerMode is null");
            return (Criteria) this;
        }

        public Criteria andRegistermodeIsNotNull() {
            addCriterion("registerMode is not null");
            return (Criteria) this;
        }

        public Criteria andRegistermodeEqualTo(String value) {
            addCriterion("registerMode =", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeNotEqualTo(String value) {
            addCriterion("registerMode <>", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeGreaterThan(String value) {
            addCriterion("registerMode >", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeGreaterThanOrEqualTo(String value) {
            addCriterion("registerMode >=", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeLessThan(String value) {
            addCriterion("registerMode <", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeLessThanOrEqualTo(String value) {
            addCriterion("registerMode <=", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeLike(String value) {
            addCriterion("registerMode like", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeNotLike(String value) {
            addCriterion("registerMode not like", value, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeIn(List<String> values) {
            addCriterion("registerMode in", values, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeNotIn(List<String> values) {
            addCriterion("registerMode not in", values, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeBetween(String value1, String value2) {
            addCriterion("registerMode between", value1, value2, "registermode");
            return (Criteria) this;
        }

        public Criteria andRegistermodeNotBetween(String value1, String value2) {
            addCriterion("registerMode not between", value1, value2, "registermode");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidIsNull() {
            addCriterion("thirdPartyId is null");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidIsNotNull() {
            addCriterion("thirdPartyId is not null");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidEqualTo(String value) {
            addCriterion("thirdPartyId =", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidNotEqualTo(String value) {
            addCriterion("thirdPartyId <>", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidGreaterThan(String value) {
            addCriterion("thirdPartyId >", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidGreaterThanOrEqualTo(String value) {
            addCriterion("thirdPartyId >=", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidLessThan(String value) {
            addCriterion("thirdPartyId <", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidLessThanOrEqualTo(String value) {
            addCriterion("thirdPartyId <=", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidLike(String value) {
            addCriterion("thirdPartyId like", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidNotLike(String value) {
            addCriterion("thirdPartyId not like", value, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidIn(List<String> values) {
            addCriterion("thirdPartyId in", values, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidNotIn(List<String> values) {
            addCriterion("thirdPartyId not in", values, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidBetween(String value1, String value2) {
            addCriterion("thirdPartyId between", value1, value2, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andThirdpartyidNotBetween(String value1, String value2) {
            addCriterion("thirdPartyId not between", value1, value2, "thirdpartyid");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordEqualTo(String value) {
            addCriterion("password =", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordGreaterThan(String value) {
            addCriterion("password >", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordLessThan(String value) {
            addCriterion("password <", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordLike(String value) {
            addCriterion("password like", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordNotLike(String value) {
            addCriterion("password not like", value, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordIn(List<String> values) {
            addCriterion("password in", values, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "encrptpassword");
            return (Criteria) this;
        }

        public Criteria andEncrptpasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "encrptpassword");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table t_user
     *
     * @mbg.generated do_not_delete_during_merge Wed May 22 11:28:08 CST 2019
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table t_user
     *
     * @mbg.generated Wed May 22 11:28:08 CST 2019
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}