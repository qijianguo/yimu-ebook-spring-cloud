package com.yimu.ebook.domain.vo;

import com.yimu.ebook.constant.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateVersionRequest {

  private String platform;

  private String versionCode;

  private String versionName;

  private String title;

  private String description;

  private String link;

  private Integer forceUpdate;

  private Integer inReview;

  public boolean validate() {
    return platform != null && Constants.APP_PLATFORMS.contains(platform);
  }
}
