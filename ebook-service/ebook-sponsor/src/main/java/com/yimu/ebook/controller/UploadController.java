package com.yimu.ebook.controller;

import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.util.CosUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传控制器
 * @author Angus
 */
@Controller
@RequestMapping(value = "/upload")
@CrossOrigin
@Api(description = "文件上传")
public class UploadController {

    @ApiOperation(value = "上传文件到腾讯云COS")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "type", value = "上传的类型：banner/category父分类的封面/category_icons书城分类按钮icon/cover书籍封面/head_img头像/apk软件安装包",
            required = true, dataType = "string", paramType = "query", defaultValue = "banner"),
    })
    @PostMapping
    @ResponseBody
    public Result Upload(@RequestParam(value = "file") MultipartFile file, @RequestParam("type") String type){
        if(file == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        String oldFileName = file.getOriginalFilename();
        String eName = oldFileName.substring(oldFileName.lastIndexOf("."));
        String newFileName = UUID.randomUUID()+eName;
        File localFile;
        String key = type + "/" + newFileName;
        try {
            localFile = File.createTempFile("temp",null);
            file.transferTo(localFile);
            CosUtils.simpleUploadFile(CosUtils.BUCKET_EBOOK_IMAGE, key, localFile);
            return Result.success("http://ebook-image-1259465809.file.myqcloud.com/" + key);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "上传失败");
        }
    }
}