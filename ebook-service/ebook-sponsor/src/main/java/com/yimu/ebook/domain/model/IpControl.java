package com.yimu.ebook.domain.model;

import lombok.Getter;
import org.apache.ibatis.jdbc.Null;

/**
 * @author Angus
 */

@Getter
public class IpControl {
  
  public static final Integer ALL = null;

  public static final Integer COPYRIGHT = 2;

  public static final Integer NO_COPYRIGHT = 0;

  public static final Integer IP_CONTROL = 1;

  /*ALL("", "全部可看"),
  ;

  private Integer id;

  private String name;
  IpControl(int id, String name) {
    this.id = id;
    this.name = name;
  }*/

}
