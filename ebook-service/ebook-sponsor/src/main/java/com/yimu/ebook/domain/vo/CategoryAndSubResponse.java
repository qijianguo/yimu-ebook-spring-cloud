package com.yimu.ebook.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class CategoryAndSubResponse {

    /**
     * 分类ID
     */
    private Integer id;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类默认图片
     */
    private String image;

    private List<CategoryResponse> sub;
}
