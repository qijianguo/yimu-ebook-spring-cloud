package com.yimu.ebook.domain.vo;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BannerResponse {

    private String image;

    private String link;

    private String description;

    private Date startTime;

    private Date endTime;

    private Date createTime;

    private Integer priority;

    private Integer channelId;

    private Integer active;

}
