package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.BookShelfDOMapper;
import com.yimu.ebook.dao.ReadHistoryDOMapper;
import com.yimu.ebook.domain.dataobject.BookChapterDO;
import com.yimu.ebook.domain.dataobject.BookDO;
import com.yimu.ebook.domain.dataobject.ReadHistoryDO;
import com.yimu.ebook.domain.model.ReadHistoryModel;
import com.yimu.ebook.domain.model.UserModel;
import com.yimu.ebook.domain.vo.AddBookShelfRequest;
import com.yimu.ebook.domain.vo.BookShelfRequest;
import com.yimu.ebook.domain.vo.CreateReadHistoryRequest;
import com.yimu.ebook.domain.vo.GetHistoryRequest;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.BookShelfService;
import com.yimu.ebook.service.ReadHistoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;

/**
 * @author Angus
 */
@Service
public class ReadHistoryServiceImpl implements ReadHistoryService {

    private final ReadHistoryDOMapper readHistoryDOMapper;

    @Autowired
    private BookShelfService bookShelfService;

    @Autowired
    public ReadHistoryServiceImpl(ReadHistoryDOMapper readHistoryDOMapper) {
        this.readHistoryDOMapper = readHistoryDOMapper;
    }

    @Override
    @Transactional(rollbackOn = BusinessException.class)
    public void saveOrUpdate(UserModel userModel, CreateReadHistoryRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<ReadHistoryDO> list = convertFromDataRequest(userModel, request);
        // 修改书架的时间
        AddBookShelfRequest addBookShelfRequest = new AddBookShelfRequest();
        List<BookShelfRequest> shelves = new ArrayList<>();
        list.forEach(readHistoryDO -> {
            readHistoryDOMapper.saveOrUpdate(readHistoryDO);
            // 检查书架中是否有这本书
            int shelfCount = bookShelfService
                .selectCountByUserIdAndBookId(userModel.getId(), readHistoryDO.getBookId());
            if (shelfCount > 0) {
                shelves.add(new BookShelfRequest(readHistoryDO.getBookId(),
                    readHistoryDO.getTime().getTime()));
                addBookShelfRequest.setShelves(shelves);
            }
        });
        if (CollectionUtils.isEmpty(addBookShelfRequest.getShelves())) {
            return;
        }
        bookShelfService.addBookShelf(userModel, addBookShelfRequest);
    }

    @Override
    public List<ReadHistoryModel> getByPage(UserModel userModel, GetHistoryRequest request) {
        request.defPage(20);
        List<ReadHistoryDO> list = Optional.ofNullable(readHistoryDOMapper.selectByUserIdIs(userModel.getId(), request.getPageNum(), request.getSize())).orElse(Collections.emptyList());
        return convertFromDataObjectList(list);
    }

    @Override
    public long getCount(UserModel userModel, GetHistoryRequest request) {
        return readHistoryDOMapper.selectCountByUserIdIs(userModel.getId());
    }

    @Override
    public ReadHistoryModel selectByUserIdsAndBookId(Long userId, Long bookId) {
        ReadHistoryDO readHistoryDO = readHistoryDOMapper.selectByUserIdsAndBookIdOrderByTimeDescLimit(userId, bookId);
        return convertFromDataObject(readHistoryDO);
    }

    private List<ReadHistoryDO> convertFromDataRequest(UserModel userModel, CreateReadHistoryRequest request) {
        if (CollectionUtils.isEmpty(request.getReadHistoryList())) {
            return Collections.EMPTY_LIST;
        }
        List<ReadHistoryDO> readList = new ArrayList<>();
        request.getReadHistoryList().forEach(readHistory -> {
            ReadHistoryDO readHistoryDO = new ReadHistoryDO();
            readHistoryDO.setBookId(readHistory.getBookId());
            readHistoryDO.setChapterId(readHistory.getChapterId());
            readHistoryDO.setUserId(userModel.getId());
            readHistoryDO.setTime(new Date(readHistory.getTime()));
            readList.add(readHistoryDO);
        });
        return readList;
    }

    private List<ReadHistoryModel> convertFromDataObjectList(List<ReadHistoryDO> list) {
        List<ReadHistoryModel> models = new ArrayList<>();
        list.forEach(dataObject -> models.add(convertFromDataObject(dataObject)));
        return models;
    }

    private ReadHistoryModel convertFromDataObject(ReadHistoryDO dataObject) {
        if (dataObject == null) {
            return null;
        }
        ReadHistoryModel model = new ReadHistoryModel();
        BeanUtils.copyProperties(dataObject, model);
        model.setTime(dataObject.getTime());
        model.setChapterId(dataObject.getChapterId());
        BookDO bookDO = dataObject.getBookDO();
        if (bookDO != null) {
            model.setBookName(bookDO.getName());
            model.setBookCover(bookDO.getCover());
            model.setBookSummary(bookDO.getSummary());
        }
        BookChapterDO chapterDO = dataObject.getChapterDO();
        if (chapterDO != null) {
            model.setChapterTitle(chapterDO.getTitle());
            model.setChapterContent(chapterDO.getContentUrl());
        }
        return model;
    }
}
