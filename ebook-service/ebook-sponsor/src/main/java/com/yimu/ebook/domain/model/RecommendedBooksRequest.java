package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecommendedBooksRequest {

  private Long bookId;

  private Integer pageNum;

  private Integer size;

  public boolean validate() {
    if (bookId == null || bookId == 0) {
      return false;
    }
    if (pageNum == null || pageNum == 0) {
      pageNum = 1;
    }
    // 特殊处理：给客户端返回的个数是size - 1个
    if (size == null || size == 0) {
      size = 4;
    }
    return true;
  }

}
