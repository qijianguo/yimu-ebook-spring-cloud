package com.yimu.ebook.controller;

import com.yimu.ebook.domain.model.BannerModel;
import com.yimu.ebook.domain.vo.BannerRequest;
import com.yimu.ebook.domain.vo.BannerResponse;
import com.yimu.ebook.domain.vo.CreateBannerRequest;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Angus
 */
@RestController
@RequestMapping(value = "banner", produces = MediaType.TEXT_HTML_VALUE)
@CrossOrigin
@Api(description= "Banner管理")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @ApiOperation(value = "添加banner")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", required = true, dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "active", value = "是否启用 0否 1是",required = true,  dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "priority", value = "优先级， 1 - 10",required = true,  dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "description", value = "描述", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "image", value = "图片地址", required = true, dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "link", value = "跳转地址", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "startTime", value = "开始时间", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "endTime", value = "结束时间", required = true, dataType = "string", paramType = "query"),
        @ApiImplicitParam(name = "forever", value = "是否长期有效", required = true, dataType = "string", paramType = "query"),

    })
    @PostMapping
    public Result createBanner(CreateBannerRequest request) {
        bannerService.save(request);
        return Result.success("添加成功！");
    }

    @ApiOperation(value = "查询banner")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "channelId", value = "频道ID：男频1，女频2", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "active", value = "是否启用 0否 1是", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "excludeExpired", value = "是否包含过期的 0不包含 1包含", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "sortKeyword", value = "排序关键字：create_time/priority", dataType = "string", paramType = "query", defaultValue = "create_time"),
        @ApiImplicitParam(name = "sortBy", value = "排序方式：升序asc/降序desc", dataType = "string", paramType = "query", defaultValue = "asc"),
        @ApiImplicitParam(name = "pageNum", value = "页码，默认为1", dataType = "string", paramType = "query", defaultValue = "1"),
        @ApiImplicitParam(name = "size", value = "每页数，默认是10", dataType = "string", paramType = "query", defaultValue = "5"),
    })
    @GetMapping
    public Result findAll(BannerRequest request) {
        List<BannerModel> bannerByCondition = bannerService.getBannerByCondition(request);
        return Result.success(convertFromModelList(bannerByCondition));
    }

    private List<BannerResponse> convertFromModelList(List<BannerModel> models) {
        if (CollectionUtils.isEmpty(models)) {
            return Collections.EMPTY_LIST;
        }
        List<BannerResponse> responses = new ArrayList<>();
        models.forEach(model -> {
            BannerResponse response = new BannerResponse();
            BeanUtils.copyProperties(model, response);
            responses.add(response);
        });
        return responses;
    }


}
