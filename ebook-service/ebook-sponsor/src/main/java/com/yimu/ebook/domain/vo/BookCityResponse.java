package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookCityResponse<T> {

    private String type;

    private String name;

    private String viewType;

    private String moreLink;

    private String rollLink;

    private List<T> data;

}
