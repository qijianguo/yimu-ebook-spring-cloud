package com.yimu.ebook.interceptor;

import com.yimu.ebook.constant.Constants;
import com.yimu.ebook.domain.model.AppVersionModel;
import com.yimu.ebook.domain.model.IpControl;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.service.AppVersionService;
import com.yimu.ebook.util.CusAccessObjectUtil;
import com.yimu.ebook.util.HttpUtils;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * @author Angus
 */
@Component
@Slf4j
public class AppVersionCheckInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AppVersionService appVersionService;

    private static final List<String> CITY_LIST = Arrays.asList("上海市", "北京市", "广东省", "深圳市");

    /**
     * ip是否被控制：true控制 false不控制
     */
    @SuppressWarnings("all")
    public static ThreadLocal<Integer> ipControl = new ThreadLocal<>();

    public static ThreadLocal<Boolean> IpC = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) {

        String versionCode = request.getHeader(Constants.VERSION_CODE);
        String versionName = request.getHeader(Constants.VERSION_NAME);
        String platform = request.getHeader(Constants.PLATFORM);
        // TODO 渠道统计
        String hannels = request.getHeader(Constants.HANNELS);

        boolean ignoreMethod = false;
        if (handler != null && handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            if (method.getName().contains("getVersion")) {
                ignoreMethod = true;
            }
        }

        if (platform == null || ignoreMethod) {
            // throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "参数缺少平台和版本号");
            // 都不传的话不检查版本问题
            return true;
        }
        AppVersionModel byPlatform = appVersionService.getByPlatform(platform);
        if (byPlatform == null) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "请检查平台名称是否正确");
        }
        // 控制书籍的显示
        // ipControl.set(checkIpArea(request));
        // h5显示正版授权的，移动端显示
        /*if (platform != null && !"".equals(platform)) {
            switch (platform) {
                case "h5":
                    ipControl.set(1);
                    break;
                case "ios":
                case "android":
                case "flutter":
                    ipControl.set(0);
                    break;
                default:
            }
        } else {
        }*/
        if (Objects.equals("yzCloud", platform)) {
            ipControl.set(IpControl.COPYRIGHT);
        } else {
            ipControl.set(IpControl.ALL);
            if (checkIpArea(request)) {
                ipControl.set(IpControl.IP_CONTROL);
            } else {
                ipControl.set(IpControl.ALL);
            }
        }
        // 审核中的平台只展示有版权的书籍
        if (byPlatform.getInReview() != null && byPlatform.getInReview() == 1) {
            ipControl.set(IpControl.COPYRIGHT);
        }

        if (byPlatform.getForceUpdate() == 1 && Integer.parseInt(versionCode) < Integer.parseInt(byPlatform.getVersionCode())) {
            throw new BusinessException(EmBusinessError.VERSION_EXPIRED);
        }
        return true;
    }

    private boolean checkIpArea(HttpServletRequest request) {
        // 获取IP，查询是否是属于 北上广深的（http://whois.pconline.com.cn/jsAlert.jsp?ip=59.64.16.100）
        String ip = CusAccessObjectUtil.getIpAddress(request);
        try {
            String response = HttpUtils.getInstance().executeGet("http://whois.pconline.com.cn/jsAlert.jsp?ip=" + ip);
            boolean bool = false;
            for (String city : CITY_LIST) {
                if (response.contains(city)) {
                    bool = true;
                }
                break;
            }
            log.info("访问的ip信息是：{}, 是否限制：{}", ip + response, bool);
            return bool;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            return true;
        }
    }
}
