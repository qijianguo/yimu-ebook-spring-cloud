package com.yimu.ebook.domain.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdHannelsModel {

  private Integer id;

  private String sign;

  private String link;

  private Date updateTime;

  private Date createTime;

  private Integer status;

  private String lpicRtext;

  private String suspension;

  private String threePic;
}
