package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProblemAnswerModel {

    private Integer id;

    private String problem;

    private String answer;

}
