package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T> {

    private int pageNum;

    private int size;

    private long totalSize;

    private long totalPage;

    private T data;

    public Page(int pageNum, int size, long totalSize, T data) {
        this.pageNum = pageNum;
        this.size = size;
        this.totalSize = totalSize;
        this.data = data;
        if (totalSize % size > 0) { // 100 / 15  6
            this.totalPage = totalSize / size + 1;
        } else {
            this.totalPage = totalSize / size;
        }
    }
}
