package com.yimu.ebook.domain.vo;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryBookUpdateResponse {

  private Long bookId;

  private Date updateTime;

}
