package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.BookChapterDOMapper;
import com.yimu.ebook.dao.BookDOMapper;
import com.yimu.ebook.dao.EditorRecommendDOMapper;
import com.yimu.ebook.dao.HotSearchDOMapper;
import com.yimu.ebook.dao.MonthHotRankDOMapper;
import com.yimu.ebook.dao.NewbookHotRankDOMapper;
import com.yimu.ebook.dao.TotalRankDOMapper;
import com.yimu.ebook.dao.WeekHotRankDOMapper;
import com.yimu.ebook.domain.dataobject.BookChapterDO;
import com.yimu.ebook.domain.dataobject.BookDO;
import com.yimu.ebook.domain.dataobject.BookSizeByCategoryDo;
import com.yimu.ebook.domain.dataobject.QueryBookCondition;
import com.yimu.ebook.domain.model.BookAudit;
import com.yimu.ebook.domain.model.BookChapterModel;
import com.yimu.ebook.domain.model.BookModel;
import com.yimu.ebook.domain.model.RecommendedBooksRequest;
import com.yimu.ebook.domain.vo.*;
import com.yimu.ebook.exception.BusinessException;
import com.yimu.ebook.exception.EmBusinessError;
import com.yimu.ebook.interceptor.AppVersionCheckInterceptor;
import com.yimu.ebook.service.BookService;
import com.yimu.ebook.util.AesUtils;
import com.yimu.ebook.util.CosUtils;
import com.yimu.ebook.util.RedisKeyUtils;
import java.io.IOException;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import org.springframework.util.StringUtils;

/**
 * @author Angus
 */
@Service
@Slf4j
public class BookServiceImpl implements BookService {

    private final BookDOMapper bookDOMapper;
    private final BookChapterDOMapper bookChapterDOMapper;
    private final SolrClient solrClient;
    private final RedisTemplate redisTemplate;
    private final MonthHotRankDOMapper monthHotRankDOMapper;
    private final WeekHotRankDOMapper weekHotRankDOMapper;
    private final TotalRankDOMapper totalRankDOMapper;
    private final HotSearchDOMapper hotSearchDOMapper;
    private final EditorRecommendDOMapper editorRecommendDOMapper;
    private final NewbookHotRankDOMapper newbookHotRankDOMapper;
    
    public BookServiceImpl(BookDOMapper bookDOMapper, BookChapterDOMapper bookChapterDOMapper,
        SolrClient solrClient, RedisTemplate redisTemplate,
        MonthHotRankDOMapper monthHotRankDOMapper, WeekHotRankDOMapper weekHotRankDOMapper,
        TotalRankDOMapper totalRankDOMapper, HotSearchDOMapper hotSearchDOMapper,
        EditorRecommendDOMapper editorRecommendDOMapper,
        NewbookHotRankDOMapper newbookHotRankDOMapper) {
        this.bookDOMapper = bookDOMapper;
        this.bookChapterDOMapper = bookChapterDOMapper;
        this.solrClient = solrClient;
        this.redisTemplate = redisTemplate;
        this.monthHotRankDOMapper = monthHotRankDOMapper;
        this.weekHotRankDOMapper = weekHotRankDOMapper;
        this.totalRankDOMapper = totalRankDOMapper;
        this.hotSearchDOMapper = hotSearchDOMapper;
        this.editorRecommendDOMapper = editorRecommendDOMapper;
        this.newbookHotRankDOMapper = newbookHotRankDOMapper;
    }

    @Override
    public void update(CreateBookRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        BookDO bookDO = convertFromCreateBookRequest(request);
        BookDO old = bookDOMapper.selectByPrimaryKey(bookDO.getId());
        if (old != null) {
            int state = bookDOMapper.updateByPrimaryKeySelective(bookDO);
            if(state <= 0) {
                throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "保存书籍失败");
            }
        } else {
//            bookDO.setActive(BookAudit.NO_AUDIT.getId());
//            bookDO.setCreateTime(new Date());
//            bookDO.setUpdateTime(bookDO.getCreateTime());
//            state = bookDOMapper.insert(bookDO);
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "要修改的书籍不存在，请检查BookId");
        }
        // 修改solr中的内容
        BookDO ne = bookDOMapper.selectByPrimaryKey(bookDO.getId());
        try {
            solrClient.add(convertDocFromDataObject(ne));
            solrClient.commit();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
        // 如果active ！= 1则删除热度排名中的数据
        if (!ne.getActive().equals(BookAudit.SHELVE.getId())) {
            monthHotRankDOMapper.deleteByBookId(ne.getId());
            weekHotRankDOMapper.deleteByBookId(ne.getId());
            newbookHotRankDOMapper.deleteByBookId(ne.getId());
            totalRankDOMapper.deleteByBookId(ne.getId());
            editorRecommendDOMapper.deleteByBookId(ne.getId());
            hotSearchDOMapper.deleteByBookId(ne.getId());
        }
    }

    @Override
    public void update(CreateBookChapterRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }

        List<BookDO> list = bookDOMapper.selectByUuid(request.getBookId());
        if (CollectionUtils.isEmpty(list)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "书籍不存在");
        }
        BookDO bookDO = list.get(0);
        List<BookChapterDO> chapterDOList = bookChapterDOMapper.selectByBookIdAndChapterId(bookDO.getId(), request.getChapterId());
        if (!CollectionUtils.isEmpty(chapterDOList)) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR, "章节已存在");
        }
        BookChapterDO chapterDO = convertFromCreateChapterRequest(request);
        chapterDO.setBookId(bookDO.getId());
        int state = bookChapterDOMapper.insertSelective(chapterDO);
        if (state <= 0) {
            throw new BusinessException(EmBusinessError.UNKNOW_ERROR, "保存章节失败");
        }
    }

    @Override
    public List<BookModel> getRecommendedBooks(RecommendedBooksRequest request)
        throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        // 根据bookId查询书籍详情
        BookDO bookDO = bookDOMapper.selectByPrimaryKey(request.getBookId());
        if(bookDO == null) {
            throw new BusinessException(EmBusinessError.NOT_FOUND_RESOURCE, "书籍不存在");
        }
        // 查询size + 1个，查询出来后如果包含当前的bookId，则排除，不包含则去掉第一个
        request.setSize(request.getSize() + 1);

        QueryBookCondition condition = new QueryBookCondition();
        BeanUtils.copyProperties(bookDO, condition);
        condition.setPageNum(request.getPageNum());
        condition.setSize(request.getSize());
        // 去掉几个条件，不然书籍太少
        condition.setSubCategoryId(null);
        condition.setBookStatus(null);
        if (AppVersionCheckInterceptor.ipControl != null){
            condition.setIpControl(AppVersionCheckInterceptor.ipControl.get());
        }
        long count = bookDOMapper.selectCountByCondition(condition);
        // 每页size个， 则有 count / size + 1页，但是最后一页可能不全，则去掉，即（count /size）页
        // pageNum = currentTimeMillis / (count / size)
        int totalPage = (int)(count / request.getSize()) + 1;
        int pageNum = (int)(System.currentTimeMillis() % totalPage) + 1;
        request.setPageNum(pageNum);
        List<BookDO> hot = Optional.ofNullable(bookDOMapper
            .selectByCondition(condition)).orElse(Collections.emptyList());
        // 过滤掉当前书籍
        hot = hot.stream().filter(b1 ->
            !Objects.equals(b1.getId(), request.getBookId())
        ).collect(Collectors.toList());
        // 保证给前端的书籍是查询条件中 （size -1）个
        if (hot.size() == request.getSize()) {
            hot.remove(0);
        }
        return convertFromListDataObject(hot);
    }

  @Override
  public List<BookSizeByCategoryDo> getBookSizeByCategory() {
    return bookDOMapper.selectBookSizeByCategory();
  }

    @Override
    public List<BookChapterDO> getByRegexp(String regexp) {
        return Optional.ofNullable(bookChapterDOMapper.selectByRegexp(regexp)).orElse(Collections.emptyList());
    }

    @Override
    public void updateChapterByRegexp(String regexp) {
        List<BookChapterDO> bookChapterDOS = getByRegexp(regexp);
        bookChapterDOS.forEach(bookChapterDO -> {
            String title = bookChapterDO.getTitle();
            // 替换  '》第' 之前的字符为空
            if (title != null && title.contains("第")) {
                int index = title.indexOf("第");
                title = title.substring(index);
                bookChapterDO.setTitle(title);
            }
            // 替换  '》'及其之前的字符为空
            if (title != null && title.contains("》")) {
                int index = title.indexOf("》");
                title = title.substring(index + 1);
                bookChapterDO.setTitle(title);
            }
            int state = bookChapterDOMapper.updateByPrimaryKey(bookChapterDO);
            System.out.println(state);
        });
    }

    @Override
    public List<QueryBookUpdateResponse> checkBookUpdate(QueryBookUpdateRequest request) {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<QueryBookUpdateResponse> responses = new ArrayList<>();
        request.getBookIds().forEach(bookId -> {
            Object obj = redisTemplate.opsForValue().get(RedisKeyUtils.checkBookUpdate(bookId));
            if (obj instanceof Date) {
                QueryBookUpdateResponse response = new QueryBookUpdateResponse();
                Date updateTime = (Date) obj;
                response.setBookId(bookId);
                response.setUpdateTime(updateTime);
                responses.add(response);
            }
        });
        return responses;
    }

    @Override
    public void updateChapterContent(UpdateChapterContentRequest request) throws Exception {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        String content = AesUtils.encrypt(request.getContent(), AesUtils.AES_KEY);
        String key = request.getContentUrl().replace("http://ebook2-1259465809.file.myqcloud.com/", "").replace("http://ebook-1259465809.file.myqcloud.com/", "");
        CosUtils.upload(CosUtils.BUCKET_EBOOK2, key, content);
    }

    @Override
    public String getChapterContent(String contentUrl) {
        if (StringUtils.isEmpty(contentUrl)) {
            return null;
        }
        if (contentUrl.contains("http://ebook-1259465809.file.myqcloud.com/")) {
            contentUrl = contentUrl.replace("http://ebook-1259465809.file.myqcloud.com/", "");
        }
        if (contentUrl.contains("http://ebook2-1259465809.file.myqcloud.com/")) {
            contentUrl = contentUrl.replace("http://ebook2-1259465809.file.myqcloud.com/", "");
        }
        String downloadContent = CosUtils.downloadContent(CosUtils.BUCKET_EBOOK2, contentUrl);
        return downloadContent;
    }


    @Override
    public List<BookModel> getBookListByCondition(BookRequest bookRequest) {
        // 初始化参数
        bookRequest.init();
        QueryBookCondition condition = new QueryBookCondition();
        BeanUtils.copyProperties(bookRequest, condition);

        List<BookDO> bookDOS = bookDOMapper.selectByCondition(condition);
        return convertFromListDataObject(bookDOS);
    }

    @Override
    public BookModel getBookDetailByUuid(String uuid) throws BusinessException {
        if (uuid == null) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<BookDO> list = bookDOMapper.selectByUuid(uuid);
        if (CollectionUtils.isEmpty(list)) {
            throw new BusinessException(EmBusinessError.NOT_FOUND_RESOURCE, "无法找到匹配书籍");
        }
        return convertFromDataObject(list.get(0));
    }

    @Override
    public BookModel getBookDetailById(Long id) throws BusinessException {
        if (id == null || id == 0) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        BookDO bookDO = bookDOMapper.selectByPrimaryKey(id);
        if (bookDO == null) {
            throw new BusinessException(EmBusinessError.NOT_FOUND_RESOURCE, "无法找到匹配书籍");
        }
        return convertFromDataObject(bookDO);
    }

    @Override
    public int getCountChapterByBookId(Long bookId) {
        return bookChapterDOMapper.selectCountByBookId(bookId);
    }

    @Override
    public List<BookChapterModel> getBookChapter(BookChapterRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        List<BookChapterDO> list = bookChapterDOMapper.selectByBookId(request.getBookId(), request.getPageNum(), request.getSize());
        return convertFromChapterListDataObject(list);
    }

    @Override
    public void setSolr() {
        QueryBookCondition condition = new QueryBookCondition();
        condition.setPageNum(1);
        condition.setSize(Integer.MAX_VALUE);
        List<BookDO> bookDOS = bookDOMapper.selectByCondition(condition);
        setSolr(bookDOS);
    }

    @Override
    public List<SearchResponse> searchFromSolr(SearchRequest request) throws BusinessException {
        if (!request.validate()) {
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }

        SolrQuery solrQuery = new SolrQuery();

        String key = String.format(" name:%s OR author:%s", request.getKeyword(), request.getKeyword());
        solrQuery.set("q", key);

        // 如果是北上广深的则过滤
        if (AppVersionCheckInterceptor.ipControl != null && AppVersionCheckInterceptor.ipControl.get() != null) {
            solrQuery.addFilterQuery(String.format("ipControl:%d", AppVersionCheckInterceptor.ipControl.get()));
        }
        // 只展示active=1的
        solrQuery.addFilterQuery("active:" + BookAudit.SHELVE.getId());

        solrQuery.setStart((request.getPageNum() - 1) * request.getSize());
        solrQuery.setRows(request.getSize());
        solrQuery.setHighlight(true);

        solrQuery.addHighlightField("name");
        solrQuery.addHighlightField("author");
        // 设置高亮字段的前缀
        solrQuery.setHighlightSimplePre("<font color='#3399FF'>");
        // 设置高亮字段的后缀
        solrQuery.setHighlightSimplePost("</font>");

        // 执行查询
        QueryResponse response = null;
        try {
            response = solrClient.query(solrQuery);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        if (response == null) {
            return Collections.emptyList();
        }

        // 文档结果集
        SolrDocumentList docs = response.getResults();

        List<SearchResponse> bookResponses = new ArrayList<>();

        // 高亮显示的返回结果
        Map<String, Map<String, List<String>>> maplist = response.getHighlighting();

        for (SolrDocument solrDocument : docs) {
            Long id = Long.parseLong(solrDocument.getFirstValue("id").toString());
            StringBuilder name = new StringBuilder();
            StringBuilder author = new StringBuilder();
            String cover = solrDocument.getFirstValue("cover").toString();
            String summary = solrDocument.getFirstValue("summary").toString();


            Map<String, List<String>> fieldMap = maplist.get(solrDocument.get("id"));
            List<String> stringlist = fieldMap.get("name");
            if (1 == request.getHighlight() && !CollectionUtils.isEmpty(stringlist)) {
                stringlist.forEach(name::append);
            } else {
                name.append(solrDocument.getFirstValue("name").toString());
            }
            List<String> authorlist = fieldMap.get("author");
            if (1 == request.getHighlight() && !CollectionUtils.isEmpty(authorlist)) {
                authorlist.forEach(author::append);
            } else {
                author.append(solrDocument.getFirstValue("author").toString());
            }

            SearchResponse searchResponse = new SearchResponse(id, name.toString(), author.toString(), cover, summary);
            bookResponses.add(searchResponse);
        }
        return bookResponses;
    }

    private List<BookModel> convertFromListDataObject(List<BookDO> list) {
        if (list == null) {
            return Collections.emptyList();
        }
        List<BookModel> result = new ArrayList<>();
        list.forEach(bookDO -> result.add(convertFromDataObject(bookDO)));
        return result;
    }

    private BookModel convertFromDataObject(BookDO bookDO){
        BookModel bookModel = new BookModel();
        BeanUtils.copyProperties(bookDO, bookModel);
        bookModel.setWordSize(bookDO.getWordSize().doubleValue());
        if (bookDO.getCategoryDO() != null) {
            bookModel.setCategoryId(bookDO.getCategoryId());
            bookModel.setCategoryName(bookDO.getCategoryDO().getName());
        }
        if (bookDO.getCategorySubDO() != null) {
            bookModel.setSubCategoryId(bookDO.getSubCategoryId());
            bookModel.setSubCategoryName(bookDO.getCategorySubDO().getName());
        }
        return bookModel;
    }

    private List<BookChapterModel> convertFromChapterListDataObject(List<BookChapterDO> list) {
        if (list == null) {
            return Collections.emptyList();
        }
        List<BookChapterModel> result = new ArrayList<>();
        list.forEach(data -> {
            BookChapterModel model = new BookChapterModel();
            BeanUtils.copyProperties(data, model);
            String url = data.getContentUrl().replace("http://ebook-1259465809.file.myqcloud.com", "http://ebook2-1259465809.file.myqcloud.com");
            model.setContentUrl(url);
            result.add(model);
        });
        return result;
    }

    private BookDO convertFromCreateBookRequest(CreateBookRequest request) {
        BookDO bookDO = new BookDO();
        BeanUtils.copyProperties(request, bookDO);
        bookDO.setId(request.getBookId());
        if (request.getScore() != null) {
            bookDO.setScore(new BigDecimal(request.getScore()));
        }
        if (request.getWordSize() != null) {
            bookDO.setWordSize(new BigDecimal(request.getWordSize()));
        }
        return bookDO;
    }

    private BookChapterDO convertFromCreateChapterRequest(CreateBookChapterRequest request) {

        BookChapterDO chapterDO = new BookChapterDO();
        BeanUtils.copyProperties(request, chapterDO);
        chapterDO.setCreateTime(new Date());
        return chapterDO;
    }

    private SolrInputDocument convertDocFromDataObject(BookDO bookDO) {
        if (bookDO == null) {
            return null;
        }
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", bookDO.getId());
        doc.setField("name", bookDO.getName());
        doc.setField("author", bookDO.getAuthor());
        doc.setField("cover", bookDO.getCover());
        doc.setField("summary", bookDO.getSummary());
        doc.setField("active", bookDO.getActive());
        doc.setField("ipControl", bookDO.getIpControl());
        return doc;
    }

    private void setSolr(List<BookDO> list) {
//        List<SolrBook> solrBooks = new ArrayList<>();
//        Optional.ofNullable(list).orElse(Collections.emptyList()).forEach(data -> {
//            SolrBook model = new SolrBook();
//            BeanUtils.copyProperties(data, model);
//            solrBooks.add(model);
//        });
//        //基于实体类创建索引
//        try {
//            client.addBeans(solrBooks);
//            client.commit();
//        } catch (SolrServerException | IOException e) {
//            e.printStackTrace();
//        }
    }
}
