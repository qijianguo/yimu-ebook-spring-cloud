package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.AppVersionModel;
import com.yimu.ebook.domain.vo.UpdateVersionRequest;
import com.yimu.ebook.exception.BusinessException;

/**
 * @author Angus
 */
public interface AppVersionService {

    /**
     * 根据平台查询版本
     * @param platform 平台：ios/android/h5/flutter
     * @return AppVersionModel
     * @throws BusinessException
     */
    AppVersionModel getByPlatform(String platform) throws BusinessException;

    /**
     * 缓存App版本信息
     */
    void cacheAppVersion();

  /**
   * 更新版本
   * @param request
   * @return
   * @throws BusinessException
   */
  void updateVersion(UpdateVersionRequest request) throws BusinessException;
}
