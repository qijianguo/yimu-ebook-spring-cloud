package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminLoginRequest {

  private String username;

  private String password;

  public boolean validate() {
    return username != null && password != null;
  }

}
