package com.yimu.ebook.domain.dataobject;

import java.util.Date;

public class HotWordsDO {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.id
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.word
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private String word;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.link
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private String link;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.image
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private String image;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.channel_id
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private Integer channelId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.description
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private String description;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.create_time
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hot_words.active
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    private Integer active;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.id
     *
     * @return the value of t_hot_words.id
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.id
     *
     * @param id the value for t_hot_words.id
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.word
     *
     * @return the value of t_hot_words.word
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public String getWord() {
        return word;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.word
     *
     * @param word the value for t_hot_words.word
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setWord(String word) {
        this.word = word == null ? null : word.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.link
     *
     * @return the value of t_hot_words.link
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public String getLink() {
        return link;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.link
     *
     * @param link the value for t_hot_words.link
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setLink(String link) {
        this.link = link == null ? null : link.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.image
     *
     * @return the value of t_hot_words.image
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public String getImage() {
        return image;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.image
     *
     * @param image the value for t_hot_words.image
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.channel_id
     *
     * @return the value of t_hot_words.channel_id
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.channel_id
     *
     * @param channelId the value for t_hot_words.channel_id
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.description
     *
     * @return the value of t_hot_words.description
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public String getDescription() {
        return description;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.description
     *
     * @param description the value for t_hot_words.description
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.create_time
     *
     * @return the value of t_hot_words.create_time
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.create_time
     *
     * @param createTime the value for t_hot_words.create_time
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hot_words.active
     *
     * @return the value of t_hot_words.active
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public Integer getActive() {
        return active;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hot_words.active
     *
     * @param active the value for t_hot_words.active
     *
     * @mbg.generated Thu Jun 13 18:45:31 CST 2019
     */
    public void setActive(Integer active) {
        this.active = active;
    }
}