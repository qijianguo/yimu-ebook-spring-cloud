package com.yimu.ebook.service.impl;

import com.yimu.ebook.dao.CategorySubDOMapper;
import com.yimu.ebook.domain.dataobject.CategorySubDO;
import com.yimu.ebook.domain.model.SubCategoryModel;
import com.yimu.ebook.service.CategorySubService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author Angus
 */
@Service
public class CategorySubServiceImpl implements CategorySubService {

    @Autowired
    private CategorySubDOMapper categorySubDOMapper;

    @Override
    public List<SubCategoryModel> getByCategoryId(Integer categoryId) {
        List<CategorySubDO> categorySubDOList = Optional.ofNullable(categorySubDOMapper.selectByCategoryId(categoryId)).orElse(Collections.emptyList());
        return convertFromDataObjectList(categorySubDOList);
    }

    @Override
    public SubCategoryModel getSubCategory(Integer subCategoryId) {
        CategorySubDO categorySubDO = categorySubDOMapper.selectSubCategory(subCategoryId);
        return convertFromDataObject(categorySubDO);
    }

    public List<SubCategoryModel> convertFromDataObjectList(List<CategorySubDO> dataObjectList) {
        List<SubCategoryModel> models = new ArrayList<>();
        dataObjectList.forEach(dataObject -> models.add(convertFromDataObject(dataObject)));
        return models;
    }

    private SubCategoryModel convertFromDataObject(CategorySubDO dataObject) {
        SubCategoryModel model = new SubCategoryModel();
        BeanUtils.copyProperties(dataObject, model);
        return model;
    }


}
