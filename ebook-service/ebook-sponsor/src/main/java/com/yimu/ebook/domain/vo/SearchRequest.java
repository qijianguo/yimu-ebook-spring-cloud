package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchRequest {

    private String keyword;

    private Integer pageNum;

    private Integer size;

    /**
     * 是否高亮：1高亮，0不高亮
     */
    private Integer highlight;

    public boolean validate() {
        if (keyword != null) {
            if (pageNum == null || pageNum == 0) {
                pageNum = 1;
            }
            if (size == null || size == 0) {
                size = 20;
            }
            if (highlight == null) {
                highlight = 0;
            }
            return true;
        }
        return false;
    }
}
