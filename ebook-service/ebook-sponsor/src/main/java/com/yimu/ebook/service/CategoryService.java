package com.yimu.ebook.service;

import com.yimu.ebook.domain.model.CategoryModel;
import com.yimu.ebook.exception.BusinessException;

import java.util.List;

/**
 * @author Angus
 */
public interface CategoryService {

    /**
     * 查询所有分类
     */
    List<CategoryModel> getAll();

    /**
     * 查询所有分类
     */
    List<CategoryModel> selectAllActive();

    /**
     * 查询所有分类及其子类
     */
    List<CategoryModel> getAllAndSub();

    /**
     * 按频道查询分类
     * @param channelId 频道Id
     */
    List<CategoryModel> getByChannel(Integer channelId) throws BusinessException;

    /**
     * 根据分类ID查询子分类
     */
    CategoryModel getByParentId(Integer parentId);

    void updateCategory(List<CategoryModel> list);


}
