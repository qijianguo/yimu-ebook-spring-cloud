package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HotWordsModel {

    private Integer id;

    private String word;

    private String link;

    private String image;

    private String description;

}
