package com.yimu.ebook.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateReadHistoryRequest {

    private List<ReadHistoryRequest> readHistoryList;

    public boolean validate() {
        return readHistoryList != null && readHistoryList.size() > 0;
    }

}
