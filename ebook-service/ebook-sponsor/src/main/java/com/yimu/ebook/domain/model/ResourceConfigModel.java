package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceConfigModel {


    private String imgUrlClick;

    private String imgUrlUnClick;

    private String name;

    /**
     * 目标地址
     */
    private String link;

}
