package com.yimu.ebook.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Angus
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppVersionModel {

    private String versionName;

    private String versionCode;

    private String platform;

    private String title;

    private String description;

    private String link;

    private Integer forceUpdate;

    private Integer inReview;

    private String agreement;
}
