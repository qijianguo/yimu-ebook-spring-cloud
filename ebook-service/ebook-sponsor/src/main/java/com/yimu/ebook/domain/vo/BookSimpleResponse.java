package com.yimu.ebook.domain.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BookSimpleResponse {

    /**
     * 书Id
     */
    private Long id;

    /**
     * 书名
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    /**
     * 封面图片
     */
    private String cover;

    /**
     * 简介
     */
    private String summary;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 子分类名称
     */
    private String subCategoryName;

    private String status;

}
