package com.yimu.ebook.domain.model;

import lombok.Getter;

/**
 * @author Angus
 */
@Getter
public enum BookAudit {

  // 待审核  审核成功 审核失败 上架 下架

  NO_AUDIT(-3, "待审核"),
  NO_PASS(-2, "待审核"),
  PASSED(-1, "待审核"),
  UNSHELVE(0, "下架"),
  SHELVE(1, "上架"),
  ;

  private Integer id;

  private String name;

  BookAudit(Integer id, String name) {
    this.id = id;
    this.name = name;
  }
}
