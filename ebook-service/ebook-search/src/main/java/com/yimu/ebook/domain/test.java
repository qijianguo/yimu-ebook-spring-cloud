package com.yimu.ebook.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class test {


    private String bookId;

    private String name;

    private String author;

    private String cover;

    private Double wordSize;

    private Long hot;

    private Long favorites;

    private Double score;

    private String summary;

    private Date createTime;

    private Date updateTime;

    private Integer categoryId;

    private Integer subCategoryId;

    private Integer channelId;

    private Integer bookStatusId;

    /**
     * 是否同步：0未同步1同步
     */
    private int sync;

    public boolean validate() {
        if (bookId == null) {
            return false;
        }
        return true;
    }


}