package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.IgnoreResponseAdvice;
import com.yimu.ebook.client.SponsorClient;
import com.yimu.ebook.domain.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class SearchController {

    @Autowired
    private SponsorClient sponsorClient;

    @IgnoreResponseAdvice
    @PostMapping("/getTelphoneCode")
    public Result getTelphoneCode(@PathVariable String telphone) {
        log.info(telphone);
        return sponsorClient.getTelphoneCode(telphone);
    }
}
