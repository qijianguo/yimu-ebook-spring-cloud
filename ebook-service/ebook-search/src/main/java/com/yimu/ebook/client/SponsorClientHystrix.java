package com.yimu.ebook.client;

import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.EmBusinessError;
import org.springframework.stereotype.Component;

/**
 * 熔断器：服务降级，防止应用雪崩
 */
@Component
public class SponsorClientHystrix implements SponsorClient{
    @Override
    public Result getTelphoneCode(String telphone) {
        return Result.fail(EmBusinessError.UNKNOW_ERROR.getErrorCode(),
                "eureka-client-ebook-sponsor error");
    }
}
