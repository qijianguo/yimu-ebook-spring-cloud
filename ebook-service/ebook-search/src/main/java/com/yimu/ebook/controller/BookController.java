package com.yimu.ebook.controller;

import com.yimu.ebook.annotation.IgnoreResponseAdvice;
import com.yimu.ebook.client.BookClient;
import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.domain.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class BookController {

    @Autowired
    private BookClient bookClient;

    @IgnoreResponseAdvice
    @PostMapping
    public Result saveOrUpdate(CreateBookRequest request) {
        return bookClient.saveOrUpdate(request);
    }

}
