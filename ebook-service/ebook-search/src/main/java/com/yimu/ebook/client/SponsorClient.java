package com.yimu.ebook.client;

import com.yimu.ebook.domain.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@FeignClient(value = "eureka-client-ebook-sponsor",
        fallback = SponsorClientHystrix.class)
public interface SponsorClient  {

    @PostMapping("/ebook-sponsor/user/{telphone}/code")
    Result getTelphoneCode(@PathVariable String telphone);
}
