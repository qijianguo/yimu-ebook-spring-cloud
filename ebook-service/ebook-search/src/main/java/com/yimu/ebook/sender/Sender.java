package com.yimu.ebook.sender;

import com.yimu.ebook.exception.BusinessException;

public interface Sender {

    void sender(Object obj) throws BusinessException;
}
