package com.yimu.ebook.client;

import com.yimu.ebook.domain.vo.CreateBookRequest;
import com.yimu.ebook.domain.vo.Result;
import com.yimu.ebook.exception.EmBusinessError;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;


@Component
public class BookClientHystrix implements BookClient {

    @Override
    public Result saveOrUpdate(CreateBookRequest request) {
        return Result.fail(EmBusinessError.UNKNOW_ERROR.getErrorCode(),
                "eureka-client-ebook-sponsor error");
    }
}
