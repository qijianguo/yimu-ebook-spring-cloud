package com.yimu.ebook.sender;

import com.alibaba.fastjson.JSON;
import com.yimu.ebook.exception.BusinessException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("kafkaSender")
public class KafkaSender implements Sender{

    /**
     * 短信验证码
     */
    @Value("${ebook_conf.kafka.topic}")
    private String topic;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Override
    public void sender(Object obj) throws BusinessException {
        kafkaTemplate.send(topic, JSON.toJSON(obj));
    }

    /**
     * 测试方法：
     * 监听kafka，如果有消息就去消费掉
     */
    @KafkaListener(topics = {"ebook_login"}, groupId = "ebook-search")
    public void process(ConsumerRecord<?, ?> record ) {
        Optional optional = Optional.ofNullable(record);
        if (optional.isPresent()) {
            Object value = optional.get();
            String message = (String) value;
            System.out.println(message);
        }
    }
}
