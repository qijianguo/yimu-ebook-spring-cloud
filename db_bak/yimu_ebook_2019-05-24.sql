# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: sh-cdb-rtzsmifi.sql.tencentcdb.com (MySQL 5.6.28-cdb2016-log)
# Database: yimu_ebook
# Generation Time: 2019-06-12 02:56:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table t_app_version
# ------------------------------------------------------------

CREATE TABLE `t_app_version` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `platform` varchar(10) NOT NULL DEFAULT '' COMMENT '平台：android/ios/h5/flutter',
  `version_code` varchar(11) NOT NULL DEFAULT '0.0.0' COMMENT '版本号',
  `version_name` varchar(11) NOT NULL DEFAULT '' COMMENT '版本名称',
  `description` varchar(50) DEFAULT '' COMMENT '描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_banner
# ------------------------------------------------------------

CREATE TABLE `t_banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT ' ',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `channel_id` int(2) NOT NULL DEFAULT '0' COMMENT '用户类型：男1、女2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book
# ------------------------------------------------------------

CREATE TABLE `t_book` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_uuid` varchar(32) NOT NULL DEFAULT '''''',
  `name` varchar(20) NOT NULL DEFAULT '''''' COMMENT '书名',
  `author` varchar(20) NOT NULL DEFAULT '''''' COMMENT ' 作者',
  `cover` varchar(200) NOT NULL DEFAULT '''''' COMMENT '封面图片地址',
  `word_size` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '字数',
  `hot` bigint(11) NOT NULL DEFAULT '0' COMMENT '热度',
  `favorites` bigint(11) NOT NULL DEFAULT '0',
  `score` decimal(2,1) NOT NULL DEFAULT '5.0' COMMENT '评分',
  `summary` varchar(300) NOT NULL DEFAULT '''''' COMMENT '简介',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上架时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `category_id` int(2) NOT NULL COMMENT '分类',
  `sub_category_id` int(2) NOT NULL COMMENT '子分类',
  `channel_id` int(2) NOT NULL DEFAULT '0' COMMENT '男频/女频',
  `book_status_id` int(11) NOT NULL DEFAULT '0' COMMENT '状态：连载1、完结2',
  `active` int(2) NOT NULL DEFAULT '0' COMMENT '审核状态1代审核2审核成功3审核失败4下架',
  PRIMARY KEY (`id`),
  UNIQUE KEY `book_uuid` (`book_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_active
# ------------------------------------------------------------

CREATE TABLE `t_book_active` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_chapter
# ------------------------------------------------------------

CREATE TABLE `t_book_chapter` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) NOT NULL COMMENT '章节ID',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `content_url` varchar(200) NOT NULL DEFAULT '' COMMENT '章节内容',
  `book_id` bigint(11) NOT NULL COMMENT '图书ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `book_id` (`book_id`,`chapter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_editor_recommend
# ------------------------------------------------------------

CREATE TABLE `t_book_editor_recommend` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hot` bigint(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `book_name` varchar(20) NOT NULL DEFAULT '' COMMENT '书名（暂时冗余）',
  `channel_id` int(2) NOT NULL,
  `book_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_hot_search
# ------------------------------------------------------------

CREATE TABLE `t_book_hot_search` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hot` bigint(11) NOT NULL,
  `date` date NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `book_name` varchar(20) NOT NULL DEFAULT '' COMMENT '书名（暂时冗余）',
  `channel_id` int(2) NOT NULL,
  `book_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_month_hot_rank
# ------------------------------------------------------------

CREATE TABLE `t_book_month_hot_rank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hot` bigint(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `book_name` varchar(20) NOT NULL DEFAULT '' COMMENT '书名（暂时冗余）',
  `channel_id` int(2) NOT NULL,
  `book_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_newbook_hot_rank
# ------------------------------------------------------------

CREATE TABLE `t_book_newbook_hot_rank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hot` bigint(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `book_name` varchar(20) NOT NULL DEFAULT '' COMMENT '书名（暂时冗余）',
  `channel_id` int(2) NOT NULL,
  `book_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_show_control
# ------------------------------------------------------------

CREATE TABLE `t_book_show_control` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_status
# ------------------------------------------------------------

CREATE TABLE `t_book_status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_total_rank
# ------------------------------------------------------------

CREATE TABLE `t_book_total_rank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hot` bigint(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `book_name` varchar(20) NOT NULL DEFAULT '' COMMENT '书名（暂时冗余）',
  `channel_id` int(2) NOT NULL,
  `book_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_book_week_hot_rank
# ------------------------------------------------------------

CREATE TABLE `t_book_week_hot_rank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hot` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `book_name` varchar(20) NOT NULL DEFAULT '' COMMENT '书名（暂时冗余）',
  `channel_id` int(2) NOT NULL,
  `book_id` bigint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_bookshelf
# ------------------------------------------------------------

CREATE TABLE `t_bookshelf` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` bigint(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_book` (`user_id`,`book_id`),
  KEY `idx_user_id_book_id` (`user_id`,`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_category
# ------------------------------------------------------------

CREATE TABLE `t_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  `image` varchar(200) NOT NULL DEFAULT '' COMMENT '分类的展示图片',
  `channel_id` int(11) NOT NULL COMMENT '频道ID（男频(1)/女频(2)）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_category_sub
# ------------------------------------------------------------

CREATE TABLE `t_category_sub` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(11) NOT NULL DEFAULT '',
  `image` varchar(200) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_channel
# ------------------------------------------------------------

CREATE TABLE `t_channel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '',
  `image` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_feedback
# ------------------------------------------------------------

CREATE TABLE `t_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(300) NOT NULL DEFAULT '' COMMENT '反馈内容',
  `contact` varchar(15) NOT NULL DEFAULT '' COMMENT '联系方式',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间 ',
  `staus` int(2) NOT NULL DEFAULT '0' COMMENT '状态：0未处理/1已处理',
  `user_id` bigint(11) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_problem
# ------------------------------------------------------------

CREATE TABLE `t_problem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_problem_answer
# ------------------------------------------------------------

CREATE TABLE `t_problem_answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `problem` varchar(30) NOT NULL DEFAULT '',
  `answer` varchar(200) NOT NULL DEFAULT '',
  `problem_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_read_history
# ------------------------------------------------------------

CREATE TABLE `t_read_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL COMMENT '用户ID',
  `book_id` bigint(11) NOT NULL COMMENT '书籍ID',
  `chapter_id` bigint(11) NOT NULL COMMENT '章节ID',
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次阅读时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_resource_book_city
# ------------------------------------------------------------

CREATE TABLE `t_resource_book_city` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `img_url_click` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(5) DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_resource_tab
# ------------------------------------------------------------

CREATE TABLE `t_resource_tab` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `img_url_click` varchar(50) NOT NULL DEFAULT '' COMMENT '图片点击',
  `img_url_unclick` varchar(50) NOT NULL DEFAULT '' COMMENT '图片未点击',
  `name` varchar(5) NOT NULL DEFAULT '' COMMENT '功能描述',
  `link` varchar(50) NOT NULL DEFAULT '' COMMENT '目标地址（由前端提供）',
  `description` varchar(20) NOT NULL DEFAULT '' COMMENT '说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_second_test
# ------------------------------------------------------------

CREATE TABLE `t_second_test` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table t_user
# ------------------------------------------------------------

CREATE TABLE `t_user` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '男1、女2',
  `age` int(3) NOT NULL DEFAULT '0',
  `telphone` varchar(11) NOT NULL DEFAULT '0',
  `head_img_url` varchar(50) NOT NULL DEFAULT '' COMMENT '头像地址',
  `register_mode` varchar(32) NOT NULL DEFAULT '',
  `third_party_id` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `channel_id` int(2) NOT NULL COMMENT '频道Id 1男频2女频',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
