package com.yimu.ebook.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * PostFilter
 * @author Angus
 */
@Slf4j
@Component
public class AccessLogFilter extends ZuulFilter {

    @Value("${server.port}")
    private String serverPort;

    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.SEND_RESPONSE_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext context = RequestContext.getCurrentContext();
        log.info("网关：{}", serverPort);
        long startTime = (Long) context.get("startTime");
        long duration = System.currentTimeMillis() - startTime;
        HttpServletRequest request = context.getRequest();
        String uri = request.getRequestURI();
        log.info("uri: {}, duration: {} ms", uri, duration / 100);
        return null;
    }
}
